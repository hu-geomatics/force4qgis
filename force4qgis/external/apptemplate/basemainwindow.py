# from __future__ import annotations

from os.path import join, dirname

from PyQt5.QtWidgets import QMainWindow, QToolButton, QProgressBar, QLabel, QWidget
from qgis.PyQt import uic
from qgis._gui import QgisInterface, QgsMessageBar


class BaseMainWindow(QMainWindow):
    iface: QgisInterface

    mMessageBar: QgsMessageBar
    mContent: QWidget
    mRun: QToolButton
    mCancel: QToolButton
    mProgressBar: QProgressBar
    mInfo: QLabel

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        uic.loadUi(join(dirname(__file__), 'mainwindow.ui'), self)
