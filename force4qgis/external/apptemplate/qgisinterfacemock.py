from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QAction, QToolButton, QVBoxLayout
from qgis._gui import QgisInterface


class QgisInterfaceMock(QgisInterface):

    def __init__(self):
        QgisInterface.__init__(self)

        self.ui = QMainWindow()
        self.ui.setWindowTitle('QGIS')
        self.ui.setWindowIcon(QIcon(r'C:\source\QGIS3-master\images\icons\qgis_icon.svg'))
        self.ui.resize(QSize(1500, 400))
        self.ui.show()

    def addToolBarIcon(self, action: QAction):
        button = QToolButton()
        button.setDefaultAction(action)
        self.ui.layout().addWidget(button)