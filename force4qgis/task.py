from datetime import datetime
from os import makedirs, listdir
from os.path import join, basename, exists
from shutil import rmtree
from typing import List, Tuple

from qgis._core import Qgis, QgsRasterLayer, QgsProject

from .script import Parameters, main, OpenOption

import traceback
from datetime import datetime

from dataclasses import dataclass

from os.path import join, basename
from typing import List, Tuple

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QDialog, QVBoxLayout, QTextEdit, QPushButton
from qgis._core import QgsTask, Qgis, QgsApplication, QgsRasterLayer, QgsProject
from qgis._gui import QgsMessageBar

from force4qgis.force4qgis.script import CanceledError, main, OpenOption
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core.progress import Progress, printProgress


@dataclass(frozen=True)
class Validation(object):
    valid: bool
    widgets: List[QWidget]
    message: str

class Task(QgsTask):

    def __init__(self, plugin, parameters: Parameters):
        super().__init__(plugin.name(), QgsTask.CanCancel)
        #from force4qgis.force4qgis.external.apptemplate.baseplugin import BasePlugin
        #assert isinstance(plugin, BasePlugin)
        self.plugin = plugin
        self.parameters = parameters
        self.exception = None
        self.traceback = None
        self.callback = self._makeCallback()
        self.plugin.mainWindow.mRun.setEnabled(False)
        self.plugin.mainWindow.mCancel.setEnabled(True)
        self.plugin.mainWindow.mInfo.setText('')

        self.logMessage(message='Validate parameters')
        self.invalidInputs = False
        for validation in self.validate():
            for widget in validation.widgets:
                if widget not in self.plugin.defaultStyleSheets:
                    self.plugin.defaultStyleSheets[widget] = widget.styleSheet()
                if validation.valid:
                    widget.setStyleSheet(self.plugin.defaultStyleSheets[widget])
                else:
                    widget.setStyleSheet('border-style: outset; border-width: 1px;border-color: red;')
                    self.logMessage(message=validation.message, level=Qgis.Critical)
                    self.pushWarning(title='Invalid input', message=validation.message)
                    self.invalidInputs = True

    def _makeCallback(self):

        now = str(datetime.now()).replace(':', '').replace(' ', 'T').replace('-', '')
        logfilename = f'{self.parameters.outputRoot}/log_{now}.txt'

        def callback(obj, end=None, flush=True, logOnly=False):
            # update progress
            # - in QGIS
            if isinstance(obj, Progress):
                self.setProgress(progress=obj.percentage)
            else:
                if not logOnly:
                    self.logMessage(message=str(obj))
            # - in log file
            with open(logfilename, 'a') as file:
                printProgress(progress=obj, end=end, file=file, flush=flush)

            # check if task was canceled
            if self.isCanceled():
                raise CanceledError('Task canceled by the user.')
        return callback

    def logMessage(self, message: str, level=Qgis.Info):
        QgsApplication.instance().messageLog().logMessage(message=message, tag=self.plugin.name(), level=level)
        self.plugin.mainWindow.mInfo.setText(message)
        self.plugin.mainWindow.mInfo.update()

    def pushError(self, exception: Exception, traceback: str):
        errorText = traceback
        errorTitle = f'{str(exception)}'

        def showError():
            class Dialog(QDialog):
                def __init__(self, title, text, parent, *args, **kwds):
                    super().__init__(parent, *args, **kwds)
                    self.setWindowTitle(title)
                    self.setFont(QFont("Fixed", 10))
                    self.setMinimumSize(500, 500)
                    self.setLayout(QVBoxLayout())
                    self.text = QTextEdit()
                    self.text.setText(text)
                    self.layout().addWidget(self.text)

            dialog = Dialog(title=f'{self.plugin.name()}: {errorTitle}', text=errorText, parent=self.plugin.ui)
            dialog.exec_()

        messageBar = self.plugin.mainWindow.mMessageBar
        if isinstance(exception, CanceledError):
            messageBar.pushInfo('Warning', str(exception))
        else:
            widget = messageBar.createMessage(errorTitle, '')
            button = QPushButton(widget)
            button.setText("Show Traceback")
            button.pressed.connect(showError)
            widget.layout().addWidget(button)
            messageBar.pushWidget(widget, Qgis.Critical)
            self.callback(traceback, logOnly=True)

    def pushWarning(self, title, message):
        self.plugin.mainWindow.mMessageBar.pushWarning(title=title, message=message)

    def validate(self) -> List[Validation]:
        from .widget import Widget
        ui: Widget = self.plugin.ui
        validations = list()

        validations.append(
            Validation(valid=exists(self.parameters.root), widgets=[ui.mRoot], message='Folder not existing')
        )

        valid = True
        if not exists(self.parameters.outputRoot):
            try:
                makedirs(self.parameters.outputRoot)
            except:
                valid = False
        validations.append(
            Validation(valid=valid, widgets=[ui.mOutputRoot], message='Can not create output folder'))

        validations.append(
            Validation(
                valid=len(self.parameters.features) > 0,
                widgets=[ui.mGroupFeatures],
                message='No output features selected'
            )
        )

        validations.append(
            Validation(
                valid=len(self.parameters.products) > 0 or self.parameters.timeseries,
                widgets=[ui.mGroupProducts],
                message='No output products selected'
            )
        )

        return validations

    def run(self):

        if self.invalidInputs:
            return False

        try:
            self.logMessage(message='Started task')
            self.run_()
        except Exception as exception:
            self.exception = exception
            self.traceback = traceback.format_exc()
            return False

        return True

    def run_(self):
        cleanup = self.parameters.outputExtension != '.vrt'
        with AutoFilenames(dir=join(self.parameters.outputRoot, 'base'), cleanup=cleanup):
            outputTileFilenames, outputMosaicFilenames = main(
                parameters=self.parameters, callback=self.callback, verbose=1
            )

        if self.parameters.open == OpenOption.Nothing:
            filenames = []
        elif self.parameters.open == OpenOption.Tiles:
            filenames = outputTileFilenames
        elif self.parameters.open == OpenOption.Mosaic:
            filenames = outputMosaicFilenames
        else:
            assert 0
        self.filenames = filenames
        self.callback(f'Open results')

    def finished_(self, result):
        for filename in self.filenames:
            self.callback(f'Open {filename}')
            layer = QgsRasterLayer(filename, basename(filename))
            QgsProject.instance().addMapLayer(layer)

        # delete tmp/base folder
        if self.parameters.outputExtension.lower() != '.vrt':
            try:
                self.logMessage(message='Remove tmp folder', level=Qgis.Info)
                rmtree(join(self.parameters.outputRoot, 'base'))
            except:
                pass

        self.logMessage(message='Task completed', level=Qgis.Success)

    def finished(self, result):

        self.setProgress(0)
        self.plugin.mainWindow.mRun.setEnabled(True)
        self.plugin.mainWindow.mCancel.setEnabled(False)
        if result:
            self.finished_(result=result)
        else:
            if self.invalidInputs:
                return
            else:
                self.pushError(exception=self.exception, traceback=self.traceback)
