import pathlib

from .external.apptemplate.basemainwindow import BaseMainWindow
from .widget import Widget
from .widget2 import Widget2
from . import utils
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction
from qgis._core import QgsApplication
from qgis.gui import QgisInterface


class Plugin(object):

    def __init__(self, iface):
        ui = Widget()
        ui2 = Widget2()
        mainWindow = BaseMainWindow()
        mainWindow2 = BaseMainWindow()

        assert isinstance(iface, QgisInterface)
        assert isinstance(ui, Widget)
        assert isinstance(mainWindow, BaseMainWindow)
        assert isinstance(ui2, Widget2)
        assert isinstance(mainWindow2, BaseMainWindow)
        self.iface = iface

        self.ui = ui
        self.mainWindow = mainWindow
        self.mainWindow.setWindowTitle(f'{self.name()} (v{self.version()})')
        self.mainWindow.setWindowIcon(self.icon())
        self.mainWindow.mContent.layout().addWidget(self.ui)
        self.defaultStyleSheets = dict()
        self.mainWindow.mRun.clicked.connect(self.ui.onRunClicked)
        self.mainWindow.mCancel.clicked.connect(self.ui.onCancelClicked)
        QgsApplication.instance().messageLog().messageReceived.connect(self.ui.onMessageReceived)

        self.ui2 = ui2
        self.mainWindow2 = mainWindow2
        self.mainWindow2.setWindowTitle(f'{self.name2()} (v{self.version()})')
        self.mainWindow2.setWindowIcon(self.icon())
        self.mainWindow2.mContent.layout().addWidget(self.ui2)
        self.defaultStyleSheets2 = dict()
        self.mainWindow2.mRun.clicked.connect(self.ui2.onRunClicked)
        self.mainWindow2.mCancel.clicked.connect(self.ui2.onCancelClicked)
        QgsApplication.instance().messageLog().messageReceived.connect(self.ui2.onMessageReceived)

        self.ui.setPlugin(plugin=self)
        self.ui2.setPlugin(plugin=self)

    def initGui(self):
        self.action = QAction(self.icon(), self.name(), self.iface.mainWindow())
        self.action.triggered.connect(self.toggleUiVisibility)
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToRasterMenu(self.name(), self.action)

        self.action2 = QAction(self.icon(), self.name2(), self.iface.mainWindow())
        self.action2.triggered.connect(self.toggleUiVisibility2)
        # self.iface.addToolBarIcon(self.action2)
        self.iface.addPluginToRasterMenu(self.name(), self.action2)

    def unload(self):
        self.mainWindow.close()
        self.iface.removeToolBarIcon(self.action)
        self.iface.removePluginMenu(self.name(), self.action)
        self.mainWindow2.close()
        # self.iface.removeToolBarIcon(self.action2)
        self.iface.removePluginMenu(self.name(), self.action2)

    def toggleUiVisibility(self):
        self.mainWindow.setVisible(not self.mainWindow.isVisible())

    def toggleUiVisibility2(self):
        self.mainWindow2.setVisible(not self.mainWindow2.isVisible())

    def folder(self) -> str:
        return pathlib.Path(__file__).parents[1].as_posix()

    def name(self) -> str:
        return 'FORCE4Q'

    def name2(self) -> str:
        return 'FORCE4Q Data Import'

    def id(self) -> str:
        return self.name().replace(' ', '')

    def id2(self) -> str:
        return self.name2().replace(' ', '')

    def version(self) -> str:
        return utils.version()

    def icon(self):
        path = pathlib.Path(self.folder()) / 'icon.svg'
        return QIcon(path.as_posix())
