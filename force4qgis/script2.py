from collections import defaultdict
from datetime import datetime
from pathlib import Path

from dataclasses import dataclass
from enum import Enum, auto, IntEnum
from os import DirEntry, scandir, makedirs
from os.path import join, splitext, exists, dirname, abspath
from typing import Tuple, Optional, List, Callable

import numpy as np

from force4qgis.hubforce.algorithms.composite import (minComposite, meanComposite, maxComposite, medianComposite,
                                                      stdComposite)
from force4qgis.hubforce.algorithms.createvrt import createMosaic, createStack
from force4qgis.hubforce.algorithms.nodataraster import noDataRaster
from force4qgis.hubforce.algorithms.percentile import percentiles
from force4qgis.hubforce.algorithms.qaimask import qaiMask
from force4qgis.hubforce.core.const.features import Feature
from force4qgis.hubforce.core.const.product import Product
from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.metadata.date import DateRange, DateSeason, DayOfYear, Date
from force4qgis.hubforce.core.progress import printProgress, Progress
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.core.raster.shape import GridShape
from force4qgis.hubforce.core.vector.vector import Vector
from force4qgis.hubforce.importformat.importlandsatcollection1 import isLandsatCollection1, importLandsatCollection1
from force4qgis.hubforce.inputformat.forcelevel2collection import forceLevel2Collection
from force4qgis.hubforce.utils import makeVrtSourcesRelative


class UsageError(Exception):
    pass


class CanceledError(Exception):
    pass


@dataclass
class Parameters2(object):
    folderDownload: str
    folderOutput: str

    @property
    def outputRoot(self):
        return self.folderOutput


def main(parameters2: Parameters2, callback: Callable = None, verbose: int = 1):
    assert isinstance(parameters2, Parameters2)

    if callback is None:
        callback = printProgress

    if verbose > 0:
        callback(f'Started at {datetime.now()}')
        callback(parameters2)

    if not exists(parameters2.folderOutput):
        try:
            makedirs(parameters2.folderOutput)
        except Exception as error:
            raise UsageError(f'Can not create output folder: {parameters2.folderOutput}\n({str(error)})')

    callback('Parse download folder')

    folders = list()
    entry: DirEntry
    for entry in scandir(parameters2.folderDownload):
        if entry.is_dir():
            if isLandsatCollection1(folder=entry.path):
                folders.append(entry.path)
    for i, folder in enumerate(folders, 1):
        callback(Progress(i, len(folders)))
        importLandsatCollection1(
            folder=folder,
            outputRoot=parameters2.folderOutput,
            callbackLog=callback
        )
    if verbose > 0:
        callback(f'Finished at {datetime.now()}')
