from os import makedirs, listdir
from os.path import join, basename, exists

from .script2 import Parameters2, main


import traceback
from datetime import datetime

from dataclasses import dataclass

from os.path import join, basename
from typing import List, Tuple

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QDialog, QVBoxLayout, QTextEdit, QPushButton
from qgis._core import QgsTask, Qgis, QgsApplication
from force4qgis.force4qgis.script2 import CanceledError, main
from force4qgis.hubforce.core.progress import Progress, printProgress


@dataclass(frozen=True)
class Validation(object):
    valid: bool
    widgets: List[QWidget]
    message: str

class Task2(QgsTask):

    def __init__(self, plugin, parameters:Parameters2):
        super().__init__(plugin.name(), QgsTask.CanCancel)
        self.plugin = plugin
        self.parameters = parameters
        self.exception = None
        self.traceback = None
        self.callback = self._makeCallback()
        self.plugin.mainWindow2.mRun.setEnabled(False)
        self.plugin.mainWindow2.mCancel.setEnabled(True)
        self.plugin.mainWindow2.mInfo.setText('')

        self.logMessage(message='Validate parameters')
        self.invalidInputs = False
        for validation in self.validate():
            for widget in validation.widgets:
                if widget not in self.plugin.defaultStyleSheets:
                    self.plugin.defaultStyleSheets[widget] = widget.styleSheet()
                if validation.valid:
                    widget.setStyleSheet(self.plugin.defaultStyleSheets[widget])
                else:
                    widget.setStyleSheet('border-style: outset; border-width: 1px;border-color: red;')
                    self.logMessage(message=validation.message, level=Qgis.Critical)
                    self.pushWarning(title='Invalid input', message=validation.message)
                    self.invalidInputs = True

    def _makeCallback(self):

        now = str(datetime.now()).replace(':', '').replace(' ', 'T').replace('-', '')
        logfilename = f'{self.parameters.outputRoot}/log_{now}.txt'

        def callback(obj, end=None, flush=True, logOnly=False):
            # update progress
            # - in QGIS
            if isinstance(obj, Progress):
                self.setProgress(progress=obj.percentage)
            else:
                if not logOnly:
                    self.logMessage(message=str(obj))
            # - in log file
            with open(logfilename, 'a') as file:
                printProgress(progress=obj, end=end, file=file, flush=flush)

            # check if task was canceled
            if self.isCanceled():
                raise CanceledError('Task canceled by the user.')
        return callback

    def logMessage(self, message: str, level=Qgis.Info):
        QgsApplication.instance().messageLog().logMessage(message=message, tag=self.plugin.name(), level=level)
        self.plugin.mainWindow2.mInfo.setText(message)
        self.plugin.mainWindow2.mInfo.update()

    def pushError(self, exception: Exception, traceback: str):
        errorText = traceback
        errorTitle = f'{str(exception)}'

        def showError():
            class Dialog(QDialog):
                def __init__(self, title, text, parent, *args, **kwds):
                    super().__init__(parent, *args, **kwds)
                    self.setWindowTitle(title)
                    self.setFont(QFont("Fixed", 10))
                    self.setMinimumSize(500, 500)
                    self.setLayout(QVBoxLayout())
                    self.text = QTextEdit()
                    self.text.setText(text)
                    self.layout().addWidget(self.text)

            dialog = Dialog(title=f'{self.plugin.name()}: {errorTitle}', text=errorText, parent=self.plugin.ui)
            dialog.exec_()

        messageBar = self.plugin.mainWindow2.mMessageBar
        if isinstance(exception, CanceledError):
            messageBar.pushInfo('Warning', str(exception))
        else:
            widget = messageBar.createMessage(errorTitle, '')
            button = QPushButton(widget)
            button.setText("Show Traceback")
            button.pressed.connect(showError)
            widget.layout().addWidget(button)
            messageBar.pushWidget(widget, Qgis.Critical)
            self.callback(traceback, logOnly=True)

    def pushWarning(self, title, message):
        self.plugin.mainWindow2.mMessageBar.pushWarning(title=title, message=message)

    def run_(self):
        main(parameters2=self.parameters, callback=self.callback, verbose=1
        )

    def validate(self) -> List[Validation]:
        from .widget2 import Widget2
        ui: Widget2 = self.plugin.ui2
        validations = list()

        validations.append(
            Validation(valid=exists(self.parameters.folderDownload), widgets=[ui.mFolderDownload], message='Folder not existing')
        )

        valid = True
        if not exists(self.parameters.folderOutput):
            try:
                makedirs(self.parameters.folderOutput)
            except:
                valid = False
        validations.append(Validation(valid=valid, widgets=[ui.mFolderOutput], message='Can not create output folder'))

        return validations

    def run(self):

        if self.invalidInputs:
            return False

        try:
            self.logMessage(message='Started task')
            self.run_()
        except Exception as exception:
            self.exception = exception
            self.traceback = traceback.format_exc()
            return False

        return True

    def finished_(self, result):
        self.logMessage(message='Task completed', level=Qgis.Success)

    def finished(self, result):

        self.setProgress(0)
        self.plugin.mainWindow2.mRun.setEnabled(True)
        self.plugin.mainWindow2.mCancel.setEnabled(False)
        if result:
            self.finished_(result=result)
        else:
            if self.invalidInputs:
                return
            else:
                self.pushError(exception=self.exception, traceback=self.traceback)
