# from __future__ import annotations

import traceback
from os.path import join
from tempfile import gettempdir

from PyQt5.QtCore import QDate
from qgis._core import QgsMapLayer, Qgis, QgsApplication
from qgis.gui import *
from qgis.PyQt.QtWidgets import *

from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QToolButton, QProgressBar, QLabel
from qgis._core import Qgis
from qgis._gui import QgisInterface, QgsMessageBar

from .task import Task
from .script import Parameters, BinningOption, OpenOption
from ..hubforce.core.const.features import Feature
from ..hubforce.core.const.product import Product
from ..hubforce.core.const.qai import Qai
from ..hubforce.core.const.resampling import Resampling
from ..hubforce.core.metadata.date import Date, DayOfYear, DateRange, DateSeason
from ..hubforce.core.raster.resolution import Resolution
from ..hubforce.core.raster.shape import GridShape
from . import ui

MESSAGE_CATEGORY = 'Force4QGIS'


class Widget(QWidget):
    iface: QgisInterface
    mMessageBar: QgsMessageBar
    mRun: QToolButton
    mCancel: QToolButton
    mProgressBar: QProgressBar
    mInfo: QLabel
    mRoot: QgsFileWidget
    mSensorAll: QRadioButton
    mSensorL: QRadioButton
    mSensorS2: QRadioButton
    mImp: QCheckBox
    mRoi: QgsMapLayerComboBox
    mToi: QLineEdit
    mDateRangeFilter: QCheckBox
    mDateRangeFilterStart: QDateEdit
    mDateRangeFilterEnd: QDateEdit
    mDateSeasonFilter: QCheckBox
    mDateSeasonFilterStart: QDateEdit
    mDateSeasonFilterEnd: QDateEdit
    mQValid_0: QCheckBox
    mQValid_1: QCheckBox
    mQCloud_0: QCheckBox
    mQCloud_1: QCheckBox
    mQCloud_2: QCheckBox
    mQCloud_3: QCheckBox
    mQCloudShadow_0: QCheckBox
    mQCloudShadow_1: QCheckBox
    mQSnow_0: QCheckBox
    mQSnow_1: QCheckBox
    mQWater_0: QCheckBox
    mQWater_1: QCheckBox
    mQAerosol_0: QCheckBox
    mQAerosol_1: QCheckBox
    mQAerosol_2: QCheckBox
    mQAerosol_3: QCheckBox
    mQSubzero_0: QCheckBox
    mQSubzero_1: QCheckBox
    mQSaturation_0: QCheckBox
    mQSaturation_1: QCheckBox
    mQHighSunZenith_0: QCheckBox
    mQHighSunZenith_1: QCheckBox
    mQIllumination_0: QCheckBox
    mQIllumination_1: QCheckBox
    mQIllumination_2: QCheckBox
    mQIllumination_3: QCheckBox
    mQSlope_0: QCheckBox
    mQSlope_1: QCheckBox
    mQWaterVapor_0: QCheckBox
    mQWaterVapor_1: QCheckBox
    mFBlue: QCheckBox
    mFGreen: QCheckBox
    mFRed: QCheckBox
    mFRe1: QCheckBox
    mFRe2: QCheckBox
    mFRe3: QCheckBox
    mFBnir: QCheckBox
    mFNir: QCheckBox
    mFSwir1: QCheckBox
    mFSwir2: QCheckBox
    mFNdvi: QCheckBox
    mFEvi: QCheckBox
    mFNbr: QCheckBox
    mFNdti: QCheckBox
    mFArvi: QCheckBox
    mFSavi: QCheckBox
    mFSarvi: QCheckBox
    mFTcb: QCheckBox
    mFTcg: QCheckBox
    mFTcw: QCheckBox
    mFTcdi: QCheckBox
    mFNdbi: QCheckBox
    mFNdwi: QCheckBox
    mFMndwi: QCheckBox
    mFNdmi: QCheckBox
    mFNdsi: QCheckBox
    mPTimeseries: QCheckBox
    mPMin: QCheckBox
    mPMean: QCheckBox
    mPMedian: QCheckBox
    mPMax: QCheckBox
    mPStd: QCheckBox
    mPPercentiles: QCheckBox
    mPPercentilesValue: QLineEdit
    mPFirst: QCheckBox
    mPLast: QCheckBox
    mPCentral: QCheckBox
    mBinningNo: QRadioButton
    mBinningYearly: QRadioButton
    mBinningQuarterly: QRadioButton
    mBinningMonthly: QRadioButton
    mBinningDays: QRadioButton
    mBinningDaysValue: QSpinBox
    mBinningStartDay: QDateEdit
    mResolution: QLineEdit
    RESAMPLING_OPTIONS = (
        (Resampling.NearestNeighbour, Resampling.NearestNeighbour), (Resampling.Average, Resampling.Mode)
    )
    FORMAT_OPTIONS = ['.tif', '.bsq', '.vrt', '.vrt']
    mResampling: QComboBox
    mOutputRoot: QgsFileWidget
    mOutputFormat: QComboBox
    mOpenMosaic: QRadioButton
    mOpenTiles: QRadioButton
    mOpenNothing: QRadioButton
    # mProcesses: QSpinBox
    mBlockSizeFull: QRadioButton
    mBlockSizeBlock: QRadioButton
    mBlockSizeX: QSpinBox
    mBlockSizeY: QSpinBox

    def __init__(self):
        QWidget.__init__(self)
        uic.loadUi(join(ui.path, 'content.ui'), self)
        self._initUi()

        # hide qualitative selection
        self.widgetQS.hide()
        self.labelQS.hide()

    def _initUi(self):
        # self.setWindowTitle(f'{self.windowTitle()} (v{utils.version()})')
        self.mRoot.setStorageMode(QgsFileWidget.GetDirectory)
        self.mOutputRoot.setStorageMode(QgsFileWidget.GetDirectory)
        self.mOutputRoot.setFilePath(join(gettempdir(), 'FORCE4Q'))

    def setPlugin(self, plugin):
        from force4qgis.force4qgis.plugin import Plugin
        assert isinstance(plugin, Plugin)
        self.plugin = plugin

    def onRunClicked(self):

        self.plugin.mainWindow.mMessageBar.clearWidgets()
        try:
            parameters = self.parameters()
        except Exception as exception:
            self.plugin.mainWindow.mMessageBar.pushWarning(title='Error', message=str(exception))
            return

        self.task = Task(parameters=parameters, plugin=self.plugin)
        self.task.progressChanged.connect(self.onProgressChanged)
        QgsApplication.taskManager().addTask(self.task)

    def onCancelClicked(self):
        try:
            self.task.cancel()
        except:
            pass

    def onMessageReceived(self, message: str, level: int):
        if level in [Qgis.Info, Qgis.Success]:
            self.plugin.mainWindow.mInfo.setText(message)

    def onProgressChanged(self, progress: float):
        self.plugin.mainWindow.mProgressBar.setValue(int(round(progress)))

    def parameters(self) -> Parameters:

        def getRoi(qgsMapLayer):
            if isinstance(qgsMapLayer, QgsMapLayer):
                return qgsMapLayer.source()
            else:
                return None

        def getToi(qLineEdit: QLineEdit):
            text = qLineEdit.text().strip()
            if text == '':
                return None
            else:
                return tuple(v.strip() for v in text.split(','))

        def getDate(qDate: QDate):
            return Date(year=qDate.year(), month=qDate.month(), day=qDate.day())

        def getDayOfYear(qDate: QDate):
            return DayOfYear(month=qDate.month(), day=qDate.day())

        def getPercentiles(qLineEdit: QLineEdit):
            return tuple(int(v) for v in qLineEdit.text().split(' '))

        def getBinning():
            if self.mBinningNo.isChecked():
                return BinningOption.No
            elif self.mBinningYearly.isChecked():
                return BinningOption.Yearly
            elif self.mBinningQuarterly.isChecked():
                return BinningOption.Quarterly
            elif self.mBinningMonthly.isChecked():
                return BinningOption.Monthly
            elif self.mBinningDays.isChecked():
                return BinningOption.Days
            else:
                assert 0

        def getOpen():
            if self.mOpenMosaic.isChecked():
                return OpenOption.Mosaic
            elif self.mOpenTiles.isChecked():
                return OpenOption.Tiles
            elif self.mOpenNothing.isChecked():
                return OpenOption.Nothing
            else:
                assert 0

        def getBlockSize():
            if self.mBlockSizeFull.isChecked():
                x = y = 9999999
            elif self.mBlockSizeBlock.isChecked():
                x, y = self.mBlockSizeX.value(), self.mBlockSizeY.value()
            else:
                assert 0
            return GridShape(x=x, y=y)

        if self.mDateRangeFilter.isChecked():
            dateRange = DateRange(
                start=getDate(qDate=self.mDateRangeFilterStart.date()),
                end=getDate(qDate=self.mDateRangeFilterEnd.date())
            )
        else:
            dateRange = None

        if self.mDateSeasonFilter.isChecked():
            dateSeason = DateSeason(
                start=getDayOfYear(qDate=self.mDateSeasonFilterStart.date()),
                end=getDayOfYear(qDate=self.mDateSeasonFilterEnd.date())
            )
        else:
            dateSeason = None

        guiQaiFlags = (
            (self.mQValid_0, Qai.ValidYes),
            (self.mQValid_1, Qai.ValidNo),
            (self.mQCloud_0, Qai.CloudClear),
            (self.mQCloud_1, Qai.CloudLessConfident),
            (self.mQCloud_2, Qai.CloudConfident),
            (self.mQCloud_3, Qai.CloudCirrus),
            (self.mQCloudShadow_0, Qai.CloudShadowNo),
            (self.mQCloudShadow_1, Qai.CloudShadowYes),
            (self.mQSnow_0, Qai.SnowNo),
            (self.mQSnow_1, Qai.SnowYes),
            (self.mQWater_0, Qai.WaterNo),
            (self.mQWater_1, Qai.WaterYes),
            (self.mQAerosol_0, Qai.AerosolEstimated),
            (self.mQAerosol_1, Qai.AerosolInterpolated),
            (self.mQAerosol_2, Qai.AerosolHigh),
            (self.mQAerosol_3, Qai.AerosolFill),
            (self.mQSubzero_0, Qai.SubzeroNo),
            (self.mQSubzero_1, Qai.SubzeroYes),
            (self.mQSaturation_0, Qai.SaturationNo),
            (self.mQSaturation_1, Qai.SaturationYes),
            (self.mQHighSunZenith_0, Qai.HighSunZenithNo),
            (self.mQHighSunZenith_1, Qai.HighSunZenithYes),
            (self.mQIllumination_0, Qai.IlluminationGood),
            (self.mQIllumination_1, Qai.IlluminationLow),
            (self.mQIllumination_2, Qai.IlluminationPoor),
            (self.mQIllumination_3, Qai.IlluminationShadow),
            (self.mQSlope_0, Qai.SlopeNo),
            (self.mQSlope_1, Qai.SlopeYes),
            (self.mQWaterVapor_0, Qai.WaterVaporMeasured),
            (self.mQWaterVapor_1, Qai.WaterVaporFill)
        )
        qaiFlags = tuple(qaiFlag for checkbox, qaiFlag in guiQaiFlags if checkbox.isChecked())

        guiFeatures = (
            (self.mFBlue, Feature.Blue),
            (self.mFGreen, Feature.Green),
            (self.mFRed, Feature.Red),
            (self.mFRe1, Feature.Re1),
            (self.mFRe2, Feature.Re2),
            (self.mFRe3, Feature.Re3),
            (self.mFBnir, Feature.Bnir),
            (self.mFNir, Feature.Nir),
            (self.mFSwir1, Feature.Swir1),
            (self.mFSwir2, Feature.Swir2),
            (self.mFNdvi, Feature.Ndvi),
            (self.mFEvi, Feature.Evi),
            (self.mFNbr, Feature.Nbr),
            (self.mFNdti, Feature.Ndti),
            (self.mFArvi, Feature.Arvi),
            (self.mFSavi, Feature.Savi),
            (self.mFSarvi, Feature.Sarvi),
            (self.mFTcb, Feature.Tcb),
            (self.mFTcg, Feature.Tcg),
            (self.mFTcw, Feature.Tcw),
            (self.mFTcdi, Feature.Tcdi),
            (self.mFNdbi, Feature.Ndbi),
            (self.mFNdwi, Feature.Ndwi),
            (self.mFMndwi, Feature.Mndwi),
            (self.mFNdmi, Feature.Ndmi),
            (self.mFNdsi, Feature.Ndsi)
        )
        features = tuple(feature for checkbox, feature in guiFeatures if checkbox.isChecked())

        guiProducts = (
            (self.mPMin, Product.Min),
            (self.mPMean, Product.Mean),
            (self.mPMedian, Product.Median),
            (self.mPMax, Product.Max),
            (self.mPStd, Product.Std),
            (self.mPPercentiles, Product.Percentiles),
            # (self.mPFirst, Product.First),
            # (self.mPLast, Product.Last),
            # (self.mPCentral, Product.Central),
        )
        products = tuple(product for checkbox, product in guiProducts if checkbox.isChecked())

        binStart = getDayOfYear(qDate=self.mBinningStartDay.date())
        binDays = self.mBinningDaysValue.value()
        res = float(self.mResolution.text())
        if self.mSensorAll.isChecked():
            sensors = ('LND04', 'LND05', 'LND07', 'LND08', 'SEN2A', 'SEN2B')
        elif self.mSensorL.isChecked():
            sensors = ('LND04', 'LND05', 'LND07', 'LND08')
        elif self.mSensorS2.isChecked():
            sensors = ('SEN2A', 'SEN2B')
        else:
            assert 0

        parameters = Parameters(
            root=self.mRoot.filePath(),
            outputRoot=self.mOutputRoot.filePath(),
            outputExtension=self.FORMAT_OPTIONS[self.mOutputFormat.currentIndex()],
            sensors=sensors,
            prefereLandsatImproved=self.mImp.isChecked(),
            roi=getRoi(qgsMapLayer=self.mRoi.currentLayer()),
            tilenames=getToi(qLineEdit=self.mToi),
            dateRange=dateRange,
            dateSeason=dateSeason,
            qaiFlags=qaiFlags,
            features=features,
            products=products,
            percentiles=getPercentiles(qLineEdit=self.mPPercentilesValue),
            timeseries=self.mPTimeseries.isChecked(),
            binning=getBinning(),
            binStart=binStart,
            binDays=binDays,
            resolution=Resolution(x=res, y=res),
            resamplingBoa=self.RESAMPLING_OPTIONS[self.mResampling.currentIndex()][0],
            resamplingQai=self.RESAMPLING_OPTIONS[self.mResampling.currentIndex()][1],
            relativeToVRT=self.mOutputFormat.currentIndex() == (len(self.FORMAT_OPTIONS) - 1),
            open=getOpen(),
            # processes=self.mProcesses.value(),
            blocksize=getBlockSize())

        return parameters
