from os.path import join
from qgis._core import QgsApplication
from qgis.gui import *


from force4qgis.force4qgis.script2 import Parameters2
from .task2 import Task2
from . import ui

MESSAGE_CATEGORY = 'Force4QGIS'

from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QToolButton, QProgressBar, QLabel
from qgis._core import Qgis
from qgis._gui import QgisInterface, QgsMessageBar

class Widget2(QWidget):
    iface: QgisInterface
    mMessageBar: QgsMessageBar
    mRun: QToolButton
    mCancel: QToolButton
    mProgressBar: QProgressBar
    mInfo: QLabel

    def __init__(self):
        QWidget.__init__(self)
        uic.loadUi(join(ui.path, 'content2.ui'), self)
        self._initUi()

    def _initUi(self):
        self.mFolderDownload.setStorageMode(QgsFileWidget.GetDirectory)
        self.mFolderOutput.setStorageMode(QgsFileWidget.GetDirectory)

    def setPlugin(self, plugin):
        from force4qgis.force4qgis.plugin import Plugin
        assert isinstance(plugin, Plugin)
        self.plugin = plugin

    def onCancelClicked(self):
        self.task.cancel()

    def onMessageReceived(self, message: str, level: int):
        if level in [Qgis.Info, Qgis.Success]:
            self.plugin.mainWindow2.mInfo.setText(message)

    def onProgressChanged(self, progress: float):
        self.plugin.mainWindow2.mProgressBar.setValue(int(round(progress)))

    def onRunClicked(self):
        parameters2 = Parameters2(
            folderDownload = self.mFolderDownload.filePath(), folderOutput = self.mFolderOutput.filePath()
        )
        self.plugin.mainWindow2.mMessageBar.clearWidgets()
        self.task = Task2(parameters=parameters2, plugin=self.plugin)
        self.task.progressChanged.connect(self.onProgressChanged)
        QgsApplication.taskManager().addTask(self.task)
