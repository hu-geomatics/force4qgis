"""
Create NDVI collection from given FORCE collection.
"""
from collections import defaultdict
from datetime import datetime
from pathlib import Path

from dataclasses import dataclass
from enum import Enum, auto, IntEnum
from os import DirEntry, scandir, makedirs
from os.path import join, splitext, exists, dirname, abspath
from typing import Tuple, Optional, List, Callable

import numpy as np

from force4qgis.hubforce.algorithms.composite import (minComposite, meanComposite, maxComposite, medianComposite,
                                                      stdComposite)
from force4qgis.hubforce.algorithms.createvrt import createMosaic, createStack
from force4qgis.hubforce.algorithms.nodataraster import noDataRaster
from force4qgis.hubforce.algorithms.percentile import percentiles
from force4qgis.hubforce.algorithms.qaimask import qaiMask
from force4qgis.hubforce.core.const.features import Feature
from force4qgis.hubforce.core.const.product import Product
from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.metadata.date import DateRange, DateSeason, DayOfYear, Date
from force4qgis.hubforce.core.progress import printProgress
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.core.raster.shape import GridShape
from force4qgis.hubforce.core.vector.vector import Vector
from force4qgis.hubforce.inputformat.forcelevel2collection import forceLevel2Collection
from force4qgis.hubforce.utils import makeVrtSourcesRelative


class UsageError(Exception):
    pass


class CanceledError(Exception):
    pass


class OpenOption(Enum):
    Mosaic = auto()
    Tiles = auto()
    Nothing = auto()


class BinningOption(Enum):
    User = auto()
    No = auto()
    Yearly = auto()
    Quarterly = auto()
    Monthly = auto()
    Days = auto()


@dataclass
class Parameters(object):
    root: str
    outputRoot: str
    outputExtension: str = '.bsq'
    sensors: Tuple[str, ...] = None
    roi: Optional[str] = None
    tilenames: Tuple[str, ...] = None
    prefereLandsatImproved: bool = True
    dateRange: Optional[DateRange] = None
    dateSeason: Optional[DateSeason] = None
    qaiFlags: Tuple[Qai, ...] = (
        Qai.ValidNo, Qai.CloudLessConfident, Qai.CloudConfident, Qai.CloudCirrus, Qai.CloudShadowYes
    )
    features: Tuple[Feature, ...] = tuple()
    products: Tuple[Product, ...] = tuple()
    percentiles: Tuple[int, ...] = (0, 5, 25, 50, 75, 95, 100)
    timeseries: bool = False
    binning: BinningOption = BinningOption.No
    binStart: DayOfYear = DayOfYear(month=1, day=1)
    binDays: int = 10
    bins: Tuple[DateRange, ...] = None
    resolution: Resolution = Resolution(x=30., y=30.)
    resamplingBoa: Resampling = Resampling.NearestNeighbour
    resamplingQai: Resampling = Resampling.NearestNeighbour
    relativeToVRT: bool = False
    open: OpenOption = OpenOption.Tiles
    processes: int = 1
    blocksize: GridShape = GridShape(x=15000, y=15000)


def mask(raster: Raster, bandNames: List[str], qaiFlags: List[Qai]) -> Raster:
    qai = raster.select(['QAI'])
    mask = qaiMask(qai, flags=qaiFlags)
    return raster.select(bandNames).setMask(mask)


# Normalized Difference Vegetation Index
# 'https://www.indexdatabase.de/db/i-single.php?id=58'
def addNdvi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED']).expression(
        expression='((NIR - RED) / (NIR + RED) * 1e4)',
        name='NDVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Enhanced Vegetation Index
# 'https://www.indexdatabase.de/db/i-single.php?id=16'
def addEvi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED', 'BLUE']).expression(
        expression='(2.5 * ((NIR - RED) / (NIR + 6. * RED - 7.5 * BLUE + 1e4)) * 1e4)',
        name='EVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Normalized Burn Ratio
# 'https://www.indexdatabase.de/db/i-single.php?id=16'
def addNbr(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'SWIR2']).expression(
        expression='((NIR - SWIR2) / (NIR + SWIR2) * 1e4)',
        name='NBR',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Normalized Difference Tillage Index
def addNdti(raster: Raster) -> Raster:
    feature = raster.select(['SWIR1', 'SWIR2']).expression(
        expression='((SWIR1 - SWIR2) / (SWIR1 + SWIR2) * 1e4)',
        name='NDTI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Atmospherically Resistant Vegetation Index
def addArvi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED', 'BLUE']).expression(
        expression='((NIR - (RED - (BLUE - RED))) / (NIR + (RED - (BLUE - RED))) * 1e4)',
        name='ARVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Soil Adjusted Vegetation Index
def addSavi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED']).expression(
        expression='((NIR - RED) / (NIR + RED + 0.5e4) * 1.5 * 1e4)',
        name='SAVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Soil adjusted and Atmospherically Resistant Vegegation Index
def addSarvi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED', 'BLUE', ]).expression(
        expression='((NIR - (RED - (BLUE - RED))) / (NIR + (RED - (BLUE - RED)) + 0.5e4) * 1.5 * 1e4)',
        name='SARVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Tasselled Cap - brightness
def addTcb(raster: Raster) -> Raster:
    tcb = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='force4qgis.force4qgis.script.tcbFormular',
        name='TCB',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcb)


# Tasselled Cap - greenness
def addTcg(raster: Raster) -> Raster:
    tcg = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='force4qgis.force4qgis.script.tcgFormular',
        name='TCG',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcg)


# Tasselled Cap - wetness
def addTcw(raster: Raster) -> Raster:
    tcw = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='force4qgis.force4qgis.script.tcwFormular',
        name='TCW',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcw)


# Tasselled Cap - wetness
def addTcdi(raster: Raster) -> Raster:
    tcw = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='force4qgis.force4qgis.script.tcdiFormular',
        name='TCDI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcw)


# Normalized Difference Built-Up Index
def addNdbi(raster: Raster) -> Raster:
    feature = raster.select(['SWIR1', 'NIR']).expression(
        expression='((SWIR1 - NIR) / (SWIR1 + NIR) * 1e4)',
        name='NDBI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Normalized Difference Water Index
def addNdwi(raster: Raster) -> Raster:
    feature = raster.select(['GREEN', 'NIR']).expression(
        expression='((GREEN - NIR) / (GREEN + NIR) * 1e4)',
        name='NDWI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Modified Normalized Difference Water Index
def addMndwi(raster: Raster) -> Raster:
    feature = raster.select(['GREEN', 'SWIR1']).expression(
        expression='((GREEN - SWIR1) / (GREEN + SWIR1) * 1e4)',
        name='MNDWI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Normalized Difference Moisture Index
def addNdmi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'SWIR1']).expression(
        expression='((NIR - SWIR1) / (NIR + SWIR1) * 1e4)',
        name='NDMI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Normalized Difference Snow Index
def addNdsi(raster: Raster) -> Raster:
    feature = raster.select(['GREEN', 'SWIR1']).expression(
        expression='((GREEN - SWIR1) / (GREEN + SWIR1) * 1e4)',
        name='NDSI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


def tcbFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [0.2043, 0.4158, 0.5524, 0.5741, 0.3124, 0.2303]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def tcgFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [-0.1603, -0.2819, -0.4934, 0.7940, -0.0002, -0.1446]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def tcwFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [0.0315, 0.2021, 0.3102, 0.1594, -0.6806, -0.6109]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff):
    tc = np.multiply(BLUE, coeff[0], dtype=np.float32)
    for b, c in zip((GREEN, RED, NIR, SWIR1, SWIR2), coeff[1:]):
        tc += np.multiply(b, c, dtype=np.float32)
    return tc


def tcdiFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    tcb = tcbFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2)
    tcg = tcgFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2)
    tcw = tcwFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2)
    tcdi = tcb - (tcg + tcw)
    return tcdi


def tilenamesCoveredByVector(root: str, vectorFilename: str = None):
    tilenames = list()

    # get ROI geometry
    if vectorFilename is not None:
        vector = Vector.open(filename=vectorFilename)
        layer = vector.layer()
        assert layer.featureCount == 1
        feature = list(layer.features)[0]
        roiGeometry = feature.geometry
        roiProjection = layer.projection
    else:
        roiGeometry = None
        roiProjection = None

    # get tilenames
    entry: DirEntry
    entry2: DirEntry

    for entry in scandir(root):
        # find tile folder
        if not entry.is_dir():
            continue
        if not (len(entry.name) == 11 and entry.name[0] == 'X' and entry.name[6] == 'Y'):
            continue

        if roiGeometry is None:
            tilenames.append(entry.name)
        else:
            # find first raster
            for entry2 in scandir(entry.path):
                if not entry2.is_file():
                    continue
                ext = splitext(entry2.name)[1]
                if ext in ['.bsq', '.dat', '.tif']:
                    gdalRaster = GdalRaster.open(filename=entry2.path)
                    tileGeometry = gdalRaster.grid.extent.geometry
                    tileProjection = gdalRaster.grid.projection
                    assert roiProjection == tileProjection
                    if roiGeometry.intersects(other=tileGeometry):
                        tilenames.append(entry.name)
                    break

    return tilenames


def log(filename, text):
    if filename is not None:
        with open(filename, 'a') as logfile:
            logfile.write(f'{text}\n')


def main(parameters: Parameters, callback: Callable = None, verbose: int = 1):
    assert isinstance(parameters, Parameters)

    if callback is None:
        callback = printProgress

    if verbose > 0:
        callback(f'Started at {datetime.now()}')
        callback(parameters)

    if parameters.sensors is None:
        sensors = ['LND04', 'LND05', 'LND07', 'LND08', 'SEN2A', 'SEN2B']
    else:
        sensors = parameters.sensors

    if verbose > 1:
        callback(f'Sensors: {sensors}')

    if parameters.binStart is None:
        binStart = DayOfYear(month=1, day=1)
    else:
        binStart = parameters.binStart

    if verbose > 1:
        callback(f'BinStart: {binStart}')

    if parameters.dateSeason is None:
        dateSeason = DateSeason(start=DayOfYear(month=1, day=1), end=DayOfYear(month=12, day=31))
    else:
        dateSeason = parameters.dateSeason

    sSeasonalRange = dateSeason.format(pattern='mmdd', separator='-')
    sBinStart = binStart.format(pattern='mmdd')
    sResolution = f'{parameters.resolution.x}m'
    if parameters.binning is BinningOption.Days:
        sBinning = f'{parameters.binDays}-{parameters.binning.name.upper()}'
    else:
        sBinning = parameters.binning.name.upper()

    if not exists(parameters.outputRoot):
        try:
            makedirs(parameters.outputRoot)
        except Exception as error:
            raise UsageError(f'Can not create output folder: {parameters.outputRoot}\n({str(error)})')

    if parameters.tilenames is None:
        tilenames = tilenamesCoveredByVector(root=parameters.root, vectorFilename=parameters.roi)
    else:
        tilenames = parameters.tilenames

    assert len(tilenames) > 0, 'no tiles found'

    callback('Parse Level 2 folder')

    # get boa
    featuresCollection = (
        forceLevel2Collection(
            forceLevel2Path=parameters.root,
            tilenames=tilenames,
            sensors=sensors,
            dateRange=parameters.dateRange,
            dateSeason=parameters.dateSeason,
            resolution=parameters.resolution,
            boaResampling=parameters.resamplingBoa,
            qaiResampling=parameters.resamplingQai,
            prefereLandsatImproved=parameters.prefereLandsatImproved,
            callback=callback,
            verbose=verbose
        )
    )

    if parameters.dateRange is None:
        dateRange = DateRange(
            start=min([r.date for r in featuresCollection]),
            end=max([r.date for r in featuresCollection])
        )
    else:
        dateRange = parameters.dateRange

    sDateRange = dateRange.format(pattern='yyyymmdd')

    if verbose > 1:
        callback(dateRange)

    # mask qai
    if verbose > 0:
        callback('Prepare QAI masking')

    if set(sensors).issubset(['SEN2A', 'SEN2B']):
        bandNames = ['BLUE', 'GREEN', 'RED', 'RE1', 'RE2', 'RE3', 'BNIR', 'NIR', 'SWIR1', 'SWIR2']
    else:
        bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']
    featuresCollection = featuresCollection.map(
        function=mask, bandNames=bandNames, qaiFlags=parameters.qaiFlags, callback=callback, verbose=verbose
    )

    # add derived features
    if verbose > 0:
        callback('Prepare features')
    featureNames = list()
    mapFunction = {
        Feature.Ndvi: addNdvi,
        Feature.Evi: addEvi,
        Feature.Nbr: addNbr,
        Feature.Ndti: addNdti,
        Feature.Arvi: addArvi,
        Feature.Savi: addSavi,
        Feature.Sarvi: addSarvi,
        Feature.Tcb: addTcb,
        Feature.Tcg: addTcg,
        Feature.Tcw: addTcw,
        Feature.Tcdi: addTcdi,
        Feature.Ndbi: addNdbi,
        Feature.Ndwi: addNdwi,
        Feature.Mndwi: addMndwi,
        Feature.Ndmi: addNdmi,
        Feature.Ndsi: addNdsi
    }

    for feature in parameters.features:
        featureNames.append(feature.name.upper())
        if feature in mapFunction:
            featuresCollection = featuresCollection.map(
                function=mapFunction[feature], callback=callback, verbose=verbose
            )

    # exclude unwanted wavebands
    if verbose > 0:
        callback('Exclude unwanted wavebands')

    featuresCollection = featuresCollection.select(selectors=featureNames, callback=callback, verbose=verbose)

    resultRasters = list()

    # reduce collection to composites
    if verbose > 0:
        callback('Prepare composites')

    productRasters = list()
    reduceFunction = {
        Product.Min: minComposite,
        Product.Mean: meanComposite,
        Product.Median: medianComposite,
        Product.Max: maxComposite,
        Product.Std: stdComposite,
        Product.Percentiles: percentiles,
        # Product.First = auto()
        # Product.Last = auto()
        # Product.Central = auto()
    }
    reduceKwargs = {
        Product.Percentiles: {'q': parameters.percentiles}
    }

    if verbose > 1:
        callback(parameters.binning)

    if parameters.binning == BinningOption.User:
        assert parameters.bins is not None
        bins = parameters.bins
    else:
        binStart = parameters.binStart
        bins = list()
        if parameters.binning == BinningOption.No:
            bins.append(dateRange)
        elif parameters.binning == BinningOption.Yearly:
            for year in range(dateRange.start.year - 1, dateRange.end.year + 1):
                start = Date(year=year, month=binStart.month, day=binStart.day)
                end = Date(year=year + 1, month=binStart.month, day=binStart.day).addDays(days=-1)
                bins.append(DateRange(start=start, end=end))
        elif parameters.binning == BinningOption.Monthly:
            for year in range(dateRange.start.year - 1, dateRange.end.year + 1):
                for off in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]:
                    start = Date(year=year, month=binStart.month, day=binStart.day).addMonths(months=off)
                    end = start.addMonths(months=1).addDays(days=-1)
                    bins.append(DateRange(start=start, end=end))
        elif parameters.binning == BinningOption.Quarterly:
            for year in range(dateRange.start.year - 1, dateRange.end.year + 1):
                for off in [0, 3, 6, 9]:
                    start = Date(year=year, month=binStart.month, day=binStart.day).addMonths(months=off)
                    end = start.addMonths(months=3).addDays(days=-1)
                    bins.append(DateRange(start=start, end=end))
        elif parameters.binning == BinningOption.Days:
            assert parameters.binDays >= 1
            start = dateRange.start
            while start <= dateRange.end:
                end = start.addDays(parameters.binDays - 1)
                bins.append(DateRange(start=start, end=end))
                start = end.addDays(days=1)
        else:
            assert 0

        # remove empty edge bins
        bins = tuple(bin for bin in bins if (bin.end >= dateRange.start) and (bin.start <= dateRange.end))

    for bin in bins:
        if verbose > 1:
            callback(f'Prepare bin: {bin}')
        featuresCollectionBin = featuresCollection.filterDate(dateRange=bin)
        for product in parameters.products:
            if featuresCollectionBin.empty:
                continue
            productRaster = featuresCollectionBin.reduce(
                function=reduceFunction[product], callback=callback, verbose=verbose,
                **reduceKwargs.get(product, dict())
            )

            # split into single band raster
            for bandName in productRaster.bandNames:
                sBin = bin.format(pattern='yyyymmdd')
                if productRaster.name == 'percentiles':
                    sFeature, sProduct = bandName.split('_')
                else:
                    sProduct, sFeature = bandName.split('_')
                    if sProduct.startswith('NAN'):
                        sProduct = sProduct[3:]
                newBandName = '_'.join([sBin, sSeasonalRange, sBinStart, sResolution, sProduct, sFeature])
                resultRaster = productRaster.select(selectors=[bandName], newBandNames=[newBandName],
                    newRasterName=newBandName)
                resultRasters.append(resultRaster)

    # reduce collection to timeseries bandstack
    if parameters.timeseries:
        if verbose > 1:
            callback('Prepare timeseries')
        # timeseriesRaster = featuresCollection.toBands().rename(name='TIMESERIES')

        # split into single band raster
        for productRaster in featuresCollection:
            for bandName in productRaster.bandNames:
                sResolution = f'{parameters.resolution.x}m'
                newBandName = '_'.join([productRaster.name, sResolution, bandName])
                resultRaster = productRaster.select(selectors=[bandName], newBandNames=[newBandName],
                    newRasterName=newBandName)
                resultRasters.append(resultRaster)

    noDataRaster_ = noDataRaster(
        name=f'noDataRaster_{parameters.resolution.x}m',
        bandNames=[f'noDataBand_{parameters.resolution.x}m'],
        date=None, noDataValue=-9999, dtype=np.int16,
        grids=featuresCollection.grids, tilenames=featuresCollection.tilenames
    )
    resultRasters.append(noDataRaster_)

    # compute to disk
    if verbose > 1:
        callback('Compute results')

    result = RasterCollection(
        rasters=tuple(resultRasters), grids=featuresCollection.grids, tilenames=featuresCollection.tilenames
    )
    outputFilenames = result.compute(
        dirname=parameters.outputRoot, extention=parameters.outputExtension, blockShape=parameters.blocksize,
        processes=parameters.processes, noDataRaster=noDataRaster_, callback=callback, verbose=verbose
    )

    outputTileFilenames = list()
    outputMosaicFilenames = list()

    # build stacks for each tile
    if verbose > 0:
        callback('Build VRT stacks')

    keyNoDataRaster = '_'.join(['noDataRaster', sResolution])

    # - composites
    if len(parameters.products) != 0:
        outputProductBandByTile = defaultdict(list)
        outputProducts = list()
        keyProduct = '_'.join([sDateRange, sBinning, sSeasonalRange, sBinStart, sResolution])
        for tilename in tilenames:
            bandNames = list()
            # find all files
            for bin in bins:
                names = list()
                sBin = bin.format(pattern="yyyymmdd")
                for product in parameters.products:
                    if product is Product.Percentiles:
                        sProducts = [f'P{percentile}' for percentile in parameters.percentiles]
                    else:
                        sProducts = [product.name.upper()]
                    for sProduct in sProducts:
                        for feature in parameters.features:
                            sFeature = feature.name.upper()
                            key = '_'.join([sBin, sSeasonalRange, sBinStart, sResolution, sProduct, sFeature])
                            if tilename in outputFilenames[key]:
                                filename = outputFilenames[key][tilename]
                            else:
                                filename = outputFilenames[keyNoDataRaster][tilename]
                            outputProductBandByTile[tilename].append(filename)
                            bandNames.append('_'.join([sBin, sProduct, sFeature]))
                            names.append('_'.join([sProduct, sFeature]))

            # build vrt
            filenames = outputProductBandByTile[tilename]
            rasters = [GdalRaster.open(filename=filename) for filename in filenames]
            filename = join(parameters.outputRoot, 'VRT', tilename, keyProduct) + '.vrt'
            raster = createStack(rasters=rasters, filename=filename)
            outputProducts.append(filename)

            # set metadata
            dates = [bin.center.format(pattern='yyyy-mm-dd') for bin in bins]
            raster.setMetadataItem(key='names', value=names, domain='TIMESERIES')
            raster.setMetadataItem(key='dates', value=dates, domain='TIMESERIES')
            for band, bandName in zip(raster.bands, bandNames):
                band.setNoDataValue(band.noDataValue(-9999))
                band.setDescription(bandName)

            outputTileFilenames.extend(outputProducts)

    # - timeseries
    if parameters.timeseries:
        outputTimeseriesBandByTile = defaultdict(list)
        outputTimeseriess = list()
        keyTimeseries = '_'.join([sDateRange, 'TIMESERIES', sSeasonalRange, sResolution])
        scenesAndDates = sorted([(raster.name, raster.date) for raster in featuresCollection], key=lambda t: t[0])
        scenes, dates = zip(*scenesAndDates)
        for tilename in tilenames:
            bandNames = list()
            # find all files
            for sScene in scenes:
                for feature in parameters.features:
                    sFeature = feature.name.upper()
                    key = '_'.join([sScene, sResolution, sFeature])
                    filenameByTile = outputFilenames[key]
                    if tilename in filenameByTile:
                        filename = filenameByTile[tilename]
                    else:
                        filename = outputFilenames[keyNoDataRaster][tilename]
                    outputTimeseriesBandByTile[tilename].append(filename)
                    bandNames.append(key)

            # build vrt
            filenames = outputTimeseriesBandByTile[tilename]
            rasters = [GdalRaster.open(filename=filename) for filename in filenames]
            filename = join(parameters.outputRoot, 'VRT', tilename, keyTimeseries) + '.vrt'
            raster = createStack(rasters=rasters, filename=filename)
            outputTimeseriess.append(filename)

            # set metadata
            dates = [date.format(pattern='yyyy-mm-dd') for date in dates]
            raster.setMetadataItem(key='names', value=featureNames, domain='TIMESERIES')
            raster.setMetadataItem(key='dates', value=dates, domain='TIMESERIES')
            for band, bandName in zip(raster.bands, bandNames):
                band.setNoDataValue(band.noDataValue(-9999))
                band.setDescription(bandName)

        outputTileFilenames.extend(outputTimeseriess)

    # build mosaic
    if verbose > 0:
        callback('Build VRT mosaics')

    # - products
    if len(parameters.products) != 0:
        rasters = [GdalRaster.open(filename=filename) for filename in outputProducts]
        outputProduct = join(parameters.outputRoot, 'VRT', keyProduct) + '.vrt'
        raster = createMosaic(rasters=rasters, filename=outputProduct)
        raster.setMetadataDict(rasters[0].metadataDict)
        for band, band_ in zip(raster.bands, rasters[0].bands):
            band.setNoDataValue(band_.noDataValue())
            band.setMetadataDict(band_.metadataDict)
            band.setDescription(band_.description)

        outputMosaicFilenames.append(outputProduct)

    # - timeseries
    if parameters.timeseries:
        rasters = [GdalRaster.open(filename=filename) for filename in outputTimeseriess]
        outputTimeseries = join(parameters.outputRoot, 'VRT', keyTimeseries) + '.vrt'
        raster = createMosaic(rasters=rasters, filename=outputTimeseries)
        raster.setMetadataDict(rasters[0].metadataDict)
        for band, band_ in zip(raster.bands, rasters[0].bands):
            band.setNoDataValue(band_.noDataValue())
            band.setMetadataDict(band_.metadataDict)
            band.setDescription(band_.description)

        outputMosaicFilenames.append(outputTimeseries)

    # inject relative VRT filenames
    if parameters.relativeToVRT and parameters.outputExtension == '.vrt':
        if verbose > 0:
            callback('Inject relative VRT filenames')

        del band, band_, featuresCollection, featuresCollectionBin, noDataRaster_, raster, rasters, result, resultRaster, resultRasters, productRaster, productRasters

        for path in Path(parameters.outputRoot).rglob('*.vrt'):
            if verbose > 1:
                callback(f'Found {path}')
            makeVrtSourcesRelative(vrtFilename=str(path))

    if verbose > 0:
        callback(f'Finished at {datetime.now()}')

    return outputTileFilenames, outputMosaicFilenames

# todo

# - percentiles, first, last, central
# - check if any product is selected OR the timeseries
