from os import scandir

from hubdsm.core.gdaldriver import ENVI_DRIVER, GTIFF_DRIVER
from hubdsm.core.gdalraster import GdalRaster
from hubdsm.core.raster import Raster

raster = Raster.open(r'C:\Work\data\FORCE\edc\l2_validrange\20160807_LEVEL2_LND08_BOA.tif')
array = raster.readAsArray()
array *= 0
array += 10000
i = 0
for i1 in [0,1]:
    for i2 in [0,1]:
        for i3 in [0, 1]:
            for i4 in [0, 1]:
                for i5 in [0, 1]:
                    for i6 in [0, 1]:
                        print(i)
                        array[0, i, 0] = i1 * 1e4
                        array[1, i, 0] = i2 * 1e4
                        array[2, i, 0] = i3 * 1e4
                        array[3, i, 0] = i4 * 1e4
                        array[4, i, 0] = i5 * 1e4
                        array[5, i, 0] = i6 * 1e4
                        i += 1

GTIFF_DRIVER.createFromArray(array=array, grid=raster.grid, filename=r'C:\Work\data\FORCE\edc\validrange\X0058_Y0054\20160807_LEVEL2_LND08_BOA.tif').setNoDataValue(-9999)

from datetime import datetime
from time import time
from traceback import format_exc

from osgeo import gdal

from force4qgis.force4qgis.script import main, OpenOption, BinningOption
from force4qgis.force4qgis.script import Parameters
from force4qgis.hubforce.core.const.features import Feature
from force4qgis.hubforce.core.const.product import Product
from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core.metadata.date import DateRange, Date, DayOfYear
from force4qgis.hubforce.core.progress import printProgress, makeLogProgress
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.core.raster.shape import GridShape
from hubdsm.core.gdalraster import GdalRaster


parameters = Parameters(
    root=r'C:\Work\data\FORCE\edc\validrange',
    outputRoot='data',
    outputExtension='.tif',
    qaiFlags=tuple(),
    features=tuple(f for f in Feature.__members__.values() if f not in [Feature.Re1, Feature.Re2, Feature.Re3, Feature.Bnir]),
    products=tuple([Product.Median]),
    binning=BinningOption.No,
    resolution=Resolution(x=30, y=30),
    resamplingBoa=Resampling.NearestNeighbour, resamplingQai=Resampling.NearestNeighbour,
    processes=1, blocksize=GridShape(x=100000000, y=10000000)
)

if 1:
    with AutoFilenames(dir='data/tmp', cleanup=not True):
        outputTileFilenames, outputMosaicFilenames = main(parameters=parameters)

for entry in scandir(r'C:\source\QGISPlugIns\force4qgis\test\data\X0058_Y0054'):
    r = GdalRaster.open(entry.path)
    a = r.readAsArray()[:,0:64,0]
    print(entry.name.split('_')[-1][:-4], a.min(), a.max())