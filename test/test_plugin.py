from PyQt5.QtCore import QDate

from enmapbox.testing import initQgisApplication
from force4qgis.force4qgis.external.apptemplate.qgisinterfacemock import QgisInterfaceMock
from force4qgis.force4qgis.plugin import Plugin
from force4qgis.force4qgis.widget import Widget
from force4qgis.force4qgis.widget2 import Widget2

try:
    import qgisresources.images
    qgisresources.images.qInitResources()
except:
    pass

def test_plugin():

    iface = QgisInterfaceMock()
    plugin = Plugin(iface=iface)
    plugin.initGui()

    # select parameters
    ui: Widget = plugin.ui
    ui.mRoot.setFilePath(r'C:\Work\data\FORCE\edc\level2')
    ui.mToi.setText('X0058_Y0054')
    ui.mOutputRoot.setFilePath(r'data')
    ui.mOutputFormat.setCurrentIndex(2)
    ui.mDateRangeFilter.setChecked(True)
    ui.mDateRangeFilterStart.setDate(QDate(2017, 1, 1))
    ui.mDateRangeFilterEnd.setDate(QDate(2017, 12, 31))
    ui.mImp.setChecked(True)

    ui2: Widget2 = plugin.ui2
    ui2.mFolderDownload.setFilePath(r'C:\Work\data\FORCE4Q\Landsat')
    ui2.mFolderOutput.setFilePath('data')

    plugin.toggleUiVisibility()
    plugin.toggleUiVisibility2()

    qgsApp.exec_()


if __name__ == '__main__':

    qgsApp = initQgisApplication()
    test_plugin()
