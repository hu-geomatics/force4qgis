from datetime import datetime
from time import time
from traceback import format_exc

from osgeo import gdal

from force4qgis.force4qgis.script2 import Parameters2, main
from force4qgis.hubforce.core.const.features import Feature
from force4qgis.hubforce.core.const.product import Product
from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core.metadata.date import DateRange, Date, DayOfYear
from force4qgis.hubforce.core.progress import printProgress, makeLogProgress

if __name__ == '__main__':

    t0 = time()

    parameters2 = Parameters2(
        folderDownload=r'C:\Work\data\FORCE4Q\Landsat',
        folderOutput=r'C:\_test'
    )

    main(parameters2=parameters2)

    print((time()-t0)/60, 'min')
