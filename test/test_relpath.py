from osgeo import gdal

from force4qgis.force4qgis.script import main
from force4qgis.force4qgis.script import Parameters
from force4qgis.hubforce.core.const.features import Feature
from force4qgis.hubforce.core.const.product import Product
from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core.metadata.date import DateRange, Date, DayOfYear
from force4qgis.hubforce.core.progress import printProgress, makeLogProgress
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.core.raster.shape import GridShape
from force4qgis.hubforce.utils import copyOutputFolder

if __name__ == '__main__':

    parameters = Parameters(
        root=r'C:\Work\data\FORCE\edc\level2',
        outputRoot='data',
        outputExtension='.vrt',
        tilenames=('X0058_Y0054',),
        dateRange=DateRange(start=Date(2017, 1, 1), end=Date(2017, 1, 11)),
        qaiFlags=(Qai.ValidNo, ),
        features=(Feature.Ndvi, ),
        products=(Product.Mean, ),
        timeseries=True,
        resolution=Resolution(x=30, y=30),
        resamplingBoa=Resampling.NearestNeighbour, resamplingQai=Resampling.NearestNeighbour,
        relativeToVRT=False,
        processes=1, blocksize=GridShape(x=100000000, y=100000000)
    )

    with AutoFilenames(dir='data/tmp', cleanup=not True):
        outputTileFilenames, outputMosaicFilenames = main(parameters=parameters, verbose=999)
        print(outputTileFilenames)
        print(outputMosaicFilenames)

    from force4qgis.hubforce.utils import copyOutputFolder
    copyOutputFolder(
        src=r'c:\test\output', dst=r'd:\USBDRIVE\output',
        pathmap=[
            (r'C:\source\QGISPlugIns\force4qgis\test\data', r'D:\USBDRIVE'),
            (r'C:\Work\data\FORCE\edc\level2', r'D:\level2')
                 ]
    )

    print(gdal.Open(r'D:\USBDRIVE\VRT\20170101-20170111_TIMESERIES_0101-1231_30m.vrt').ReadAsArray().shape)


#    for filename in outputTileFilenames+outputMosaicFilenames:
#        print(filename)
