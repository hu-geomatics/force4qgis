from datetime import datetime
from time import time
from traceback import format_exc

from osgeo import gdal

from force4qgis.force4qgis.script import main, OpenOption, BinningOption
from force4qgis.force4qgis.script import Parameters
from force4qgis.hubforce.core.const.features import Feature
from force4qgis.hubforce.core.const.product import Product
from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core.metadata.date import DateRange, Date, DayOfYear
from force4qgis.hubforce.core.progress import printProgress, makeLogProgress
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.core.raster.shape import GridShape
from hubdsm.core.gdalraster import GdalRaster

if __name__ == '__main__':

    t0 = time()

    #bins=tuple(DateRange(start=Date(2017, i, 1), end=Date(2017, i+1, 1).addDays(-1)) for i in range(1, 12))[0:2]

    parameters = Parameters(
        root=r'C:\Work\data\FORCE\edc\level2',
        #root=r'\\141.20.140.91\san\_EnMAP\Project_LUMOS\ExampleData\edc\level2',
        outputRoot='data2',
        outputExtension='.tif',
        sensors=None, #('SEN2A', 'SEN2B'),
        #roi=r'C:\Work\data\FORCE\edc\roi3.gpkg',
        tilenames=('X0058_Y0054', 'X0058_Y0055',),
        #tilenames=('X0058_Y0055',),

        #tilenames=('X0058_Y0055',),
        #dateRange=DateRange(start=Date(2017, 1, 1), end=Date(2017, 3, 31)),
        dateRange=DateRange(start=Date(2017, 1, 1), end=Date(2017, 12, 31)),
        #dateSeason=DateSeason(start=DayOfYear(month=7, day=1), end=DayOfYear(month=7, day=31)),
        qaiFlags=(Qai.ValidNo, Qai.CloudLessConfident, Qai.CloudConfident, Qai.CloudCirrus, Qai.CloudShadowYes),
        #qaiFlags=tuple(),
        features=(Feature.Tcb, Feature.Tcg, Feature.Tcw),
        # features=(Feature.Ndvi, ),#Feature.Green, ),#Feature.Blue),# Feature.Ndvi),
        # features=(Feature.Sarvi, ),#Feature.Blue),# Feature.Ndvi),
        #features=tuple(Feature.__members__.values()),
        #products=tuple(Product.__members__.values()),
        products=(Product.Median, ),
        percentiles=(25, 50, 75),
#        percentiles=tuple(),
        timeseries=True,
        #bins=bins,
        binning=BinningOption.No,
        binStart=DayOfYear(month=1, day=1),
        binDays=5,
        resolution=Resolution(x=30, y=30),
        resamplingBoa=Resampling.NearestNeighbour, resamplingQai=Resampling.NearestNeighbour,
        relativeToVRT=True,
        open=OpenOption.Tiles,
        processes=1, blocksize=GridShape(x=100000000, y=100)
    )

    with AutoFilenames(dir='data/tmp', cleanup=not True):
        now = str(datetime.now()).replace(':', '').replace(' ','T').replace('-', '')
        logfilename = f'data/log_{now}.txt'
        callback = makeLogProgress(filename=logfilename)
        try:
            outputTileFilenames, outputMosaicFilenames = main(parameters=parameters, callback=callback, verbose=999)
        except Exception as error:
            callback(format_exc())
            raise error
        print(outputTileFilenames)
        print(outputMosaicFilenames)

    for filename in outputTileFilenames+outputMosaicFilenames:
        print(filename)

    print((time()-t0)/60, 'min')
