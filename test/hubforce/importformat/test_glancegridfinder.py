from unittest import TestCase

from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.spatial.extent import Extent
from force4qgis.hubforce.importformat.glancegridfinder import GlanceGridFinder, GlanceExtent


class TestGlanceGridFinder(TestCase):

    def test(self):
        raster = GdalRaster.open(
            filename=r'C:\Work\data\EarthExplorer\Landsat\C1L2\LC080140322019033001T1-SC20190517105817\LC08_L1TP_014032_20190330_20190404_01_T1_pixel_qa.tif'
        )
        glanceExtents = list(
            GlanceGridFinder().findGridsByGeometry(
                geometry=raster.grid.extent.geometry, projection=raster.grid.projection
            )
        )
        self.assertListEqual([64, 65, 63, 64, 65, 64, 65], [e.x for e in glanceExtents])
        self.assertListEqual([37, 37, 38, 38, 38, 39, 39], [e.y for e in glanceExtents])
