from unittest import TestCase

import numpy as np

from force4qgis.hubforce.importformat.importlandsatcollection1 import importLandsatCollection1, usgsToForceQai
from force4qgis.hubforce.importformat.usgsqa import PixelQaArray, SrAerosolArray


class TestImportLandsatCollection1(TestCase):

    def test(self):

        folders = [
                      r'C:\Work\data\FORCE4Q\Landsat\LC081930232019072601T1-SC20200903172131',
                      r'C:\Work\data\FORCE4Q\Landsat\LC081930242019072601T1-SC20200904132304',
                  ]

        for folder in folders:
            importLandsatCollection1(
                folder=folder,
                outputRoot=r'C:\vsimem\force'
            )


    def test_usgsToForceQai(self):

        pixelQaArray = np.array([[[0]]])
        srAerosolArray = np.array([[[0]]])

        qaiArray = usgsToForceQai(
            pixelQaArray=PixelQaArray(pixelQaArray),
            srAerosolArray=SrAerosolArray(srAerosolArray),
            boaArray=np.array([[[10]]])
        )

        print(qaiArray & 1)

        assert qaiArray & 1 == 0
        print(qaiArray)