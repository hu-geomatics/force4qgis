from unittest import TestCase

import numpy as np

from force4qgis.hubforce.importformat.usgsqa import SrAerosolArray, SrAerosol


class TestSrAerosolArray(TestCase):

    def test(self):
        self.assertTrue(SrAerosolArray(np.array(0, dtype=np.uint8)).mask(SrAerosol.FillYes))
        self.assertTrue(SrAerosolArray(np.array(1, dtype=np.uint8)).mask(SrAerosol.FillNo))

