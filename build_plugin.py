"""
This is a very small example how to
1. build a *.zip file that contains the QGIS Plugin and
2. printout the python code to install it via the QGIS python console

"""
import pathlib, zipfile, os, re, typing

def build():
    PLUGIN_DIR = pathlib.Path(__file__).parent
    assert PLUGIN_DIR.is_dir()
    PLUGIN_DIR = PLUGIN_DIR.absolute()
    PLUGIN_ZIP = PLUGIN_DIR / '{}.zip'.format(PLUGIN_DIR.name)

    PLUGIN_METADATA = PLUGIN_DIR / 'metadata.txt'
    assert os.path.isfile(PLUGIN_METADATA)

    PLUGIN_FILE_ENDINGS = re.compile(r'\.(py|png|svg|ui|txt)$')

    def scantree(path, ending='')->typing.Iterator[pathlib.Path]:
        """
        Recursively returns file paths in directory
        :param path: root directory to search in
        :param ending: str with required file ending, e.g. ".py" to search for *.py files
        :return: pathlib.Path
        """
        for entry in os.scandir(path):
            if entry.is_dir(follow_symlinks=False):
                yield from scantree(entry.path, ending=ending)
            elif entry.is_file and entry.path.endswith(ending):
                yield pathlib.Path(entry.path)


    for pathQrc in scantree(PLUGIN_DIR, '.qrc'):
        pass

    PLUGIN_FILES = [f for f in scantree(PLUGIN_DIR) if PLUGIN_FILE_ENDINGS.search(f.as_posix())]


    # create the zip file that contains all PLUGIN_FILES
    with zipfile.ZipFile(PLUGIN_ZIP, 'w') as f:
        for path in PLUGIN_FILES:
            arcName = path.relative_to(PLUGIN_DIR.parent).as_posix()
            f.write(path, arcname=arcName)

    print('## QGIS Plugin created: {}'.format(PLUGIN_ZIP.as_posix()))
    print('## To install it, copy the following lines to the QGIS Python Console\n')
    print('from pyplugin_installer.installer import pluginInstaller')
    print('pluginInstaller.installFromZipFile(r"{}")\n'.format(PLUGIN_ZIP.as_posix()))
    print('## Close (and restart manually)\n')
    # print('iface.mainWindow().close()\n')
    print('QProcess.startDetached(QgsApplication.arguments()[0], [])')
    print('QgsApplication.quit()')
    print('## press ENTER\n')

if __name__ == '__main__':

    build()
