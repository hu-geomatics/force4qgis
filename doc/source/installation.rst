Installation
============

Install QGIS
------------

FORCE4Q is a plugin for QGIS and therefore requires QGIS to operate. You can `download and install QGIS here <https://www.qgis.org/en/site/forusers/download.html>`_.
Additional information on the installation process is provided in the `QGIS Documentation <https://www.qgis.org/en/site/forusers/alldownloads.html>`_.


Install FORCE4Q
---------------
FORCE4Q is a QGIS plugin available in the QGIS plugin repository, directly accessible through QGIS. To install the FORCE4Q plugin

#. Open QGIS and go to :menuselection:`Plugins --> Manage and Install Plugins --> All`
#. In the search bar enter ``FORCE4Q``
#. Now FORCE4Q should be listed in the plugin list:

   Select and click :guilabel:`Install plugin` (or :guilabel:`Upgrade` in case you update to a newer version)
#. Start the plugin via the |icon| icon or from the menubar :menuselection:`Raster --> FORCE4Q`

.. |icon| image:: img/force-icon.svg
   :width: 30px
