Related Links
=============

* `FORCE4Q source code <https://bitbucket.org/hu-geomatics/force4qgis/src/>`_
* `FORCE4Q Issue tracker <https://bitbucket.org/hu-geomatics/force4qgis/issues?status=new&status=open>`_
* `FORCE documentation <https://force-eo.readthedocs.io/en/latest/>`_
* `FORCE source code <https://github.com/davidfrantz/force>`_

.. blank

Project Pages
-------------
* `HU-Berlin Earth Observation Lab <https://www.geographie.hu-berlin.de/en/professorships/eol>`_
* `University of Greifswald - Working Group on Remote Sensing and Geographic Information Processing <https://geo.uni-greifswald.de/lehrstuehle/geographie/fernerkundung-gi/intro/>`_
* `LUMOS project <https://eo.belspo.be/en/stereo-in-action/projects/remote-sensing-image-processing-algorithms-land-use-and-land-cover>`_

How to cite
-----------

Rabe, A., Jakimow, B., Frantz, D., Cooper, S., Thiel, F., Hostert, P., van der Linden, S., 2020.
FORCE4Q - A QGIS plugin for generating higher level EO data products. https://force4q.readthedocs.io.

BibTeX:

.. code-block:: bibtex

   @misc{force4q2020,
   author = {Rabe, A. and Jakimow, B. and Frantz, D. and Cooper, S. and Thiel, F. and Hostert, P. and van der Linden, S.},
   title = {FORCE4Q - A QGIS plugin for generating higher level EO data products},
   year = 2020,
   url = {https://force4q.readthedocs.io}
   }

Related QGIS Plugins
--------------------
Below are a number of open source QGIS plugins which have been developed by the HU Earth Observation Lab
and partners to enhance QGIS to better handle remote sensing data visualization and processing.

* `Bit flag renderer <https://bit-flag-renderer.readthedocs.io/en/latest/>`_: Visualize bit flags in raster quality images.
* `EO Time Series Viewer <https://eo-time-series-viewer.readthedocs.io/en/latest/>`_: Visualize and label raster-based earth observation time series data.
* `EnMAP-Box <https://enmap-box.readthedocs.io/en/latest/index.html>`_: Toolbox and extended GUI for visualising and processing optical remote sensing data
  with a focus on tools commonly needed for hyperspectral image analysis.
* `FORCE4Q <https://force4q.readthedocs.io/en/develop/index.html>`_: Import Landsat and Sentinel-2 bands into analysis-ready images and calculate higher level products.
* `MESMA <https://mesma.readthedocs.io/en/latest/>`_: QGIS implementation of the Multiple Endmember Spectral Mixture Analysis unmixing algorithm.
* `Raster Data Plotting <https://raster-data-plotting.readthedocs.io/en/latest/>`_: Visualize raster data for all pixels currently visible inside the map canvas.
* `Raster Time Series Manager <https://raster-timeseries-manager.readthedocs.io/en/latest/>`_: Navigate through earth observation imagery archives and derived products in space and time.
* `Spectral Library Tool <https://spectral-libraries.readthedocs.io/en/latest/>`_: Suite of processing tools for multi- and hyperspectral spectral libraries.
* `Virtual raster builder <https://virtual-raster-builder.readthedocs.io/en/latest/>`_: Define GDAL Virtual Raster (VRT) files.
* `Zoom View <https://zoomview.readthedocs.io/en/latest/>`_: Adds a Zoom Panel showing a magnification of the map canvas around a selected point.

