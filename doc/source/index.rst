.. FORCE4QG documentation master file, created by
   sphinx-quickstart on Tue Jun 23 14:43:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: img/force4q2.png

|

About FORCE4Q
================
A QGIS plugin for generating higher level EO data products, FORCE4Q is.
It is a QGIS port for the higher level functionality of the `Framework for Operational Radiometric Correction for Environmental Monitoring <https://force-eo.readthedocs.io/en/latest/index.html>`_ (FORCE).
It specifically makes use of FORCE's `Higher Level Processing System <https://force-eo.readthedocs.io/en/latest/components/higher-level/index.html>`_ for generating image composites and time series from
multitemporal Level 2 bottom of atmosphere surface reflectance imagery. FORCE4Q is compatible with Landsat 5-8 and
Sentinel-2 reflectance imagery as downloaded from common online repositories.

FORCE4Q consists of two primary components. The data import function can be used to stack downloaded imagery into analysis
ready data cubes. It further subsets imagery into a tile-based folder structure allowing for efficient storing and computing. The main
FORCE4Q application is a processing wizard enabling rapid and user friendly implementation of FORCE higher level processing
products.

This documentation provides an overview of the functionalities of the FORCE4Q plugin through step-by-step instruction of a basic use case.
For detailed methodological descriptions, please refer to the `Publications <https://force-eo.readthedocs.io/en/latest/refs.html>`_ page of the FORCE documentation.

For any issues, bugs, proposals or remarks, please visit the `issue tracker <https://bitbucket.org/hu-geomatics/force4qgis/issues?status=new&status=open>`_.


Documentation Content
---------------------

.. toctree::
   :maxdepth: 1

   installation.rst
   usingFORCE.rst
   relatedLinks.rst


FORCE4Q  is developed at Humboldt-Universität zu Berlin, `Earth Observation Lab <https://www.geographie.hu-berlin.de/en/professorships/eol>`_
and the Universität Greifswald `Working Group on Remote Sensing and Geographic Information Processing <https://geo.uni-greifswald.de/lehrstuehle/geographie/fernerkundung-gi/intro/>`_
as part of the `Land Use Monitoring System (LUMOS) <https://eo.belspo.be/en/stereo-in-action/projects/remote-sensing-image-processing-algorithms-land-use-and-land-cover>`_
project, funded by the Belgian Science Policy Office as part of the Stereo-III research program (grant no. SR/01/349).

.. csv-table::
   :header-rows: 0

   |hu|, |ug|, |belspo|


.. |hu| image:: img/logo_hu-berlin.svg
   :target: https://www.geographie.hu-berlin.de/en/professorships/eol

.. |ug| image:: img/logo_ug.png
   :target: https://geo.uni-greifswald.de/eo

.. |belspo| image:: img/logo_BELSPO.jpg
   :target: https://eo.belspo.be/en/stereo-in-action/projects/remote-sensing-image-processing-algorithms-land-use-and-land-cover

