from os.path import abspath, relpath, dirname
from pathlib import Path
from shutil import copytree
from typing import List, Tuple


def makeVrtSourcesRelative(vrtFilename: str):
    vrtFilename = abspath(vrtFilename)
    with open(vrtFilename) as file:
        lines = file.readlines()

    for i, line in enumerate(lines):
        if '<SourceFilename relativeToVRT="0">' in line:
            head, tail = line.split('"0"')
            absoluteSourceFilename = tail[1:-18]
            relativeSourceFilename = relpath(absoluteSourceFilename, start=dirname(vrtFilename))
            tail2 = f'>{relativeSourceFilename}</SourceFilename>\n'
            line2 = '"1"'.join((head, tail2))
            lines[i] = line2

    with open(vrtFilename, 'w') as file:
        file.writelines(lines)


def copyOutputFolder(src: str, dst: str, pathmap: List[Tuple[str, str]]):
    '''Copy output folder from source to destination folder and replace all pathes in given pathmap.'''
    src = abspath(src)
    dst = abspath(dst)
    copytree(src, dst)
    for path in Path(dst).rglob('*.vrt'):

        with open(str(path)) as file:
            text = file.read()

        for oldPath, newPath in pathmap:
            text = text.replace(oldPath, newPath)

        with open(str(path), 'w') as file:
            file.write(text)
