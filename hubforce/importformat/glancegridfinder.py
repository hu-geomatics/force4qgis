from os import scandir
from os.path import join, abspath
from typing import List, Iterator, NamedTuple

from osgeo import ogr

from force4qgis.hubforce.core.spatial.coordinatetransformation import CoordinateTransformation
from force4qgis.hubforce.core.spatial.extent import Extent
from force4qgis.hubforce.core.spatial.geometry import Geometry
from force4qgis.hubforce.core.spatial.projection import Projection


class GlanceExtent(NamedTuple):
    zone: str
    x: int
    y: int
    extent: Extent
    projection: Projection


class GlanceGridFinder(object):

    def __init__(self):
        folderGrids = abspath(join(__file__, '..', '..', '..', 'resources', 'glance_grids'))
        self.sources: List[ogr.DataSource] = list()
        self.layers: List[ogr.Layer] = list()
        for entry in scandir(folderGrids):
            if entry.name.endswith('.shp'):
                self.sources.append(ogr.Open(entry.path))
                self.layers.append(self.sources[-1].GetLayer())
        assert len(self.layers) == 7

    def findGridsByGeometry(self, geometry: Geometry, projection: Projection) -> Iterator[GlanceExtent]:

        for layer in self.layers:
            layerProjection = Projection(wkt=layer.GetSpatialRef().ExportToWkt())
            coordinateTransformation = CoordinateTransformation(
                source=projection,
                target=layerProjection
            )
            geometryReprojected = geometry.reproject(coordinateTransformation=coordinateTransformation)
            layer.SetSpatialFilter(geometryReprojected.ogrGeometry)
            feature: ogr.Feature
            for feature in layer:
                isLand = feature.GetField(2) == 1
                if isLand:
                    extent = Extent.fromGeometry(geometry=Geometry(wkt=feature.GetGeometryRef().ExportToWkt()))
                    yield GlanceExtent(
                        zone=layer.GetDescription(), x=feature.GetField(0), y=feature.GetField(1), extent=extent,
                        projection=layerProjection
                    )
