from enum import IntEnum, Enum, auto
from typing import NamedTuple

import numpy as np


class SrAerosol(Enum):
    FillYes = auto()
    FillNo = auto()
    AerosolRetrievalValidYes = auto()
    AerosolRetrievalValidNo = auto()
    AerosolRetrievalInterpolatedYes = auto()
    AerosolRetrievalInterpolatedNo = auto()
    WaterPixelYes = auto()
    WaterPixelNo = auto()
    WaterAerosolRetrievalFailedYes = auto()
    WaterAerosolRetrievalFailedNo = auto()
    NeighborOfFailedAerosolRetrievalYes = auto()
    NeighborOfFailedAerosolRetrievalNo = auto()
    AerosolContentClimatology = auto()
    AerosolContentLow = auto()
    AerosolContentMedium = auto()
    AerosolContentHigh = auto()


class PixelQa(Enum):
    FillYes = auto()
    FillNo = auto()
    ClearYes = auto()
    ClearNo = auto()
    WaterYes = auto()
    WaterNo = auto()
    CloudShadowYes = auto()
    CloudShadowNo = auto()
    SnowYes = auto()
    SnowNo = auto()
    CloudYes = auto()
    CloudNo = auto()
    CloudConfidenceNone = auto()
    CloudConfidenceLow = auto()
    CloudConfidenceMedium = auto()
    CloudConfidenceHigh = auto()
    CirrusConfidenceNone = auto()
    CirrusConfidenceLow = auto()
    CirrusConfidenceMedium = auto()
    CirrusConfidenceHigh = auto()
    TerrainOcclusionYes = auto()
    TerrainOcclusionNo = auto()


class RadsatQa(Enum):
    FillFlagValidData = auto()
    FillFlagInvalidData = auto()
    Band1DataSaturatedYes = auto()
    Band1DataSaturatedNo = auto()
    Band2DataSaturatedYes = auto()
    Band2DataSaturatedNo = auto()
    Band3DataSaturatedYes = auto()
    Band3DataSaturatedNo = auto()
    Band4DataSaturatedYes = auto()
    Band4DataSaturatedNo = auto()
    Band5DataSaturatedYes = auto()
    Band5DataSaturatedNo = auto()
    Band6DataSaturatedYes = auto()
    Band6DataSaturatedNo = auto()
    Band7DataSaturatedYes = auto()
    Band7DataSaturatedNo = auto()
    Band9DataSaturatedYes = auto()
    Band9DataSaturatedNo = auto()
    Band10DataSaturatedYes = auto()
    Band10DataSaturatedNo = auto()
    Band11DataSaturatedYes = auto()
    Band11DataSaturatedNo = auto()


class SrAerosolArray(NamedTuple):
    array: np.ndarray

    def mask(self, flag: SrAerosol) -> np.ndarray:
        assert isinstance(flag, SrAerosol)
        if flag is SrAerosol.FillYes:
            bit, code, mask = 0, 0, 1
        elif flag is SrAerosol.FillNo:
            bit, code, mask = 0, 1, 1
        elif flag is SrAerosol.AerosolRetrievalValidYes:
            bit, code, mask = 1, 0, 1
        elif flag is SrAerosol.AerosolRetrievalValidNo:
            bit, code, mask = 1, 1, 1
        elif flag is SrAerosol.AerosolRetrievalInterpolatedYes:
            bit, code, mask = 2, 0, 1
        elif flag is SrAerosol.AerosolRetrievalInterpolatedNo:
            bit, code, mask = 2, 1, 1
        elif flag is SrAerosol.WaterPixelYes:
            bit, code, mask = 3, 0, 1
        elif flag is SrAerosol.WaterPixelNo:
            bit, code, mask = 3, 1, 1
        elif flag is SrAerosol.WaterAerosolRetrievalFailedYes:
            bit, code, mask = 4, 0, 1
        elif flag is SrAerosol.WaterAerosolRetrievalFailedNo:
            bit, code, mask = 4, 1, 1
        elif flag is SrAerosol.NeighborOfFailedAerosolRetrievalYes:
            bit, code, mask = 5, 0, 1
        elif flag is SrAerosol.NeighborOfFailedAerosolRetrievalNo:
            bit, code, mask = 5, 1, 1
        elif flag is SrAerosol.AerosolContentClimatology:
            bit, code, mask = 6, 0, 3
        elif flag is SrAerosol.AerosolContentLow:
            bit, code, mask = 6, 1, 3
        elif flag is SrAerosol.AerosolContentMedium:
            bit, code, mask = 6, 2, 3
        elif flag is SrAerosol.AerosolContentHigh:
            bit, code, mask = 6, 3, 3
        else:
            assert 0

        return np.right_shift(self.array, bit) & mask == code


class PixelQaArray(NamedTuple):
    array: np.ndarray

    def mask(self, flag: PixelQa) -> np.ndarray:
        assert isinstance(flag, PixelQa)
        if flag is PixelQa.FillNo:
            bit, code, mask = 0, 0, 1
        elif flag is PixelQa.FillYes:
            bit, code, mask = 0, 1, 1
        elif flag is PixelQa.ClearNo:
            bit, code, mask = 1, 0, 1
        elif flag is PixelQa.ClearYes:
            bit, code, mask = 1, 1, 1
        elif flag is PixelQa.WaterNo:
            bit, code, mask = 2, 0, 1
        elif flag is PixelQa.WaterYes:
            bit, code, mask = 2, 1, 1
        elif flag is PixelQa.CloudShadowNo:
            bit, code, mask = 3, 0, 1
        elif flag is PixelQa.CloudShadowYes:
            bit, code, mask = 3, 1, 1
        elif flag is PixelQa.SnowNo:
            bit, code, mask = 4, 0, 1
        elif flag is PixelQa.SnowYes:
            bit, code, mask = 4, 1, 1
        elif flag is PixelQa.CloudNo:
            bit, code, mask = 5, 0, 1
        elif flag is PixelQa.CloudYes:
            bit, code, mask = 5, 1, 1
        elif flag is PixelQa.CloudConfidenceNone:
            bit, code, mask = 6, 0, 3
        elif flag is PixelQa.CloudConfidenceLow:
            bit, code, mask = 6, 1, 3
        elif flag is PixelQa.CloudConfidenceMedium:
            bit, code, mask = 6, 2, 3
        elif flag is PixelQa.CloudConfidenceHigh:
            bit, code, mask = 6, 3, 3
        elif flag is PixelQa.CirrusConfidenceNone:
            bit, code, mask = 8, 0, 3
        elif flag is PixelQa.CirrusConfidenceLow:
            bit, code, mask = 8, 1, 3
        elif flag is PixelQa.CirrusConfidenceMedium:
            bit, code, mask = 8, 2, 3
        elif flag is PixelQa.CirrusConfidenceHigh:
            bit, code, mask = 8, 3, 3
        elif flag is PixelQa.TerrainOcclusionNo:
            bit, code, mask = 10, 0, 1
        elif flag is PixelQa.TerrainOcclusionYes:
            bit, code, mask = 10, 1, 1
        else:
            assert 0

        return (np.right_shift(self.array, bit) & mask == code) * 1
