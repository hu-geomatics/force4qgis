from os import makedirs, scandir
from os.path import exists, join, basename
from typing import Callable
from warnings import warn

import numpy as np
from osgeo import gdal

from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.rasterdriver import RasterDriver
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.importformat.glancegridfinder import GlanceGridFinder
from force4qgis.hubforce.importformat.usgsqa import PixelQaArray, PixelQa, SrAerosolArray, SrAerosol


def importLandsatCollection1(
        folder: str, outputRoot: str, callbackLog: Callable = None
):
    assert isinstance(folder, str)
    assert isinstance(outputRoot, str)
    assert isLandsatCollection1(folder=folder)
    if callbackLog is None:
        callbackLog = print

    if not exists(outputRoot):
        makedirs(outputRoot)
    filenameHead = None
    for entry in scandir(folder):
        if entry.name.endswith('pixel_qa.tif'):
            filenameHead = entry.path.replace('pixel_qa.tif', '')

    assert isinstance(filenameHead, str)

    if basename(folder).startswith('LC08'):
        numbers = 2, 3, 4, 5, 6, 7
    else:
        numbers = 1, 2, 3, 4, 5, 7

    filenameBands = [f'{filenameHead}sr_band{number}.tif' for number in numbers]
    filenamePixelQa = f'{filenameHead}pixel_qa.tif'
    filenameSrAerosol = f'{filenameHead}sr_aerosol.tif'
    tmp = basename(filenameHead).split('_')
    sensor = {'LC08': 'LND08', 'LE07': 'LND07', 'LT05': 'LND05', 'LT04': 'LND04'}[tmp[0]]
    basenameBoa = '_'.join([tmp[3], 'LEVEL2', sensor, 'BOA.tif'])

    # find extents
    grid0 = GdalRaster.open(filenameBands[0]).grid
    glanceExtents = GlanceGridFinder().findGridsByGeometry(geometry=grid0.extent.geometry, projection=grid0.projection)
    for glanceExtent in glanceExtents:

        callbackLog(f'Import {basename(folder)} to {glanceExtent.zone}/X{str(glanceExtent.x).zfill(4)}_Y{str(glanceExtent.y).zfill(4)}')

        # warp and read SR Bands
        boaArray = list()
        grid = Grid(extent=glanceExtent.extent, resolution=Resolution(x=30, y=30), projection=glanceExtent.projection)
        minX, minY, maxX, maxY = grid.extent.xmin, grid.extent.ymin, grid.extent.xmax, grid.extent.ymax
        skip = False
        for i, filenameBand in enumerate(filenameBands):
            ds = gdal.Warp(destNameOrDestDS='', srcDSOrSrcDSTab=filenameBand,
                options=gdal.WarpOptions(
                    format='MEM',
                    outputBounds=(minX, minY, maxX, maxY),
                    width=grid.shape.x,
                    height=grid.shape.y,
                    dstSRS=grid.projection.wkt
                )
            )
            boaArray.append(ds.ReadAsArray())
            if i == 1:
                if np.all(boaArray[-1] == -9999):
                    skip = True
                    break

        if skip:
            continue

        # warp and read PIXEL_QA band
        ds = gdal.Warp(destNameOrDestDS='', srcDSOrSrcDSTab=filenamePixelQa,
            options=gdal.WarpOptions(
                format='MEM',
                outputBounds=(minX, minY, maxX, maxY),
                width=grid.shape.x,
                height=grid.shape.y,
                dstSRS=grid.projection.wkt
            )
        )
        pixelQaArray = ds.ReadAsArray()

        # warp and read SR_AEROSOL band
        if sensor == 'LND08':
            assert exists(filenameSrAerosol)
            ds = gdal.Warp(destNameOrDestDS='', srcDSOrSrcDSTab=filenameSrAerosol,
                options=gdal.WarpOptions(
                    format='MEM',
                    outputBounds=(minX, minY, maxX, maxY),
                    width=grid.shape.x,
                    height=grid.shape.y,
                    dstSRS=grid.projection.wkt
                )
            )
            srAerosolArray = ds.ReadAsArray()
        else:
            srAerosolArray = None

        # write BOA
        outputFolder = join(outputRoot, glanceExtent.zone,
            f'X{str(glanceExtent.x).zfill(4)}_Y{str(glanceExtent.y).zfill(4)}')
        if not exists(outputFolder):
            makedirs(outputFolder)
        filenameBoa = join(outputFolder, basenameBoa)
        boaArray = np.array(boaArray)
        if exists(filenameBoa):
            boaArrayOld = GdalRaster.open(filenameBoa).readAsArray()
            mask = boaArray == -9999
            boaArray[mask] = boaArrayOld[mask]
        driver = RasterDriver.fromFilename(filename=filenameBoa)
        gdalRaster = driver.createFromArray(array=boaArray, grid=grid, filename=filenameBoa)
        gdalRaster.setNoDataValue(-9999)

        # write QAI
        qaiArray = usgsToForceQai(
            pixelQaArray=PixelQaArray(array=pixelQaArray[None]),
            srAerosolArray=SrAerosolArray(array=srAerosolArray),
            boaArray=boaArray
        )
        filenameQai = filenameBoa.replace('BOA.tif', 'QAI.tif')
        driver = RasterDriver.fromFilename(filename=filenameQai)
        if exists(filenameQai):
            qaiArrayOld = GdalRaster.open(filenameQai).readAsArray()
            mask = qaiArray == 1
            qaiArray[mask] = qaiArrayOld[mask]
        gdalRaster = driver.createFromArray(array=qaiArray, grid=grid, filename=filenameQai)
        gdalRaster.setNoDataValue(1)


def isLandsatCollection1(folder: str) -> bool:
    '''Return wether given folder is a valid Landsat Collection 1 Tier 1 Level 2 product,
     e.g. LC080140322019033001T1-SC20190517105817.'''

    assert isinstance(folder, str)
    if len(basename(folder)) != 39:
        return False
    if basename(folder)[0] != 'L':
        return False
    if basename(folder)[20:25] != 'T1-SC':
        return False
    return True


def usgsToForceQai(pixelQaArray: PixelQaArray, srAerosolArray: SrAerosolArray, boaArray: np.ndarray) -> np.ndarray:

    validData = pixelQaArray.mask(flag=PixelQa.FillYes)

    cloudState = np.zeros_like(pixelQaArray.array)  # init as clear
    cloudState[
        np.logical_and(
            pixelQaArray.mask(flag=PixelQa.CloudYes),
            pixelQaArray.mask(flag=PixelQa.CloudConfidenceLow),
        )
    ] = 1  # update less confident cloud
    cloudState[
        np.logical_and(
            pixelQaArray.mask(flag=PixelQa.CloudYes),
            np.logical_or(
                pixelQaArray.mask(flag=PixelQa.CloudConfidenceMedium),
                pixelQaArray.mask(flag=PixelQa.CloudConfidenceHigh),
            )
        )
    ] = 2  # update confident, opaque cloud
    cloudState[
        np.logical_and(
            pixelQaArray.mask(flag=PixelQa.CloudYes),
            np.logical_or(
                pixelQaArray.mask(flag=PixelQa.CirrusConfidenceMedium),
                pixelQaArray.mask(flag=PixelQa.CirrusConfidenceHigh),
            )
        )
    ] = 3  # update cirrus (should also work for Landsat <= 7)

    cloudShadowFlag = pixelQaArray.mask(flag=PixelQa.CloudShadowYes)

    snowFlag = pixelQaArray.mask(flag=PixelQa.SnowYes)

    waterFlag = pixelQaArray.mask(flag=PixelQa.WaterYes)

    if srAerosolArray.array is None:
        aerosolState = np.ones_like(validData)  # for Landsat <=7 set to interpolated (mid quality)
    else:
        aerosolState = np.zeros_like(srAerosolArray.array)  # init as estimated (best quality)
        aerosolState[
            srAerosolArray.mask(flag=SrAerosol.AerosolRetrievalInterpolatedYes)
        ] = 1  # update interpolated (mid quality)
        aerosolState[
            srAerosolArray.mask(flag=SrAerosol.AerosolContentHigh)
        ] = 2  # update high (aerosol optical depth > 0.6, use with caution)
        aerosolState[
            srAerosolArray.mask(flag=SrAerosol.AerosolContentClimatology)
        ] = 3  # update fill (global fallback, low quality)

    subzeroFlag = np.full_like(pixelQaArray.array, fill_value=False, dtype=np.bool)
    saturationFlag = np.full_like(pixelQaArray.array, fill_value=False, dtype=np.bool)

    for a in boaArray:
        subzeroFlag[0][np.logical_and(a != -9999, a < 0)] = True
        saturationFlag[0][np.logical_and(a != -9999, a > 0)] = True

    forceQaiArray = validData.copy()
    forceQaiArray += np.left_shift(cloudState, 1)
    forceQaiArray += np.left_shift(cloudShadowFlag, 3)
    forceQaiArray += np.left_shift(snowFlag, 4)
    forceQaiArray += np.left_shift(waterFlag, 5)
    forceQaiArray += np.left_shift(aerosolState, 6)
    forceQaiArray += np.left_shift(subzeroFlag, 8)
    forceQaiArray += np.left_shift(saturationFlag, 9)

    # mask nodata region
    forceQaiArray[validData == 1] = 1

    return forceQaiArray
