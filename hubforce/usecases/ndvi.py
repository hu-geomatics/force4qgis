"""
Create NDVI collection from given FORCE collection.
"""
import numpy as np

from force4qgis.hubforce.algorithms.composite import meanComposite
from force4qgis.hubforce.core.raster import Resolution, GridShape, Raster
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core import RasterCollection
from hubforce import Qai
from hubforce.algorithms.qaimask import qaiMask
from hubforce.inputformat import forceLevel2Collection


def main():
    # get FORCE L2 collection
    l2Collection = forceLevel2Collection(
        forceLevel2Path=r'C:\Work\data\FORCE\edc\level2',
        tilenames=['X0058_Y0054', 'X0058_Y0055'],
        sensors=['LND08'],
        resolution=Resolution(30, 30)
    )

    # calculate NDVI collection
    ndviCollection = l2Collection.map(function=ndvi)

    # reduce to raster
    ndviTimeseries = ndviCollection.toBands(name='ndvi')
    ndviMean = ndviCollection.reduce(function=meanComposite).rename(name='NDVI_MEAN', bandNames=['NDVI_MEAN'])

    # compute to disk
    result = RasterCollection.fromRasters(rasters=(ndviMean, ndviTimeseries))
    result.compute(dirname='data', extention='.bsq', blockShape=GridShape(x=1000, y=1000), processes=1)


def ndvi(raster: Raster) -> Raster:
    qaiFlags = [Qai.ValidNo, Qai.CloudLessConfident, Qai.CloudConfident, Qai.CloudCirrus, Qai.CloudShadowYes]
    qai = raster.select(['QAI'])
    mask = qaiMask(qai, flags=qaiFlags)
    ndvi = raster.select(['NIR', 'RED']).setMask(mask).expression(
        expression='np.clip((NIR - RED) / (NIR + RED) * 1e4, -1e4, 1e4).astype(np.int16)',
        name=f'{raster.name}_NDVI',
        outType=np.int16,
        outNoDataValue=-1e4
    )
    return ndvi

if __name__ == '__main__':
    with AutoFilenames(dir='data/tmp'):
        main()
