import pickle
import numpy as np
from hubdc.core import RasterDataset

if __name__ == '__main__':

    # unpickle
    features = pickle.load(open('data_ao_ref/features.pkl', "rb"))
    labels = pickle.load(open('data_ao_ref/labels.pkl', "rb"))

    # pseudo image mit features und mit fractions (l1_veg bis l4_bac)
    # -> Koordinaten kommen aus 'id_ref'
    RasterDataset.fromArray(array=np.atleast_3d(features), filename='data_ao_ref/features.bsq')
    for name in ['l1_veg', 'l1_bac', 'l2_wve', 'l2_nvw', 'l2_bac', 'l3_tre', 'l3_shr', 'l3_gra', 'l3_bac', 'l4_con', 'l4_bro', 'l4_shr', 'l4_gra', 'l4_bac']:
        RasterDataset.fromArray(array=np.atleast_3d(labels[name]), filename=f'data_ao_ref/{name}.bsq')
