import pickle
import time
from multiprocessing.pool import Pool
from os import listdir
from os.path import join
from random import randint
import numpy as np

from force4qgis.hubforce.core.raster.gdalband import GdalBand
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.inputformat.forceraster import forceRaster

debug = True


def main(tilename):
    tilenames = [tilename]
    shapefile = r"\\141.20.140.91\SAN_GS2\_EnMAP\temp\temp_akpona\01_calimix\01_data\01_lib_shp\lib_shp_ba.shp"
    shapefile = r"\\141.20.140.91\san_gs2\_EnMAP\temp\temp_akpona\01_calimix\01_data\03_ref_shp\ref_shp_ba.shp"


    # tilenames = ['X0056_Y0041', 'X0056_Y0042']

    # open raster
    print('prepare raster')

    def bandNameFunc(gdalBand: GdalBand) -> str:
        return f'B{gdalBand.number}'

    rasters = list()

    # - TSI raster
    if True:
        bandList = list(range(1, 10 + 1))  # 2018 -> B159 B213
        forceRasterPath = r'\\141.20.140.91\san_gs2\_EnMAP\Project_California\00_Data\01_EnMAP\01_L2SIM_ENMAP_BA'

        basenames = (
            '2013FA_BA_ENMAP_L2SIM.bsq',
            '2013SU_BA_ENMAP_L2SIM.bsq',
            '2013SP_BA_ENMAP_L2SIM.bsq'
        )
        for basename in basenames:
            raster = forceRaster(
                forceRasterPath=forceRasterPath,
                basename=basename,
                tilenames=tilenames,
                resolution=Resolution(30, 30),
                bandList=bandList,
                bandNameFunc=bandNameFunc,
                debug=debug
            )
            rasters.append(raster)

    raster = RasterCollection.fromRasters(rasters=rasters, gridTol=1e-1).toBands()
    print(raster.bandNames)

    # sample points
    #print('sample')
    features, labels = raster.sampleFeatures(filename=shapefile, allTouched=False, debug=True)

    return features, labels


if __name__ == '__main__':

    # find tilenames
    #path = r'\\141.20.140.91\san_gs2\_EnMAP\Project_California\00_Data\01_EnMAP\01_L2SIM_ENMAP_BA'
    #print("', '".join([d for d in listdir(path)]))

    tilenames = ['X0001_Y0001', 'X0001_Y0002', 'X0001_Y0003', 'X0002_Y0001', 'X0002_Y0002', 'X0002_Y0003',
                 'X0002_Y0004', 'X0002_Y0005', 'X0003_Y0001', 'X0003_Y0002', 'X0003_Y0003', 'X0003_Y0004',
                 'X0003_Y0005', 'X0003_Y0006', 'X0003_Y0007', 'X0004_Y0001', 'X0004_Y0002', 'X0004_Y0003',
                 'X0004_Y0004', 'X0004_Y0005', 'X0004_Y0006', 'X0004_Y0007', 'X0004_Y0008', 'X0005_Y0001',
                 'X0005_Y0002', 'X0005_Y0003', 'X0005_Y0004', 'X0005_Y0005', 'X0005_Y0006', 'X0005_Y0007',
                 'X0005_Y0008', 'X0005_Y0009', 'X0005_Y0010', 'X0006_Y0002', 'X0006_Y0003', 'X0006_Y0004',
                 'X0006_Y0005', 'X0006_Y0006', 'X0006_Y0007', 'X0006_Y0008', 'X0006_Y0009', 'X0006_Y0010',
                 'X0007_Y0004', 'X0007_Y0005', 'X0007_Y0006', 'X0007_Y0007', 'X0007_Y0008', 'X0008_Y0006',
                 'X0008_Y0007'][:2]

    pool = Pool()
    t0 = time.time()
    result = pool.map(func=main, iterable=tilenames)
    features = np.hstack([r[0] for r in result])
    labels = {k: np.hstack([r[1][k] for r in result]) for k in result[0][1].keys()}
    print(round(time.time() - t0), 'sec')

    # pickle result
    pickle.dump(features, open('data_ao_ref/features.pkl', "wb"))
    pickle.dump(labels, open('data_ao_ref/labels.pkl', "wb"))

    # unpickle
#    features = pickle.load(open('data_ao/features.pkl.', "rb"))
#    labels = pickle.load(open('data_ao/labels.pkl.', "rb"))
