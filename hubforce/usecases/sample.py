"""
Create NDVI collection from given FORCE collection.
"""
import pickle
from os import makedirs
from os.path import exists, join

from sklearn.ensemble import RandomForestClassifier

from hubforce.algorithms.composite import meanComposite
from force4qgis.hubforce.core import Resampling
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core import DateRange
from force4qgis.hubforce.core import Raster
from force4qgis.hubforce.core import Resolution
from force4qgis.hubforce.core import GridShape
from hubforce import Qai
from hubforce.algorithms.qaimask import qaiMask
from hubforce.inputformat import forceLevel2Collection


def mask(raster: Raster) -> Raster:
    qaiFlags = [Qai.ValidNo, Qai.CloudLessConfident, Qai.CloudConfident, Qai.CloudCirrus, Qai.CloudShadowYes]
    qai = raster.select(['QAI'])
    mask = qaiMask(qai, flags=qaiFlags)
    return raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).setMask(mask)


def main():
    bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']
    tilenames = ['X0058_Y0054','X0058_Y0055']
    pkl = 'data/pkl'
    estimatorFilename = join(pkl, 'rfc.pkl')

    if not exists(pkl):
        # get wavebands and derive features
        boaCollection = (
            forceLevel2Collection(
                forceLevel2Path=r'C:\Work\data\FORCE\edc\level2',
                tilenames=tilenames,
                sensors=['LND07', 'LND08', 'SEN2A', 'SEN2B'],
                resolution=Resolution(30, 30),
                boaResampling=Resampling.NearestNeighbour,
                qaiResampling=Resampling.NearestNeighbour
            )
                .filterDate(dateRange=DateRange.Month(2017, 7))
                .map(function=mask)
        )

        mean = (
            boaCollection
                .reduce(function=meanComposite)
                .rename(name='MEAN', bandNames=[f'MEAN_{bandName}' for bandName in bandNames])
        )

        # sample points
        features, labels = mean.sampleFeatures(filename=r'C:\Work\data\gms\lucas\eu27_lucas_2012_subset1.shp')

        # fit and precict rfc
        X = features.T
        y = labels['LC6_ID']
        rfc = RandomForestClassifier(n_estimators=10)
        rfc.fit(X=X, y=y)
        makedirs(pkl)
        pickle.dump(mean, open(join(pkl, 'mean.pkl'), 'wb'))
        pickle.dump(rfc, open(estimatorFilename, 'wb'))
    else:
        mean = pickle.load(open(join(pkl, 'mean.pkl'), 'rb'))
        rfc = pickle.load(open(estimatorFilename, 'rb'))

    classification = mean.predict(estimatorFilename=estimatorFilename, name='classification', bandNames=['classification'])
    probability = mean.predictProbability(estimatorFilename=estimatorFilename, name='probability', bandNames=['artificial land', 'cropland', 'forest broadleaved', 'forest coniferous', 'forest mixed', 'None', 'grassland'])

    # compute to disk

    classification.compute(dirname='data', extention='.tif', blockShape=GridShape(x=1000, y=1000), processes=1)
    #probability.compute(dirname='data', extention='.tif', blockShape=GridShape(x=1000, y=1000), processes=1)


if __name__ == '__main__':

    with AutoFilenames(dir='data/tmp', cleanup=not True):
        main()

