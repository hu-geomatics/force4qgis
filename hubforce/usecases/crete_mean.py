"""
Create NDVI collection from given FORCE collection.
"""
import numpy as np

from hubforce.algorithms.composite import meanComposite
from force4qgis.hubforce.core import Resampling
from force4qgis.hubforce.core.delayed.autofilename import AutoFilenames
from force4qgis.hubforce.core import DateRange
from force4qgis.hubforce.core import Raster
from force4qgis.hubforce.core import RasterCollection
from force4qgis.hubforce.core import Resolution
from force4qgis.hubforce.core import GridShape
from hubforce.algorithms.qaimask import qaiMask
from force4qgis.hubforce.core import Qai
from hubforce.inputformats.forcelevel2collection import forceLevel2Collection


def mask(raster: Raster) -> Raster:
    qaiFlags = [Qai.ValidNo, Qai.CloudLessConfident, Qai.CloudConfident, Qai.CloudCirrus, Qai.CloudShadowYes]
    qai = raster.select(['QAI'])
    mask = qaiMask(qai, flags=qaiFlags)
    return raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).setMask(mask)


# Normalized Difference Vegetation Index
# 'https://www.indexdatabase.de/db/i-single.php?id=58'
def addNdvi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED']).expression(
        expression='np.clip((NIR - RED) / (NIR + RED) * 1e4, -1e4, 1e4).astype(np.int16)',
        name='NDVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Enhanced Vegetation Index
# 'https://www.indexdatabase.de/db/i-single.php?id=16'
def addEvi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED', 'BLUE']).expression(
        expression='np.clip(2.5 * ((NIR - RED) / (NIR + 6 * RED - 7.5 * BLUE + 1e4)), -1e4, 1e4).astype(np.int16)',
        name='EVI',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Normalized Burn Ratio
# 'https://www.indexdatabase.de/db/i-single.php?id=16'
def addNbr(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'SWIR1']).expression(
        expression='np.clip((NIR - SWIR1) / (NIR + SWIR1) * 1e4, -1e4, 1e4).astype(np.int16)',
        name='NBR',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Soil Adjusted Vegetation Index
# https://www.indexdatabase.de/db/i-single.php?id=87
def addSavi(raster: Raster) -> Raster:
    feature = raster.select(['NIR', 'RED']).expression(
        expression='np.clip((NIR - RED) / (NIR + RED + 0.5) * 1.5 * 1e4, -1e4, 1e4).astype(np.int16)',
        name='NBR',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=feature)


# Tasselled Cap - brightness
def addTcb(raster: Raster) -> Raster:
    tcb = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='hubforce.usecases.crete_mean.tcbFormular',
        name='TCB',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcb)


# Tasselled Cap - greenness
def addTcg(raster: Raster) -> Raster:
    tcg = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='hubforce.usecases.crete_mean.tcgFormular',
        name='TCG',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcg)


# Tasselled Cap - wetness
def addTcw(raster: Raster) -> Raster:
    tcw = raster.select(['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']).expression(
        expression='hubforce.usecases.crete_mean.tcwFormular',
        name='TCW',
        outType=np.int16,
        outNoDataValue=np.iinfo(np.int16).min
    )
    return raster.addBands(raster=tcw)


def tcbFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [0.2043, 0.4158, 0.5524, 0.5741, 0.3124, 0.2303]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def tcgFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [-0.1603, -0.2819, -0.4934, 0.7940, 0.0002, 0.1446]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def tcwFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [0.0315, 0.2021, 0.3102, 0.1594, -0.6806, -0.6109]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff):
    TCB = np.multiply(BLUE, coeff[0], dtype=np.float32)
    for b, c in zip((GREEN, RED, NIR, SWIR1, SWIR2), coeff[1:]):
        TCB += np.multiply(b, c, dtype=np.float32)
    return TCB


def main():
    bandNames = ['BLUE', 'GREEN', 'RED', 'NIR', 'SWIR1', 'SWIR2']
    featureNames = ['NDVI', 'EVI', 'NBR', 'SAVI', 'TCB', 'TCG', 'TCW']


    tilenames = ['X0058_Y0054','X0058_Y0055']

    #with open('germany.til') as file:
    #    tilenames = file.readlines()
    #tilenames = [t.strip() for t in tilenames[1:10]]

    # get wavebands and derive features
    boaCollection = (
        forceLevel2Collection(
            forceLevel2Path=r'C:\Work\data\FORCE\edc\level2',
            tilenames=tilenames,
            sensors=['LND07', 'LND08', 'SEN2A', 'SEN2B'],
            resolution=Resolution(30, 30),
            boaResampling=Resampling.NearestNeighbour,
            qaiResampling=Resampling.NearestNeighbour
        )
            .filterDate(dateRange=DateRange.Month(2017, 7))
            .map(function=mask)
            .map(function=addNdvi)
            .map(function=addEvi)
            .map(function=addNbr)
            .map(function=addSavi)
            .map(function=addTcb)
            .map(function=addTcg)
            .map(function=addTcw)
    )

    # reduce to raster
    # timeseries = boaCollection.toBands().rename(name='TS')
    # min = (
    #     boaCollection
    #         .reduce(function=minComposite)
    #         .rename(name='MIN', bandNames=[f'MIN_{bandName}' for bandName in bandNames + featureNames])
    # )
    mean = (
        boaCollection
            .reduce(function=meanComposite)
            .rename(name='MEAN', bandNames=[f'MEAN_{bandName}' for bandName in bandNames + featureNames])
    )
    # max = (
    #     boaCollection
    #         .reduce(function=maxComposite)
    #         .rename(name='MAX', bandNames=[f'MAX_{bandName}' for bandName in bandNames + featureNames])
    # )

    # compute to disk
    # result = RasterCollection.fromRasters(rasters=(timeseries, min, mean, max))
    result = RasterCollection.fromRasters(rasters=[mean])  # , min, mean, max))

    result.compute(dirname='data', extention='.bsq', blockShape=GridShape(x=1000, y=1000), processes=1)


if __name__ == '__main__':

    with AutoFilenames(dir='data/tmp', cleanup=True):
        main()
