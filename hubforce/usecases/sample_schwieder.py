import time
from multiprocessing.pool import Pool
from os.path import join
from random import randint
import numpy as np

from force4qgis.hubforce.core.raster.gdalband import GdalBand
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.inputformat.forceraster import forceRaster

debug = True


def main(tilename):
    tilenames = [tilename]
    shapefile = r'C:\Work\data\TEST_SAMPLING\sample_2017_substratified_1000_train.shp'


    # tilenames = ['X0056_Y0041', 'X0056_Y0042']

    # open raster
    print('prepare raster')

    def bandNameFunc(gdalBand: GdalBand) -> str:
        return f'B{gdalBand.number}'

    rasters = list()

    # - TSI raster
    if True:
        bandList = list(range(86, 140 + 1))  # 2018 -> B159 B213
        forceRasterPath = r'\\141.20.140.91\NAS_Rodinia\Croptype'
        basenames = (
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_BLU_TSI.tif',
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_GRN_TSI.tif',
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_RED_TSI.tif',
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_NIR_TSI.tif',
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_SW1_TSI.tif',
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_SW2_TSI.tif',
            '2016-2019_001-365_LEVEL4_TSA_LNDLG_NDV_TSI.tif',
        )
        for basename in basenames:
            raster = forceRaster(
                forceRasterPath=forceRasterPath,
                basename=basename,
                tilenames=tilenames,
                resolution=Resolution(30, 30),
                bandList=bandList,
                bandNameFunc=bandNameFunc,
                debug=debug
            )
            rasters.append(raster)

    # - env vars
    if True:
        forceRasterPath = r'\\141.20.140.91\NAS_Rodinia\Croptype\envVars'
        basenames = (
                        'aspect_10m.tif', 'CECSOL_M_sl1_10m.tif', 'grids_germany_monthly_soil_moist_201701_10m.tif',
                        'grids_germany_monthly_soil_moist_201702_10m.tif',
                        'grids_germany_monthly_soil_moist_201703_10m.tif',
                        'grids_germany_monthly_soil_moist_201704_10m.tif',
                        'grids_germany_monthly_soil_moist_201705_10m.tif',
                        'grids_germany_monthly_soil_moist_201706_10m.tif',
                        'grids_germany_monthly_soil_moist_201707_10m.tif',
                        'grids_germany_monthly_soil_moist_201708_10m.tif',
                        'grids_germany_monthly_soil_moist_201709_10m.tif',
                        'grids_germany_monthly_soil_moist_201710_10m.tif',
                        'grids_germany_monthly_soil_moist_201711_10m.tif',
                        'grids_germany_monthly_soil_moist_201712_10m.tif',
                        'grids_germany_monthly_soil_moist_201801_10m.tif',
                        'grids_germany_monthly_soil_moist_201802_10m.tif',
                        'grids_germany_monthly_soil_moist_201803_10m.tif',
                        'grids_germany_monthly_soil_moist_201804_10m.tif',
                        'grids_germany_monthly_soil_moist_201805_10m.tif',
                        'grids_germany_monthly_soil_moist_201806_10m.tif',
                        'grids_germany_monthly_soil_moist_201807_10m.tif',
                        'grids_germany_monthly_soil_moist_201808_10m.tif',
                        'grids_germany_monthly_soil_moist_201809_10m.tif',
                        'grids_germany_monthly_soil_moist_201810_10m.tif',
                        'grids_germany_monthly_soil_moist_201811_10m.tif',
                        'grids_germany_monthly_soil_moist_201812_10m.tif', 'ORCDRC_M_sl1_10m.tif',
                        'PHIKCL_M_sl1_10m.tif',
                        'rad_2017_01_10m.tif', 'rad_2017_02_10m.tif', 'rad_2017_03_10m.tif', 'rad_2017_04_10m.tif',
                        'rad_2017_05_10m.tif', 'rad_2017_06_10m.tif', 'rad_2017_07_10m.tif', 'rad_2017_08_10m.tif',
                        'rad_2017_09_10m.tif', 'rad_2017_10_10m.tif', 'rad_2017_11_10m.tif', 'rad_2017_12_10m.tif',
                        'rad_2018_01_10m.tif', 'rad_2018_02_10m.tif', 'rad_2018_03_10m.tif', 'rad_2018_04_10m.tif',
                        'rad_2018_05_10m.tif', 'rad_2018_06_10m.tif', 'rad_2018_07_10m.tif', 'rad_2018_08_10m.tif',
                        'rad_2018_09_10m.tif', 'rad_2018_10_10m.tif', 'rad_2018_11_10m.tif', 'rad_2018_12_10m.tif',
                        'RSMS_13_1981_30_10m.tif', 'RSMS_14_1981_30_10m.tif', 'RSMS_15_1981_30_10m.tif',
                        'RSMS_16_1981_30_10m.tif',
                        'slope_10m.tif', 'srtm_10m.tif', 'TAMM_01_2017_01_10m.tif', 'TAMM_01_2018_01_10m.tif',
                        'TAMM_02_2017_01_10m.tif', 'TAMM_02_2018_01_10m.tif', 'TAMM_03_2017_01_10m.tif',
                        'TAMM_03_2018_01_10m.tif',
                        'TAMM_04_2017_01_10m.tif', 'TAMM_04_2018_01_10m.tif', 'TAMM_05_2017_01_10m.tif',
                        'TAMM_05_2018_01_10m.tif',
                        'TAMM_06_2017_01_10m.tif', 'TAMM_06_2018_01_10m.tif', 'TAMM_07_2017_01_10m.tif',
                        'TAMM_07_2018_01_10m.tif',
                        'TAMM_08_2017_01_10m.tif', 'TAMM_08_2018_01_10m.tif', 'TAMM_09_2017_01_10m.tif',
                        'TAMM_09_2018_01_10m.tif',
                        'TAMM_10_2017_01_10m.tif', 'TAMM_10_2018_01_10m.tif', 'TAMM_11_2017_01_10m.tif',
                        'TAMM_11_2018_01_10m.tif',
                        'TAMM_12_2017_01_10m.tif', 'TAMM_12_2018_01_10m.tif', 'TAMM_13_1981_30_10m.tif',
                        'TAMM_14_1981_30_10m.tif',
                        'TAMM_15_1981_30_10m.tif', 'TAMM_16_1981_30_10m.tif'
                    )[:3]
        for basename in basenames:
            raster = forceRaster(
                forceRasterPath=forceRasterPath,
                basename=basename,
                tilenames=tilenames,
                resolution=Resolution(30, 30),
                bandNameFunc=bandNameFunc,
                debug=debug
            )
            rasters.append(raster)

    raster = RasterCollection.fromRasters(rasters=rasters, gridTol=1e-1).toBands()
    print(raster.bandNames)

    # sample points
    print('sample')
    features, labels = raster.sampleFeatures(filename=shapefile, debug=True)

    print(features.shape)
    print(features)
    print(labels['class'].shape)
    print(labels['class'])

    return features, labels


if __name__ == '__main__':

    tilenames = ['X0053_Y0051', 'X0053_Y0052', 'X0053_Y0053', 'X0053_Y0054', 'X0054_Y0043', 'X0054_Y0051',
                 'X0054_Y0052', 'X0054_Y0053', 'X0054_Y0054', 'X0055_Y0039', 'X0055_Y0040', 'X0055_Y0041',
                 'X0055_Y0042', 'X0055_Y0043', 'X0055_Y0044', 'X0055_Y0050', 'X0055_Y0051', 'X0055_Y0052',
                 'X0055_Y0053', 'X0055_Y0054', 'X0055_Y0055', 'X0056_Y0039', 'X0056_Y0040', 'X0056_Y0041',
                 'X0056_Y0042', 'X0056_Y0043', 'X0056_Y0044', 'X0056_Y0050', 'X0056_Y0051', 'X0056_Y0052',
                 'X0056_Y0053', 'X0056_Y0054', 'X0056_Y0055', 'X0057_Y0039', 'X0057_Y0040', 'X0057_Y0041',
                 'X0057_Y0042', 'X0057_Y0043', 'X0057_Y0044', 'X0057_Y0045', 'X0057_Y0050', 'X0057_Y0051',
                 'X0057_Y0052', 'X0057_Y0053', 'X0057_Y0054', 'X0057_Y0055', 'X0057_Y0056', 'X0058_Y0039',
                 'X0058_Y0040', 'X0058_Y0041', 'X0058_Y0042', 'X0058_Y0043', 'X0058_Y0044', 'X0058_Y0052',
                 'X0058_Y0053', 'X0058_Y0054', 'X0058_Y0055', 'X0058_Y0056', 'X0059_Y0036', 'X0059_Y0038',
                 'X0059_Y0039', 'X0059_Y0040', 'X0059_Y0041', 'X0059_Y0042', 'X0059_Y0043', 'X0059_Y0044',
                 'X0060_Y0038', 'X0060_Y0039', 'X0060_Y0040', 'X0060_Y0041', 'X0060_Y0042', 'X0060_Y0043',
                 'X0060_Y0044', 'X0060_Y0045', 'X0060_Y0046', 'X0061_Y0039', 'X0061_Y0040', 'X0061_Y0041',
                 'X0061_Y0042', 'X0061_Y0043', 'X0061_Y0044', 'X0061_Y0045', 'X0061_Y0046', 'X0061_Y0047',
                 'X0062_Y0040', 'X0062_Y0041', 'X0062_Y0042', 'X0062_Y0043', 'X0062_Y0044', 'X0062_Y0045',
                 'X0062_Y0046', 'X0062_Y0047', 'X0063_Y0038', 'X0063_Y0039', 'X0063_Y0040', 'X0063_Y0041',
                 'X0063_Y0042', 'X0063_Y0043', 'X0063_Y0044', 'X0063_Y0045', 'X0063_Y0046', 'X0063_Y0047',
                 'X0063_Y0051', 'X0064_Y0038', 'X0064_Y0039', 'X0064_Y0040', 'X0064_Y0041', 'X0064_Y0042',
                 'X0064_Y0043', 'X0064_Y0044', 'X0064_Y0045', 'X0064_Y0046', 'X0064_Y0048', 'X0065_Y0037',
                 'X0065_Y0038', 'X0065_Y0039', 'X0065_Y0040', 'X0065_Y0041', 'X0065_Y0042', 'X0065_Y0043',
                 'X0065_Y0044', 'X0065_Y0045', 'X0065_Y0046', 'X0065_Y0047', 'X0066_Y0037', 'X0066_Y0038',
                 'X0066_Y0039', 'X0066_Y0040', 'X0066_Y0041', 'X0066_Y0042', 'X0067_Y0036', 'X0067_Y0037',
                 'X0067_Y0038', 'X0067_Y0039', 'X0067_Y0040', 'X0067_Y0041', 'X0067_Y0042', 'X0067_Y0043',
                 'X0067_Y0044', 'X0067_Y0045', 'X0068_Y0035', 'X0068_Y0036', 'X0068_Y0037', 'X0068_Y0038',
                 'X0068_Y0039', 'X0068_Y0040', 'X0068_Y0041', 'X0068_Y0042', 'X0068_Y0043', 'X0068_Y0044',
                 'X0068_Y0045', 'X0069_Y0035', 'X0069_Y0036', 'X0069_Y0037', 'X0069_Y0038', 'X0069_Y0039',
                 'X0069_Y0040', 'X0069_Y0041', 'X0069_Y0042', 'X0069_Y0043', 'X0069_Y0044', 'X0069_Y0045',
                 'X0069_Y0046', 'X0069_Y0047', 'X0070_Y0036', 'X0070_Y0037', 'X0070_Y0038', 'X0070_Y0039',
                 'X0070_Y0040', 'X0070_Y0041', 'X0070_Y0042', 'X0070_Y0043', 'X0070_Y0044', 'X0070_Y0045',
                 'X0070_Y0046', 'X0070_Y0047', 'X0071_Y0037', 'X0071_Y0038', 'X0071_Y0039', 'X0071_Y0040',
                 'X0071_Y0041', 'X0071_Y0042', 'X0071_Y0043', 'X0071_Y0044', 'X0071_Y0045', 'X0071_Y0046',
                 'X0071_Y0047', 'X0072_Y0042', 'X0072_Y0043', 'X0072_Y0044', 'X0072_Y0045', 'X0072_Y0046',
                 'X0073_Y0046'][:2]

    pool = Pool()
    t0 = time.time()
    result = pool.map(func=main, iterable=tilenames)
    features = np.hstack([r[0] for r in result])
    labels = {k: np.hstack([r[1][k] for r in result]) for k in result[0][1].keys()}
    print(round(time.time() - t0), 'sec')

    np.savetxt('data/features.txt', features.T, fmt='%i', delimiter=', ')
    for name, label in labels.items():
        np.savetxt(join('data', name+'.txt'), label, fmt='%i', delimiter=', ')


# 170 sec für 1 Band alle Tiles