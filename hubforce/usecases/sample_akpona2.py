import pickle
import numpy as np

if __name__ == '__main__':

    # unpickle
    features = pickle.load(open('data_ao/features.pkl', "rb"))
    labels = pickle.load(open('data_ao/labels.pkl', "rb"))

    full_ids = set(labels['Full_ID'])
    means = list()
    for full_id in full_ids:
        profiles = list()
        for profile, id in zip(features.T, labels['Full_ID']):
            if id == full_id:
                profiles.append(profile)
        means.append(np.mean(profiles, axis=0))

    from hubflow.core import EnviSpectralLibrary, Raster

    raster = Raster.fromArray(filename='/vsimem/tmp.bsq', array=np.atleast_3d(features))
    EnviSpectralLibrary.fromRaster(filename='data_ao/speclib.sli', raster=raster)

    # speclib aller profiles und mean profilesProPolygon

    # spectra names = Full_ID
    # fields -> 'OrigCla', 'level1_lab' to 'level5_lab'