from dataclasses import dataclass

from osgeo import ogr


@dataclass
class FieldDefinition(object):
    ogrFieldDefn: ogr.FieldDefn

    def __post_init__(self):
        assert isinstance(self.ogrFieldDefn, ogr.FieldDefn)

    @property
    def name(self) -> str:
        """Return field name."""
        return self.ogrFieldDefn.name

    @property
    def type(self) -> int:
        """Return OGR field type (e.g. ogr.OFTReal)."""
        return self.ogrFieldDefn.type
