# from __future__ import annotations

from dataclasses import dataclass
from os.path import exists
from typing import Union, Iterable

from osgeo import ogr, gdal

from force4qgis.hubforce.core.spatial.projection import Projection
from force4qgis.hubforce.core.vector.layer import Layer


@dataclass
class Vector(object):
    """Vector dataset."""
    gdalSource: Union[ogr.DataSource, gdal.Dataset]

    def __post_init__(self):
        assert isinstance(self.gdalSource, (ogr.DataSource, gdal.Dataset))

    @classmethod
    def fromFilename(cls, filename) -> 'Vector':
        """Open vector dataset."""
        assert isinstance(filename, str)
        assert exists(filename) or filename.startswith('/vsimem/'), filename
        gdalDataset = gdal.OpenEx(filename, gdal.OF_VECTOR)
        assert gdalDataset is not None
        return Vector(gdalSource=gdalDataset)

    open = fromFilename

    @property
    def filename(self) -> str:
        """Return filename."""
        return self.gdalSource.GetDescription()

    @property
    def driver(self):
        """Return vector driver."""
        from force4qgis.hubforce.core.vector.vectordriver import VectorDriver
        ogrDriver = self.gdalSource.GetDriver()
        assert ogrDriver is not None
        return VectorDriver(name=ogrDriver.ShortName)

    def layer(self, indexOrName: Union[int, str] = 0) -> Layer:
        """Return vector layer."""
        if isinstance(indexOrName, int):
            ogrLayer = self.gdalSource.GetLayerByIndex(indexOrName)
        elif isinstance(indexOrName, str):
            ogrLayer = self.gdalSource.GetLayerByName(indexOrName)
        else:
            raise TypeError()
        assert ogrLayer is not None
        return Layer(ogrLayer=ogrLayer, gdalSource=self.gdalSource)

    def layers(self) -> Iterable[Layer]:
        for i in range(self.gdalSource.GetLayerCount()):
            yield self.layer(i)

    def createLayer(self, name='layer', projection=None) -> Layer:
        """Create layer."""
        if projection is None:
            projection = Projection.fromWgs84()
        assert isinstance(name, str)
        assert isinstance(projection, Projection)
        ogrLayer = self.gdalSource.CreateLayer(name, srs=projection.osrSpatialReference)
        return Layer(ogrLayer=ogrLayer, gdalSource=self.gdalSource)
