from dataclasses import dataclass
from typing import Any

from osgeo import ogr

from force4qgis.hubforce.core.spatial.geometry import Geometry


@dataclass
class Feature(object):
    ogrFeature: ogr.Feature

    def __post_init__(self):
        assert isinstance(self.ogrFeature, ogr.Feature)

    def field(self, name: str) -> Any:
        return self.ogrFeature.GetField(name)

    def fields(self):
        fields = {fd.name: self.field(fd.name) for fd in self.featureDefinition().fieldDefinitions}
        return fields

    def featureDefinition(self) -> 'FeatureDefinition':
        from force4qgis.hubforce.core.vector.featuredefinition import FeatureDefinition
        ogrFeatureDefn = self.ogrFeature.GetDefnRef()
        return FeatureDefinition(ogrFeatureDefn=ogrFeatureDefn)

    @property
    def geometry(self) -> Geometry:
        ogrGeometry: ogr.Geometry = self.ogrFeature.GetGeometryRef()
        wkt = ogrGeometry.ExportToWkt()
        return Geometry(wkt=wkt)
