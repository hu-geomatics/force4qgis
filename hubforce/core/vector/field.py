from dataclasses import dataclass

from osgeo import ogr


@dataclass
class Field(object):
    """Vector field."""
    ogrLayer: ogr.Layer

    def __post_init__(self):
        from force4qgis.hubforce.core import Vector
        assert isinstance(self.vectorDataset, Vector), repr(self.vectorDataset)