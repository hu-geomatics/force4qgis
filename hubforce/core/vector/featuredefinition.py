from dataclasses import dataclass
from typing import List

from osgeo import ogr

from force4qgis.hubforce.core.vector.fielddefinition import FieldDefinition


@dataclass
class FeatureDefinition(object):
    """Vector layer."""
    ogrFeatureDefn: ogr.FeatureDefn

    def __post_init__(self):
        assert isinstance(self.ogrFeatureDefn, ogr.FeatureDefn)

    @property
    def fieldCount(self) -> int:
        """Return field names."""
        return self.ogrFeatureDefn.GetFieldCount()

    def fieldDefinition(self, i) -> FieldDefinition:
        """Return i-th field definition."""
        ogrFieldDefn = self.ogrFeatureDefn.GetFieldDefn(i)
        return FieldDefinition(ogrFieldDefn=ogrFieldDefn)

    @property
    def fieldDefinitions(self) -> List[FieldDefinition]:
        """Return field names."""
        return [self.fieldDefinition(i) for i in range(self.fieldCount)]