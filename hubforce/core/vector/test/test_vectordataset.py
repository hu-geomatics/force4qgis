from unittest import TestCase

from force4qgis.hubforce.core import Vector
from force4qgis.hubforce.core import VectorDriver
from force4qgis.hubforce.core import Wgs84


class Test(TestCase):

    def setUp(self) -> None:
        self.filename = '/vsimem/vector.gpkg'
        self.driver = VectorDriver.fromFilename(filename=self.filename)
        self.driver.create(filename=self.filename, projection=Wgs84())

    def test_OpenLayer(self):
        vectorDataset = Vector.fromFilename(filename=self.filename)
        self.assertIsInstance(vectorDataset, Vector)

    def tearDown(self) -> None:
        self.driver.delete(filename=self.filename)
