# from __future__ import annotations
from collections import OrderedDict
from dataclasses import dataclass
from typing import List, Any, Dict, Union

from osgeo import ogr, gdal

from force4qgis.hubforce.core.spatial.geometry import Geometry
from force4qgis.hubforce.core.spatial.projection import Projection
from force4qgis.hubforce.core.vector.feature import Feature
from force4qgis.hubforce.core.vector.featuredefinition import FeatureDefinition
from force4qgis.hubforce.core.vector.fielddefinition import FieldDefinition


@dataclass
class Layer(object):
    """Vector layer."""
    ogrLayer: ogr.Layer
    gdalSource: Union[ogr.DataSource, gdal.Dataset]

    def __post_init__(self):
        assert isinstance(self.ogrLayer, ogr.Layer), repr(self.ogrLayer)
        assert isinstance(self.gdalSource, (ogr.DataSource, gdal.Dataset)), repr(self.gdalSource)

    @property
    def name(self):
        return self.ogrLayer.GetName()

    def createField(self, name: str, ogrFieldType: int):
        """Create field. Use one of the ogr.OFT* field types."""
        assert isinstance(name, str)
        ogrFieldDefn = ogr.FieldDefn(name, ogrFieldType)
        self.ogrLayer.CreateField(ogrFieldDefn)

    def createFeature(self, geometry: Geometry, fields: Dict[str, Any] = None):
        """Add feature with given geometry and field values."""
        assert isinstance(geometry, Geometry)
        ogrFeature = ogr.Feature(self._layerDefinition.ogrFeatureDefn)
        ogrFeature.SetGeometry(geometry.ogrGeometry)
        if fields is not None:
            for name, value in fields.items():
                ogrFeature.SetField(name, value)
        self.ogrLayer.CreateFeature(ogrFeature)

    @property
    def featureCount(self):
        """Returns the number of features."""
        return self.ogrLayer.GetFeatureCount()

    @property
    def _layerDefinition(self) -> FeatureDefinition:
        return FeatureDefinition(ogrFeatureDefn=self.ogrLayer.GetLayerDefn())

    @property
    def fieldDefinitions(self) -> List[FieldDefinition]:
        return self._layerDefinition.fieldDefinitions

    @property
    def features(self):
        self.resetReading()
        for ogrFeature in self.ogrLayer:
            assert isinstance(ogrFeature, ogr.Feature)
            yield Feature(ogrFeature=ogrFeature)

    @property
    def attributeTable(self):
        """Return attribute table."""
        result = OrderedDict()
        for fieldDef in self.fieldDefinitions:
            result[fieldDef.name] = list()
        for feature in self.features:
            for fieldDef in self.fieldDefinitions:
                result[fieldDef.name].append(feature.field(fieldDef.name))
        return result

    @property
    def projection(self):
        """Return projection."""
        return Projection(wkt=self.ogrLayer.GetSpatialRef().ExportToWkt())

    def resetReading(self):
        self.ogrLayer.ResetReading()

    def syncToDisk(self):
        self.ogrLayer.SyncToDisk()

    flushCache = syncToDisk

'''    def geometryTypeName(self):
        """Return the geometry type name."""
        return ogr.GeometryTypeToName(self.ogrLayer().GetGeomType())

    def extent(self):
        """Returns the :class:`~hubdc.model.Extent`."""
        xmin, xmax, ymin, ymax = self._ogrLayer.GetExtent()
        return Extent(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, projection=self.projection())

    def grid(self, resolution):
        """Returns grid with extent of self and given resolution."""
        return Grid(extent=self.extent(), resolution=resolution)

    def delete(self):
        """Closes and deletes/unlinks itself from disk/memory."""

        filename = self.filename()
        driver = self.driver()
        self.close()
        driver.delete(filename=filename)

    def rasterize(self, grid, gdalType=gdal.GDT_Float32,
                  initValue=0, burnValue=1, burnAttribute=None, allTouched=False,
                  filterSQL=None, noDataValue=None,
                  filename='', driver=None, options=None):
        """Returns a :class:`~hubdc.model.Raster` that is the rasterization of self into the given ``grid`` as.

        :param grid:
        :type grid: hubdc.core.Grid
        :param gdalType: one of the GDAL data types gdal.GDT_*
        :type gdalType: int
        :param initValue: value to pre-initialize the output array
        :type initValue: int
        :param burnValue: value to burn into the output array for all objects; exclusive with ``burnAttribute``
        :type burnValue: int
        :param burnAttribute: identifies an attribute field on the features to be used for a burn-in value;
                              exclusive with ``burnValue``
        :type burnAttribute: str
        :param allTouched: whether to enable that all pixels touched by lines or polygons will be updated,
                           not just those on the line render path, or whose center point is within the polygon
        :type allTouched: bool
        :param filterSQL: set an SQL WHERE clause which will be used to filter vector features
        :type filterSQL: str
        :param noDataValue: output raster no data value
        :type noDataValue: float
        :param filename: output filename
        :type filename: str
        :param driver:
        :type driver: hubdc.core.RasterDriver
        :param options: raster creation options
        :type options: list
        :return:
        :rtype: hubdc.core.RasterDataset
        """

        assert isinstance(grid, Grid)
        if driver is None:
            driver = hubdc.core.raster.rasterdriver.RasterDriver.fromFilename(filename=filename)

        assert isinstance(driver, hubdc.core.raster.rasterdriver.RasterDriver)
        if options is None:
            options = driver.options()
        assert isinstance(options, list)

        if self.projection().equal(other=grid.projection()):
            vector = self
        else:

            if not grid.projection().wkt() in self._reprojectionCache:
                self._reprojectionCache[grid.projection().wkt()] = self.reproject(projection=grid.projection())
            vector = self._reprojectionCache[grid.projection().wkt()]

        vector.ogrLayer().SetAttributeFilter(filterSQL)
        vector.ogrLayer().SetSpatialFilter(grid.extent().geometry().ogrGeometry())

        raster = driver.create(grid=grid, bands=1, gdalDataType=gdalType, filename=filename, options=options)
        if noDataValue is not None:
            raster.setNoDataValue(noDataValue)
        raster.band(index=0).fill(value=initValue)

        rasterizeLayerOptions = list()
        # special options controlling rasterization:
        #    "ATTRIBUTE": Identifies an attribute field on the features to be used for a burn in value.
        #                 The value will be burned into all output bands.
        #                 If specified, padfLayerBurnValues will not be used and can be a NULL pointer.
        #    "CHUNKYSIZE": The height in lines of the chunk to operate on.
        #                  The larger the chunk size the less times we need to make a pass through all the shapes.
        #                  If it is not set or set to zero the default chunk size will be used.
        #                  Default size will be estimated based on the GDAL cache buffer size using formula:
        #                  cache_size_bytes/scanline_size_bytes, so the chunk will not exceed the cache.
        #    "ALL_TOUCHED": May be set to TRUE to set all pixels touched by the line or polygons,
        #                   not just those whose center is within the polygon or that are selected by
        #                   brezenhams line algorithm. Defaults to FALSE.
        #    "BURN_VALUE_FROM": May be set to "Z" to use the Z values of the geometries.
        #                       The value from padfLayerBurnValues or the attribute field value is added to this
        #                       before burning. In default case dfBurnValue is burned as it is.
        #                       This is implemented properly only for points and lines for now.
        #                       Polygons will be burned using the Z value from the first point.
        #                       The M value may be supported in the future.
        #    "MERGE_ALG": May be REPLACE (the default) or ADD. REPLACE results in overwriting of value,
        #                 while ADD adds the new value to the existing raster, suitable for heatmaps for instance.
        if allTouched:
            rasterizeLayerOptions.append('ALL_TOUCHED=TRUE')
        if burnAttribute:
            rasterizeLayerOptions.append('ATTRIBUTE=' + burnAttribute)

        gdal.RasterizeLayer(raster.gdalDataset(), [1], vector.ogrLayer(), burn_values=[burnValue],
                            options=rasterizeLayerOptions)
        vector.ogrLayer().SetAttributeFilter(None)
        raster.flushCache()
        return raster

    def createFidDataset(self, filename, fidName='_fid'):
        """Create a vector dataset with same features but only one attribute "fid"."""

        driver = hubdc.core.vector.vectordriver.VectorDriver.fromFilename(filename=filename)
        driver.prepareCreation(filename=filename)
        ds = driver.ogrDriver().CreateDataSource(filename)
        srs = osr.SpatialReference(self.projection().wkt())
        layer = ds.CreateLayer('layer', srs=srs)
        field = ogr.FieldDefn(fidName, ogr.OFTInteger)
        layer.CreateField(field)

        for fid, oldOgrFeature in enumerate(self.ogrLayer()):
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField(fidName, fid)
            geometry = oldOgrFeature.GetGeometryRef().Clone()
            feature.SetGeometry(geometry)
            layer.CreateFeature(feature)

        del ds
        vectorDataset = VectorDataset.fromFilename(filename=filename)
        return vectorDataset

    def extractPixel(self, rasterDataset):
        """Extracts all pixel profiles covert by self, together with all associated attribute.

        Returns (rasterValues, vectorValues) tuple.
        """
        assert isinstance(rasterDataset, hubdc.core.raster.rasterdataset.RasterDataset)

        tmpFilename = '/vsimem/VectorDataset.extractPixel.fid.gpkg'
        fid = self.createFidDataset(filename=tmpFilename)
        fidArray = fid.rasterize(grid=rasterDataset.grid(), initValue=-1, burnAttribute=fid.fieldNames()[0],
                                 gdalType=gdal.GDT_Int32).readAsArray()[0]
        #        fidArray = self.rasterizeFid(grid=rasterDataset.grid()).readAsArray()[0]
        valid = fidArray != -1
        fieldNames = self.fieldNames()
        attributeTable = OrderedDict()
        for name in fieldNames:
            attributeTable[name] = list()
        for fid, feature in enumerate(self.features()):
            for name in fieldNames:
                attributeTable[name].append(feature.value(attribute=name))
        fids = fidArray[valid]
        vectorValues = OrderedDict()
        for k, v in attributeTable.items():
            vectorValues[k] = np.array(v)[fids]

        rasterValues = list()
        for band in rasterDataset.bands():
            rasterValues.append(band.readAsArray()[valid])
        rasterValues = np.array(rasterValues)

        return rasterValues, vectorValues, fids

    def metadataDomainList(self):
        """Returns the list of metadata domain names."""
        domains = self._ogrLayer.GetMetadataDomainList()
        return domains if domains is not None else []

    def metadataItem(self, key, domain='', dtype=str, required=False, default=None):
        """Returns the value (casted to a specific ``dtype``) of a metadata item."""
        key = key.replace(' ', '_')
        gdalString = self._ogrLayer.GetMetadataItem(key, domain)
        if gdalString is None:
            if required:
                raise Exception('missing metadata item: key={}, domain={}'.format(key, domain))
            return default
        return MetadataFormatter.stringToValue(gdalString, dtype=dtype)

    def metadataDomain(self, domain=''):
        """Returns the metadata dictionary for the given ``domain``."""
        metadataDomain = dict()
        for key in self._ogrLayer.GetMetadata(domain):
            key = key.replace('_', ' ')
            metadataDomain[key] = self.metadataItem(key=key, domain=domain)
        return metadataDomain

    def metadataDict(self):
        """Returns the metadata dictionary for all domains."""
        metadataDict = dict()
        for domain in self.metadataDomainList():
            metadataDict[domain] = self.metadataDomain(domain=domain)
        return metadataDict

    def setMetadataItem(self, key, value, domain=''):
        """Set a metadata item. ``value`` can be a string, a number or a list of strings or numbers."""
        if value is None:
            return
        key = key.replace(' ', '_').strip()
        if domain.upper() == 'ENVI' and key.lower() == 'file_compression':
            return
        gdalString = MetadataFormatter.valueToString(value)
        self._ogrLayer.SetMetadataItem(key, gdalString, domain)

    def setMetadataDomain(self, metadataDomain, domain):
        """Set the metadata domain"""
        assert isinstance(metadataDomain, dict)
        for key, value in metadataDomain.items():
            self.setMetadataItem(key=key, value=value, domain=domain)

    def setMetadataDict(self, metadataDict):
        """Set the metadata dictionary"""
        assert isinstance(metadataDict, dict)
        for domain, metadataDomain in metadataDict.items():
            self.setMetadataDomain(metadataDomain=metadataDomain, domain=domain)
            # for key, value in metadataDict[domain].items():
            #    self.setMetadataItem(key=key, value=value, domain=domain)



    @staticmethod
    def zsize():
        """Returns number of layers, which is always one."""
        return 1
'''
