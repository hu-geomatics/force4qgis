# from __future__ import annotations
from dataclasses import dataclass
from os import makedirs
from os.path import splitext, isabs, abspath, exists, dirname
from typing import Optional

from osgeo import ogr, gdal
from force4qgis.hubforce.core import Vector


@dataclass
class VectorDriver(object):
    """Vector driver."""
    name: str

    def __post_init__(self):
        assert isinstance(self.name, str)

    @classmethod
    def fromFilename(cls, filename: str) -> VectorDriver:
        if filename == '':
            return memoryDriver
        ext = splitext(filename)[1][1:].lower()
        if ext == 'shp':
            return esriShapefileDriver
        if ext == 'gpkg':
            return geoPackageDriver
        raise ValueError(f'unexpected file extention: {ext}')

    @property
    def ogrDriver(self):
        return ogr.GetDriverByName(self.name)

    def createDataset(self, filename='') -> Vector:
        """Create vector datasource."""
        assert isinstance(filename, str)
        self.prepareCreation(filename=filename)
        ogrDataSource = self.ogrDriver.CreateDataSource(filename)
        return Vector(gdalSource=ogrDataSource)

    def prepareCreation(self, filename: str) -> str:
        """Deletes dataset if it already exist and creates folder if needed."""
        assert isinstance(filename, str)
        if self == memoryDriver or filename.startswith('/vsimem/'):
            return filename

        if not isabs(filename):
            filename = abspath(filename)
        if not exists(dirname(filename)):
            makedirs(dirname(filename))
        if exists(filename):
            self.delete(filename=filename)
        return filename

    def delete(self, filename):
        """Delete/unlink file given by ``filename``."""
        if filename.lower().startswith('/vsimem/'):
            gdal.Unlink(filename)
        else:
            self.ogrDriver.DeleteDataSource(filename)


esriShapefileDriver = VectorDriver(name='ESRI Shapefile')
geoPackageDriver = VectorDriver(name='GPKG')
memoryDriver = VectorDriver(name='Memory')
