from os.path import splitext
from typing import Iterable


def importFunction(func):
    modulepath, funcname = splitext(func.decode())
    code = f'from {modulepath} import {funcname[1:]} as func'
    namespace = {}
    exec(code, namespace)
    func = namespace.get('func')
    return func


def functionImportPath(pixelFunction) -> str:
    return f'{pixelFunction.__module__}.{pixelFunction.__qualname__}'


def encodeIterable(iterable: Iterable) -> str:
    return ', '.join(iterable)