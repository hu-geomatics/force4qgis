from math import ceil

import numpy as np

CACHE = dict()
CACHE_ENABLED = False

DEBUG = not True

class Cache(object):
    def __init__(self, enabled=True):
        self.enabled = enabled

    def __enter__(self):
        global CACHE, CACHE_ENABLED
        CACHE.clear()
        CACHE_ENABLED = self.enabled
        return None

    def __exit__(self, exc_type, exc_value, exc_traceback):
        global CACHE, CACHE_ENABLED

        if DEBUG:
            bytes = 0
            for ar, noDataValue in CACHE.values():
                if isinstance(ar, np.ndarray):
                    bytes += ar.size * ar.itemsize
            megabytes = ceil(bytes / 2**20)
            print(f'CACHE Content ({megabytes} MB)')
            for key in CACHE:
                print(key)

        CACHE.clear()
        CACHE_ENABLED = False
