from os.path import join
from tempfile import gettempdir
from typing import Tuple, Union, Iterable, Optional
from uuid import uuid4

from osgeo import gdal

AUTO_FILENAMES_DIR = ''
AUTO_FILENAMES = list()
AUTO_FILENAMES_ENABLED = False


def autoFilename(name: str, tilename: str) -> str:
    assert AUTO_FILENAMES_ENABLED, 'auto-filenames not enabled'
    key128bit = uuid4().hex
    filename = join(AUTO_FILENAMES_DIR, tilename, f'{name}.{key128bit}.vrt')
    AUTO_FILENAMES.append(filename)
    return filename


#def autoFilenames(name: str, tilecount: int) -> Tuple[str, ...]:
#    filenames = tuple(autoFilename(name=name) for _ in range(tilecount))
#    return filenames


def autoFilenamesDir():
    return AUTO_FILENAMES_DIR


def setAutoFilenamesDir(dir: str):
    global AUTO_FILENAMES_DIR, AUTO_FILENAMES_ENABLED
    AUTO_FILENAMES_DIR = dir
    AUTO_FILENAMES_ENABLED = True


def unsetAutoFilenamesDir():
    global AUTO_FILENAMES_DIR, AUTO_FILENAMES_ENABLED
    AUTO_FILENAMES_DIR = ''
    AUTO_FILENAMES_ENABLED = False
    AUTO_FILENAMES.clear()


class AutoFilenames(object):
    def __init__(self, dir: str = None, cleanup: bool = None, enabled=True):
        if cleanup is None:
            if dir is None:
                cleanup = True
            else:
                cleanup = False
        assert isinstance(cleanup, bool)
        self.cleanup = cleanup
        if dir is None:
            dir = join(gettempdir(), 'autodir')
        self.dir = dir
        self.enabled = enabled

    def __enter__(self):
        global AUTO_FILENAMES_DIR, AUTO_FILENAMES, AUTO_FILENAMES_ENABLED
        if self.enabled:
            setAutoFilenamesDir(dir=self.dir)
        else:
            unsetAutoFilenamesDir()
        return None

    def __exit__(self, exc_type, exc_value, exc_traceback):
        global AUTO_FILENAMES, AUTO_FILENAMES_ENABLED
        if self.cleanup:
            for filename in AUTO_FILENAMES:
                gdal.Unlink(filename)
        unsetAutoFilenamesDir()
