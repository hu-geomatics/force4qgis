from os.path import abspath
from typing import Tuple, Dict, Sequence

import numpy as np
from osgeo.gdal_array import NumericTypeCodeToGDALTypeCode

from force4qgis.hubforce.core.delayed.autofilename import autoFilename
from force4qgis.hubforce.core.delayed.singlebandpixelfunction import singleBandPixelFunction
from force4qgis.hubforce.core.delayed.utils import functionImportPath
from force4qgis.hubforce.core.metadata.date import Date
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rasterdriver import VRT_DRIVER

DEBUG = False


def delayedSingleBandRaster(
        name: str,
        grids: Tuple[Grid, ...],
        tilenames: Tuple[str, ...],
        sourceRasters: Sequence[Raster],
        pixelFunction,
        outType=None, outNoDataValue=None,
        workType=None,
        kwds: Dict = None,
        date: Date = None,
        bandNames: Sequence[str] = None
) -> Raster:
    assert len(grids) == len(tilenames)

    for raster in sourceRasters:
        assert isinstance(raster, Raster)

    if outType is None:
        outType = np.float32

    # todo MP this!!!!!!!!!!!

    filenames = list()
    for grid, tilename in zip(grids, tilenames):
        filename = delayedSingleBandRasterTile(
            name=name, grid=grid, tilename=tilename,
            sourceRasters=sourceRasters,
            pixelFunction=pixelFunction, workType=workType, outType=outType, outNoDataValue=outNoDataValue, kwds=kwds
        )
        filenames.append(filename)

    return Raster.open(filenames=filenames, tilenames=tilenames, name=name, date=date, bandNames=bandNames)


def delayedSingleBandRasterTile(
        name: str, grid: Grid, tilename: str,
        sourceRasters: Sequence[Raster], pixelFunction,
        workType, outType, outNoDataValue, kwds
) -> str:
    filename = autoFilename(name=name, tilename=tilename)

    # create VRT skeleton for the first band
    vrtRaster = VRT_DRIVER.createDataset(grid=grid, bands=0, dtype=outType, filename=abspath(filename))
    options = [
        'subClass=VRTDerivedRasterBand',
        f'PixelFunctionType={functionImportPath(singleBandPixelFunction)}',
        'PixelFunctionLanguage=Python',
    ]

    gdalTypeCode = NumericTypeCodeToGDALTypeCode(outType)
    vrtRaster.gdalDataset.AddBand(gdalTypeCode, options)
    vrtRaster.flushCache()
    del vrtRaster

    # append all bands
    with open(filename) as file:
        lines = file.readlines()
    head = lines[:-5]
    tail = lines[-1:]
    bandheadA = lines[-5]
    bandheadB = lines[-4:-2]
    bandtail = lines[-2:-1]
    with open(filename, 'w') as file:
        file.writelines(head)

        zsize = 0
        for raster in sourceRasters:
            zsize = max(zsize, len(raster.bands))
        for index in range(zsize):
            file.write(bandheadA.replace('band="1"', f'band="{index + 1}"'))
            file.writelines(bandheadB)
            file.write('    <PixelFunctionArguments\n')
            file.write(f'      VRTRasterBand="{abspath(filename)}@B{index + 1}"\n')
            file.write(f'      VRTNoDataValue="{outNoDataValue}"\n')
            file.write(f'      func="{functionImportPath(pixelFunction)}"\n')

            if workType is not None:
                file.write(f'      worktype="numpy.{np.float32.__name__}"\n')

            sourceBands = list()
            sourceMasks = list()
            for raster in sourceRasters:
                number = index % len(raster.bands) + 1
                band = raster.band(number=number)
                if tilename in raster.tilenames:
                    tileindex = raster.tilenames.index(tilename)
                    sourceBand = f'{abspath(band.filenames[tileindex])}@B{band.numbers[tileindex]}@{band.resampling}'
                    if band.mask is not None:
                        sourceMask = f'{abspath(band.mask.filenames[tileindex])}@B{band.mask.numbers[tileindex]}@{band.mask.resampling}'
                    else:
                        sourceMask = str(None)
                else:
                    if DEBUG:
                        print('NO FILES:', tilename, raster.name)
                    sourceBand = str(None)
                    sourceMask = str(None)
                sourceBands.append(sourceBand)
                sourceMasks.append(sourceMask)

            sourceBands = ', '.join(sourceBands)
            sourceMasks = ', '.join(sourceMasks)

            file.write(f'      SourceBands="{sourceBands}"\n')
            file.write(f'      SourceMasks="{sourceMasks}"\n')

            if kwds is not None:
                for k, v in kwds.items():
                    file.write(f'      {k}="{v}"\n')

            file.write('    />\n')
            file.writelines(bandtail)
        file.writelines(tail)

    gdalRaster = GdalRaster.open(filename)
    gdalRaster.setNoDataValue(outNoDataValue)

    return filename
