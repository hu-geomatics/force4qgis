from collections import defaultdict
from datetime import datetime
from multiprocessing.pool import Pool
from os.path import join, exists
from time import time
from typing import List, NamedTuple

import numpy as np

from force4qgis.hubforce.algorithms.createvrt import createStackFromMultiBandRasters
from force4qgis.hubforce.core.delayed.cache import Cache
from force4qgis.hubforce.core.progress import Progress, printProgress
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection
from force4qgis.hubforce.core.raster.rasterdriver import RasterDriver, VRT_DRIVER
from force4qgis.hubforce.core.raster.shape import GridShape


def computeRaster(
        raster: Raster,
        dirname: str,
        blockShape: GridShape,
        extention: str = None,
        processes: int = None,
        callback=None
):
    rasterCollection = RasterCollection(rasters=(raster,), grids=raster.grids, tilenames=raster.tilenames)
    computeRasterCollection(
        rasterCollection=rasterCollection, dirname=dirname, blockShape=blockShape, extention=extention,
        processes=processes, callback=callback
    )


def computeRasterCollection(
        rasterCollection: RasterCollection, dirname: str, blockShape: GridShape, extention: str = None,
        processes: int = None, noDataRaster: Raster = None, callback=None, verbose=1,
):
    assert isinstance(rasterCollection, RasterCollection)
    assert isinstance(blockShape, GridShape)
    if processes is None or processes == 1:
        pool = None
        initializeGlobals(rasterCollection_=rasterCollection)
    else:
        pool = Pool(processes=processes, initializer=initializeGlobals, initargs=(rasterCollection,))
    if extention is None:
        extention = '.tif'
    if callback is None:
        callback = printProgress

    grids = rasterCollection.grids
    tilenames = rasterCollection.tilenames

    driver = RasterDriver.fromFilename(filename=f'tmp{extention}')
    options = None

    t0 = time()
    if verbose > 0:
        callback(f'Start at {datetime.now().isoformat()[:19]}', end='..')
        callback(Progress(0, 1))

    # yield arguments
    def yieldArguments():
        for tileindex, grid in enumerate(grids):
            for block in grid.blocks(shape=blockShape):
                yield ComputeArguments(tileindex=tileindex, grid=grid, block=block)

    arguments = yieldArguments()

    # map compute jobs
    # - prepare some global variables
    nblocks = len(tuple(grids[0].blocks(shape=blockShape)))  # it is assumed that all grids are of equal size!
    njobs = len(grids) * nblocks
    outrasters = [None for _ in range(len(grids))]
    outfilenames = defaultdict(dict)

    if driver == VRT_DRIVER:
        # in case of VRT, just Translate into another VRT
        for tileindex, tilename in enumerate(tilenames):
            if verbose > 0:
                callback(Progress(tileindex + 1, len(tilenames)))

            outrasters[tileindex] = list()
            for raster in rasterCollection.rasters:
                filename = join(dirname, tilename, raster.name + extention)
                rasters = list()
                numbers = list()
                for band in raster.bands:
                    if tilename not in band.tilenames:
                        assert noDataRaster is not None
                        band = noDataRaster.band(number=1)
                    bandTileindex = band.tilenames.index(tilename)
                    rasters.append(GdalRaster.open(band.filenames[bandTileindex]))
                    numbers.append([band.numbers[bandTileindex]])
                grid = rasterCollection.grids[tilenames.index(tilename)]
                outraster = createStackFromMultiBandRasters(
                    rasters=rasters, numbers=numbers, filename=filename, grid=grid
                )
                outfilenames[raster.name][tilename] = filename
                outrasters[tileindex].append(outraster)
    else:
        # compute results
        if pool is not None:
            # results = pool.imap_unordered(func=computeBlock, iterable=arguments) # is not working correctly on linux server!?
            results = pool.imap_unordered(func=computeBlock, iterable=arguments)
        else:
            results = map(computeBlock, arguments)

        for ijob, result in enumerate(results):
            if verbose > 0:
                callback(Progress(i=ijob + 1, n=njobs))

            # init output rasters
            if outrasters[result.tileindex] is None:
                rasters = rasterCollection.rasters
                outrasters[result.tileindex] = list()
                for raster, array in zip(rasters, result.arrays):
                    filename = join(dirname, tilenames[result.tileindex], raster.name + extention)
                    dtypes = [bandArray.dtype for bandArray in array if bandArray is not None]
                    allNone = len(dtypes) == 0

                    if not allNone:
                        assert len(set(dtypes)) == 1, f'raster band data types not matching: {dtypes}'
                        dtype = dtypes[0]
                        outraster = driver.createDataset(
                            grid=result.grid, bands=len(raster.bands), dtype=dtype, filename=filename, options=options
                        )
                        outrasters[result.tileindex].append(outraster)
                        outfilenames[raster.name][tilenames[result.tileindex]] = filename
                    else:
                        outrasters[result.tileindex].append(None)

            # write outputs to disk
            for outraster, array in zip(outrasters[result.tileindex], result.arrays):
                if isinstance(outraster, GdalRaster):
                    for band, bandArray in zip(outraster.bands, array):
                        if bandArray is None:
                            bandArray = np.full(shape=result.block.shape, fill_value=band.noDataValue(default=0))
                        band.writeArray(array=bandArray, grid=result.block)
            # todo  close files after grid is done

    # set metadata
    rasters = rasterCollection.rasters
    for tileindex, tilename in enumerate(tilenames):
        for raster, outraster in zip(rasters, outrasters[tileindex]):
            if isinstance(outraster, GdalRaster):
                for band, band2 in zip(raster.bands, outraster.bands):
                    band2.setNoDataValue(band.noDataValue)
                    band2.setDescription(band.name)
                outraster.flushCache()

    s = time() - t0
    m = s / 60
    h = m / 60
    callback(f'Done in {round(s, 1)} sec | {round(m, 1)} min | {round(h, 1)} hrs')

    if pool is None:
        deinitializeGlobals()
    else:
        pool.close()
        pool.join()

    return outfilenames


def initializeGlobals(rasterCollection_):
    global rasterCollection
    rasterCollection = rasterCollection_


def deinitializeGlobals():
    global rasterCollection
    del rasterCollection


class ComputeArguments(NamedTuple):
    tileindex: int
    grid: Grid
    block: Grid
    # rasterCollection: RasterCollection


class ComputeResults(NamedTuple):
    tileindex: int
    grid: Grid
    block: Grid
    arrays: List[List[np.ndarray]]


def computeBlock(args: ComputeArguments) -> ComputeResults:
    global rasterCollection
    assert isinstance(rasterCollection, RasterCollection)
    with Cache():
        arrays = list()
        tilename = rasterCollection.tilenames[args.tileindex]
        for raster in rasterCollection:
            array = list()
            for band in raster.bands:
                if tilename in band.tilenames:
                    tileindex = band.tilenames.index(tilename)
                    gdalRaster = GdalRaster.open(filename=band.filenames[tileindex])
                    gdalBand = gdalRaster.band(number=band.numbers[tileindex])
                    bandArray = gdalBand.readAsArray(grid=args.block, resampleAlg=band.resampling.gdalCode)
                    if band.mask is not None:
                        if tilename in band.mask.tilenames:
                            tileindex = band.mask.tilenames.index(tilename)
                            gdalMaskRaster = GdalRaster.open(filename=band.mask.filenames[tileindex])
                            gdalMaskBand = gdalMaskRaster.band(number=band.mask.numbers[tileindex])
                            m = gdalMaskBand.readAsArray(grid=args.block, resampleAlg=band.mask.resampling.gdalCode)
                            invalid = m == gdalMaskBand.noDataValue(default=0)
                            bandArray[invalid] = gdalBand.noDataValue(default=0)
                else:
                    bandArray = None
                array.append(bandArray)
            arrays.append(array)

    return ComputeResults(args.tileindex, grid=args.grid, block=args.block, arrays=arrays)

# for raster in rasterCollection:
#     filename = filenameFunc(tile, raster)
#     driver = driverFunc(raster)
#     options = optionsFunc(raster)
#
#     # todo create with first block!
#     dtypes = [band.dtype for band in raster]
#     assert len(set(dtypes)) == 1, f'raster band data types not matching: {dtypes}'
#     dtype = dtypes[0]
#     raster2 = driver.createDataset(
#         grid=grid, bands=len(raster.bands), dtype=dtype, filename=filename, options=options
#     )
#     rastersByTile[tile].append(raster)
#     rasters2ByTile[tile].append(raster2)


# todo  close files after grid is done
# unfinishedBlockByGrid[grid].remove(block)
# if len(unfinishedBlockByGrid[grid]) == 0:
#     unfinishedBlockByGrid.pop(grid)
#     rasters = rastersByTile[grid]
#     for raster, outraster in zip(rasters, outraster):
#         # outraster.setMetadataDict(raster.metadataDict)
#         for band, band2 in zip(raster.bands, outraster.bands):
#             band2.setDescription(band.name)
#             band2.setNoDataValue(band.gdalBand.noDataValue())
#         outraster.flushCache()
