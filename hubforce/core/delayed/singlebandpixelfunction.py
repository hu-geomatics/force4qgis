from dataclasses import dataclass
from traceback import print_exc
from typing import Tuple

import numpy as np
from osgeo import gdal

from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.delayed import cache
from force4qgis.hubforce.core.delayed.utils import importFunction

DEBUG = not True

nan = np.nan


def singleBandPixelFunction(
        in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt,
        func, VRTRasterBand, VRTNoDataValue, SourceBands, SourceMasks, **kwargs
):
    try:
        assert len(in_ar) == 0
        VRTRasterBand = VRTRasterBand.decode()
        out_filename, out_band = VRTRasterBand.split('@B')
        out_band = int(out_band)

        # get source bands
        SourceBands = SourceBands.decode()
        in_bandFilenames = list()
        in_bands = list()
        in_bandResamplings = list()
        for sourceBand in SourceBands.split(','):
            if sourceBand.strip() == 'None':
                in_filename = in_band = in_resampling = None
            else:
                if '@Resampling' in sourceBand:
                    in_filename, tmp = sourceBand.split('@B')
                    in_filename = in_filename.strip()
                    in_band, tmp_resampling = tmp.split('@')
                    in_band = int(in_band)
                    in_resampling = eval(tmp_resampling)
                else:
                    in_filename, in_band = sourceBand.split('@B')
                    in_filename = in_filename.strip()
                    in_band = int(in_band)
                    in_resampling = Resampling.NearestNeighbour
            in_bandFilenames.append(in_filename)
            in_bands.append(in_band)
            in_bandResamplings.append(in_resampling)

        # get source masks
        SourceMasks = SourceMasks.decode()
        in_maskbandFilenames = list()
        in_maskbands = list()
        in_maskbandResamplings = list()
        for sourceMask in SourceMasks.split(','):
            if sourceMask.strip() == 'None':
                in_filename = in_band = in_resampling = None
            else:
                if '@Resampling' in sourceMask:
                    in_filename, tmp = sourceMask.split('@B')
                    in_band, tmp_resampling = tmp.split('@')
                    in_resampling = eval(tmp_resampling)
                else:
                    in_filename, in_band = sourceMask.split('@B')
                    in_resampling = Resampling.NearestNeighbour
                in_filename = in_filename.strip()
                in_band = int(in_band)
            in_maskbandFilenames.append(in_filename)
            in_maskbands.append(in_band)
            in_maskbandResamplings.append(in_resampling)

        out_key = (
            out_filename, out_band, Resampling.NearestNeighbour.name, out_ar.shape, xoff, yoff, xsize, ysize,
            raster_xsize, raster_ysize, buf_radius, gt
        )

        if DEBUG:
            print('need', out_key)

        if out_key in cache.CACHE:
            if DEBUG:
                print('  use', out_key)
            ar = cache.CACHE[out_key][0]
            np.copyto(dst=out_ar, src=ar)
        else:
            if DEBUG:
                print('  calculate', out_key)

            # read source bands
            in_ar = list()
            in_ndv = list()
            for in_filename, in_band, in_resampling in zip(in_bandFilenames, in_bands, in_bandResamplings):

                if in_filename is None:
                    in_ar.append(None)
                    in_ndv.append(None)
                    continue

                in_key = (
                    in_filename, in_band, in_resampling.name, out_ar.shape, xoff, yoff, xsize, ysize, raster_xsize,
                    raster_ysize, buf_radius, gt
                )
                if in_key in cache.CACHE:
                    if DEBUG:
                        print('  use cached', in_key)
                    ar, noDataValue = cache.CACHE[in_key]
                else:
                    if DEBUG:
                        print('    read', in_key)
                    ar, noDataValue = readAsArray(
                        filename=in_filename, band=in_band, xoff=xoff, yoff=yoff, xsize=xsize, ysize=ysize,
                        raster_xsize=raster_xsize, raster_ysize=raster_ysize,
                        buf_xsize=out_ar.shape[1], buf_ysize=out_ar.shape[0], resampling=in_resampling
                    )
                    # ar = preProcess(ar=ar, scale=preScale, worktype=worktype)
                    if cache.CACHE_ENABLED:
                        if DEBUG:
                            print('      cache', in_key)
                        cache.CACHE[in_key] = ar.copy(), noDataValue
                in_ar.append(ar)
                in_ndv.append(noDataValue)

            # read source masks
            in_ma = list()
            for in_filename, in_band, in_resampling in zip(in_maskbandFilenames, in_maskbands, in_maskbandResamplings):
                if in_filename is None:
                    in_ma.append(None)
                    continue

                in_key = (
                    in_filename, in_band, in_resampling.name, out_ar.shape, xoff, yoff, xsize, ysize, raster_xsize,
                    raster_ysize, buf_radius, gt
                )
                if in_key in cache.CACHE:
                    if DEBUG:
                        print('  use cached', in_key)
                    ma, noDataValue = cache.CACHE[in_key]
                else:
                    if DEBUG:
                        print('    read', in_key)
                    ma, noDataValue = readAsArray(
                        filename=in_filename, band=in_band, xoff=xoff, yoff=yoff, xsize=xsize, ysize=ysize,
                        raster_xsize=raster_xsize, raster_ysize=raster_ysize,
                        buf_xsize=out_ar.shape[1], buf_ysize=out_ar.shape[0], resampling=in_resampling
                    )
                    if cache.CACHE_ENABLED:
                        if DEBUG:
                            print('      cache', in_key)
                        cache.CACHE[in_key] = ma.copy(), noDataValue
                if noDataValue is None:
                    noDataValue = 0
                ma = ma != noDataValue
                in_ma.append(ma)

            # call pixel function
            # - cast noDataValues explicitely to the correct numpy type
            def castType(value, dtype):
                if value is None:
                    return None
                return np.array([value]).astype(dtype)[0]

            out_ndv = eval(VRTNoDataValue)
            if out_ndv is not None:
                out_ndv = castType(out_ndv, out_ar.dtype)
            in_ndv = tuple(None if ndv is None else castType(ndv, ar.dtype) for ndv, ar in zip(in_ndv, in_ar))

            # calc result
            func = importFunction(func)
            buf_ysize, buf_xsize = out_ar.shape
            args = PixelFunctionArguments(
                in_ar=tuple(in_ar), in_ma=tuple(in_ma), in_ndv=in_ndv, out_ndv=out_ndv, out_size=1,
                out_dtype=out_ar.dtype, xoff=xoff, yoff=yoff, xsize=xsize, ysize=ysize, raster_xsize=raster_xsize,
                raster_ysize=raster_ysize, buf_xsize=out_ar.shape[1], buf_ysize=out_ar.shape[0], buf_radius=buf_radius,
                gt=gt
            )

            tmp_ar = func(args, **kwargs)

            np.copyto(dst=out_ar, src=tmp_ar, casting='unsafe')

            if cache.CACHE_ENABLED:
                if DEBUG:
                    print('  cache', out_key)

                cache.CACHE[out_key] = out_ar.copy(), out_ndv

    except:
        print_exc()
        raise


def readAsArray(filename, band, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_xsize, buf_ysize, resampling):
    ds: gdal.Dataset = gdal.Open(filename)
    rb: gdal.Band = ds.GetRasterBand(int(band))
    fx = ds.RasterXSize / raster_xsize
    fy = ds.RasterYSize / raster_ysize
    win_xsize = xsize * fx
    win_ysize = ysize * fy
    xoff_ = xoff * fx
    yoff_ = yoff * fy
    resample_alg = resampling.gdalCode
    ar = rb.ReadAsArray(
        xoff=xoff_, yoff=yoff_, win_xsize=win_xsize, win_ysize=win_ysize,
        buf_xsize=buf_xsize, buf_ysize=buf_ysize, buf_type=rb.DataType,
        resample_alg=resample_alg
    )
    noDataValue = rb.GetNoDataValue()

#    print(f'xoff={xoff}, yoff={yoff}, xsize={xsize}, ysize={ysize}, raster_xsize={raster_xsize}, raster_ysize={raster_ysize}, buf_xsize={buf_xsize}, buf_ysize={buf_ysize}')
    assert ar.shape == (buf_ysize, buf_xsize)
    return ar, noDataValue


@dataclass(frozen=True)
class PixelFunctionArguments(object):
    in_ar: Tuple[np.ndarray, ...]
    in_ma: Tuple[np.ndarray, ...]
    in_ndv: Tuple[float, ...]
    out_ndv: float
    out_size: int
    out_dtype: type
    xoff: int
    yoff: int
    xsize: int
    ysize: int
    raster_xsize: int
    raster_ysize: int
    buf_xsize: int
    buf_ysize: int
    buf_radius: float
    gt: Tuple[float, float, float, float, float, float]
