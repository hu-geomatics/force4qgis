from unittest import TestCase

from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core import Resolution
from force4qgis.hubforce.core import Extent


class TestGrid(TestCase):

    def test__extentSnappedToResolutionMultiple(self):
        gold = Extent(0, 9, 0, 9)
        lead = Grid.snapExtentToResolution(Extent(0, 10, 0, 8), Resolution(3, 3))
        self.assertTrue(gold.equal(lead))

    def test__isExtentAlmostResolutionMultiple(self):
        self.assertTrue(Grid.isExtentSnappedToResolution(Extent(0 - 1e-6, 9, 0, 9), Resolution(3, 3)))
        self.assertTrue(Grid.isExtentSnappedToResolution(Extent(0 + 1e-6, 9, 0, 9), Resolution(3, 3)))
        self.assertFalse(Grid.isExtentSnappedToResolution(Extent(0 - 1e-4, 9, 0, 9), Resolution(3, 3)))
        self.assertFalse(Grid.isExtentSnappedToResolution(Extent(0 + 1e-4, 9, 0, 9), Resolution(3, 3)))


    def test_gridWithAlmostMatchingExtentSize(self):
        tol = 1e-6
        Grid(Extent(0 - tol, 9, 0, 9), Resolution(3, 3))
        Grid(Extent(0 + tol, 9, 0, 9), Resolution(3, 3))
        Grid(Extent(0, 9 - tol, 0, 9), Resolution(3, 3))
        Grid(Extent(0, 9 + tol, 0, 9), Resolution(3, 3))

    def test_gridWithNotMatchingExtentSize(self):
        tol = 1e-4
        with self.assertRaises(ValueError):
            Grid(Extent(0 - tol, 9, 0, 9), Resolution(3, 3))
        with self.assertRaises(ValueError):
            Grid(Extent(0 + tol, 9, 0, 9), Resolution(3, 3))
        with self.assertRaises(ValueError):
            Grid(Extent(0, 9 - tol, 0, 9), Resolution(3, 3))
        with self.assertRaises(ValueError):
            Grid(Extent(0, 9 + tol, 0, 9), Resolution(3, 3))

    def test_withResolution(self):
        extent = Extent(0, 9, 0, 9)
        gold = Grid(extent, Resolution(3, 3))
        lead = Grid(extent, Resolution(1, 1)).withResolution(Resolution(3, 3))
        self.assertTrue(lead.equal(gold, tol=1e-5))

    def test_equal(self):
        grid = Grid(Extent(0, 1, 0, 1), Resolution(1, 1))
        self.assertTrue(grid.equal(Grid(Extent(0, 1, 0, 1), Resolution(1, 1))))
        self.assertFalse(grid.equal(Grid(Extent(0, 10, 0, 10), Resolution(1, 1))))
        self.assertFalse(grid.equal(Grid(Extent(0, 1, 0, 1), Resolution(0.1, 0.1))))

'''     
   def test(self):
        Grid(extent=grid.extent(), resolution=grid.resolution())
        grid.equal(other=grid)
        # grid.reproject(other=grid)
        grid.pixelBuffer(buffer=1)
        grid.subgrids(size=RasterSize(x=256, y=256))
        grid.atResolution(resolution=10)
        grid.anchor(point=Point(x=0, y=0, projection=Wgs84WebMercator()))

    def test_coordinates(self):
        grid = Grid(extent=Extent(xmin=0, xmax=3, ymin=0, ymax=2, projection=Wgs84()),
                    resolution=Resolution(x=1, y=1))
        xgold = np.array([[0.5, 1.5, 2.5], [0.5, 1.5, 2.5]])
        ygold = np.array([[1.5, 1.5, 1.5], [0.5, 0.5, 0.5]])
        self.assertTrue(np.all(grid.xMapCoordinatesArray() == xgold))
        self.assertTrue(np.all(grid.yMapCoordinatesArray() == ygold))

        xgold = np.array([[0, 1, 2], [0, 1, 2]])
        ygold = np.array([[0, 0, 0], [1, 1, 1]])

        self.assertTrue(np.all(grid.xPixelCoordinatesArray() == xgold))
        self.assertTrue(np.all(grid.yPixelCoordinatesArray() == ygold))

        subgrid = grid.subset(offset=Pixel(x=1, y=1), size=RasterSize(x=2, y=1))
        subxgold = xgold[1:, 1:]
        subygold = ygold[1:, 1:]

        print(subgrid.xPixelCoordinatesArray())
        print(subgrid.yPixelCoordinatesArray())

        self.assertTrue(np.all(np.equal(subgrid.xPixelCoordinatesArray(offset=1), subxgold)))
        self.assertTrue(np.all(np.equal(subgrid.yPixelCoordinatesArray(offset=1), subygold)))
'''
