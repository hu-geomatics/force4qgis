from os.path import join
from unittest.case import TestCase


class TestRasterDriver(TestCase):
    def test_Driver(self):
        self.assertIsInstance(obj=RasterDriver(name='ENVI').gdalDriver(), cls=gdal.Driver)
        with self.assertRaises(errors.InvalidGDALDriverError):
            RasterDriver(name='not a valid driver name')

    def test___str__(self):
        print(RasterDriver(name='ENVI'))
        print(MemDriver())
        print(VrtDriver())
        print(EnviDriver())
        print(GTiffDriver())
        print(ErdasDriver())

    def test_equal(self):
        d1 = EnviDriver()
        d2 = GTiffDriver()
        d3 = RasterDriver('ENVI')
        self.assertTrue(d1.equal(d3))
        self.assertFalse(d1.equal(d2))

    def test_create(self):
        self.assertIsInstance(obj=MemDriver().create(grid=grid), cls=RasterDataset)
        try:
            MemDriver().create(grid=grid, filename='abc')
        except Exception as error:
            print(error)

    def test_fromFilename(self):
        for ext in ['bsq', 'bip', 'bil', 'tif', 'img', 'vrt']:
            filename = join(outdir, 'file.' + ext)
            driver = RasterDriver.fromFilename(filename=filename)
            print(driver)

        assert RasterDriver.fromFilename(filename='file.xyz').equal(other=EnviDriver())

    def prepareCreation(self):
        print(RasterDriver().prepareCreation('raster.bsq'))
        print(MemDriver().prepareCreation(''))