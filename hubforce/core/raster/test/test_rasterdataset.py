from os.path import join, dirname, basename, splitext
from typing import Optional, Type
from unittest.case import TestCase

import numpy as np

from force4qgis.hubforce.core import GdalRaster
from force4qgis.hubforce.core import RasterShape, GridShape

outdir = f'output/{splitext(basename(__file__))[0]}'
vsimem = f'/vsimem/{outdir}'


class TestRasterDataset(TestCase):

    def makeCheckerboard(self, shape: GridShape, filename: Optional[str] = None, dtype: Type = np.uint8
                         ) -> GdalRaster:
        array = np.mod(np.arange(0, shape.x * shape.y).reshape(1, *shape), 2).astype(dtype)
        return GdalRaster.fromArray(array=array, filename=filename)

    def test_filename(self):
        r = self.makeCheckerboard(GridShape(3, 5), filename=join(outdir, 'checkerboard.bsq'))
        print(r)
        return
        assert openRasterDataset(LT51940232010189KIS01.green).filename() == LT51940232010189KIS01.green
        vrtFilename = '/vsimem/green.vrt'
        rasterDataset = createVRTDataset([LT51940232010189KIS01.green, LT51940242010189KIS01.green],
                                         filename=vrtFilename)
        assert rasterDataset.filename() == vrtFilename

    def test_translate(self):
        grid = Grid(extent=Extent(xmin=0, xmax=2, ymin=0, ymax=1, projection=Projection.wgs84()), resolution=1)
        subgrid = Grid(extent=Extent(xmin=0, xmax=1, ymin=0, ymax=1, projection=Projection.wgs84()), resolution=1)
        array = np.array([[[1, 2]]])
        gold = np.array([[[1]]])
        rasterdataset = GdalRaster.fromArray(array=array, grid=grid)
        assert rasterdataset.translate(grid=subgrid).readAsArray() == gold

    def test_translateResampleAlgs(self):
        ds = GdalRaster.fromArray(array=[[1, -1, -1, -1, -1, -1],
                                         [-1, 2, -1, -1, -1, -1],
                                         [-1, -1, 3, -1, -1, -1],
                                         [-1, -1, -1, 4, -1, -1],
                                         [-1, -1, -1, -1, 5, -1],
                                         [-1, -1, -1, -1, -1, 6]])
        ds.setNoDataValue(-1)
        grid = ds.grid().atResolution(resolution=ds.grid().resolution() * 3)

        for resampleAlg in ResampleAlgHandler.translateResamplingAlgorithms():
            print(resampleAlg)
            print(ds.translate(grid=grid, obj=resampleAlg).readAsArray())

    def test_translate_withSubpixelShift(self):
        array = np.array([[[0, 100, 200]]])
        grid = Grid(extent=Extent(xmin=0, xmax=3, ymin=0, ymax=1, projection=Projection.wgs84()), resolution=1)
        rasterdataset = GdalRaster.fromArray(array=array, grid=grid)
        rasterdataset.setNoDataValue(-1)
        shift = 0.1
        gridShifted = Grid(extent=Extent(xmin=0 - shift, xmax=3 - shift, ymin=0, ymax=1, projection=Projection.wgs84()),
                           resolution=1)

        for name in dir(gdal):
            if not name.startswith('GRA_'): continue
            if name in ['GRA_Med', 'GRA_Max', 'GRA_Min']: continue
            resampleAlg = getattr(gdal, name)

            print(name, resampleAlg)
            translated = rasterdataset.translate(grid=gridShifted, obj=resampleAlg)
            assert translated.grid().equal(gridShifted), name
            # warped = rasterdataset.warp(grid=gridShifted, resampleAlg=resampleAlg)
            # assert warped.grid().equal(gridShifted), name

    def test_warpResampleAlgs(self):

        ds = GdalRaster.fromArray(array=[[1, -1, -1, -1, -1, -1],
                                         [-1, 2, -1, -1, -1, -1],
                                         [-1, -1, 3, -1, -1, -1],
                                         [-1, -1, -1, 4, -1, -1],
                                         [-1, -1, -1, -1, 5, -1],
                                         [-1, -1, -1, -1, -1, 6]])
        ds.setNoDataValue(-1)
        grid = ds.grid().atResolution(resolution=ds.grid().resolution() * 3)

        for resampleAlg in ResampleAlgHandler.warpResamplingAlgorithms():
            print(resampleAlg)
            print(ds.warp(grid=grid, resampleAlg=resampleAlg).readAsArray())

    def test(self):
        self.assertIsInstance(obj=raster.grid(), cls=Grid)
        for band in raster.bands():
            self.assertIsInstance(obj=band, cls=RasterBandDataset)
        self.assertIsInstance(raster.driver(), RasterDriver)
        self.assertIsInstance(raster.readAsArray(), np.ndarray)
        self.assertIsInstance(raster.readAsArray(grid=grid), np.ndarray)

        raster2 = GdalRaster.fromArray(grid=grid, array=np.ones(shape=grid.shape()))
        raster2 = GdalRaster.fromArray(grid=grid, array=[np.ones(shape=grid.shape(), dtype=np.bool)],
                                   filename='/vsimem/raster.bsq', driver=EnviDriver())
        raster2.setNoDataValue(value=-9999)
        raster2.noDataValue()
        MemDriver().create(grid=grid).noDataValue(default=-9999)
        raster2.projection()
        raster2.extent()

        raster2.setDescription(value='Hello')
        raster2.description()
        raster2.copyMetadata(other=raster)
        raster2.setMetadataItem(key='a', value=42, domain='my domain')
        raster2.setMetadataItem(key='a', value=None, domain='my domain')
        raster2.setMetadataItem(key='file_compression', value=1, domain='ENVI')
        raster2.setMetadataItem(key='b', value=[1, 2, 3], domain='my domain')
        raster2.metadataItem(key='a', domain='my domain')
        raster2.metadataItem(key='b', domain='my domain')
        raster2.metadataItem(key='not a key', required=False)
        try:
            raster2.metadataItem(key='not a key', required=True)
        except errors.MissingMetadataItemError as error:
            print(error)
        raster2.setMetadataDict(metadataDict=raster2.metadataDict())
        import datetime
        raster2.setAcquisitionTime(acquisitionTime=datetime.datetime(2010, 12, 31))
        print(raster2.acquisitionTime())

        raster2.warp()
        raster2.translate()
        grid2 = Grid(extent=grid.extent(), resolution=Resolution(x=400, y=400))
        raster2.translate(grid=grid2)
        raster2.array()
        raster2.array()

        grid2 = Grid(extent=grid.extent().reproject(projection=Projection.utm(zone=33)),
                     resolution=grid.resolution())
        raster2.array(grid=grid2)
        raster.zprofile(pixel=Pixel(0, 0))
        raster.xprofile(row=Row(0, 0))
        raster.yprofile(column=Column(0, 0))

        raster2.dtype()
        raster2.flushCache()
        raster2.close()

        raster2 = GdalRaster.fromArray(grid=grid, array=[np.ones(shape=grid.shape(), dtype=np.bool)],
                                   filename=join(outdir, 'zeros.tif'), driver=GTiffDriver())

        raster2 = GdalRaster.fromArray(grid=grid, array=[np.ones(shape=grid.shape(), dtype=np.bool)],
                                   filename=join(outdir, 'zeros.img'), driver=EnviDriver())

        raster.filename()
        raster.filenames()

        raster3 = GdalRaster.fromArray(array=np.zeros(shape=(2, 10, 10)))
        raster3.setNoDataValues(values=[1, 2])
        try:
            raster3.noDataValue()
        except errors.HubDcError as error:
            print(error)

    def test_categoryNamesAndLookup(self):
        raster2 = GdalRaster.fromArray(grid=grid, array=np.ones(shape=grid.shape()))
        band = raster2.bandByIndex(0)
        names = ['a', 'b', 'c']
        colors = [(1, 1, 1, 255), (10, 10, 10, 255), (100, 100, 100, 255)]
        band.setCategoryNames(names=names)
        band.setCategoryColors(colors=colors)
        self.assertListEqual(names, band.categoryNames())
        self.assertListEqual(colors, band.categoryColors())

    def test_createVRT(self):
        createVRTDataset(filename=join(outdir, 'stack1.vrt'), rasterDatasetsOrFilenames=[raster, raster])
        createVRTDataset(filename=join(outdir, 'stack2.vrt'),
                         rasterDatasetsOrFilenames=[LT51940232010189KIS01.cfmask] * 2)

    def test_buildOverviews(self):
        filename = join(outdir, 'rasterWithOverviews.bsq')
        GdalRaster.fromArray(array=np.zeros(shape=[1, 1000, 1000]), filename=filename, driver=EnviDriver()).close()
        buildOverviews(filename=filename, minsize=128)
        buildOverviews(filename=filename, minsize=1280)

    def test_plots(self):
        import enmapboxtestdata
        enmap = openRasterDataset(filename=enmapboxtestdata.enmap)
        enmap.plotZProfile(pixel=Pixel(x=0, y=0)).win.close()
        enmap.plotZProfile(pixel=Pixel(x=0, y=0), spectral=True).win.close()
        enmap.plotXProfile(row=Row(0, 0)).win.close()
        enmap.plotYProfile(column=Column(0, 0)).win.close()

        # enmap.plotSinglebandGrey()  # , vmin=0, vmax=9000)
        # enmap.plotSinglebandGrey(index=0, pmin=2, pmax=98)
        # enmap.plotMultibandColor()

        # raster.
        # raster = Raster.fromSample(filename=join(outdir, 'RasterFromSample.bsq'), sample=enmapClassificationSample)
        # print(raster)
        # raster.dataset().plotPixel(pixel=Pixel(x=0, y=0))
        # pyplot.plot(raster.dataset().array()[:,0,0])
