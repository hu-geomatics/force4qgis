from unittest import TestCase
from os.path import join
import numpy as np

from force4qgis.hubforce.core import openRasterDataset

outdir = 'output/core'
vsimem = f'/vsimem/{outdir}'


class TestVectorDriver(TestCase):
    def test_Driver(self):
        self.assertIsInstance(obj=VectorDriver(name='ESRI Shapefile').ogrDriver(), cls=ogr.Driver)
        with self.assertRaises(errors.InvalidOGRDriverError):
            VectorDriver(name='not a valid driver name')

    def test___str__(self):
        print(VectorDriver(name='ESRI Shapefile'))
        print(ESRIShapefileDriver())
        print(GeoPackageDriver())
        print(MemoryDriver())

    def test_equal(self):
        d1 = ESRIShapefileDriver()
        d2 = GeoPackageDriver()
        d3 = VectorDriver(name='ESRI Shapefile')
        self.assertTrue(d1.equal(d3))
        self.assertFalse(d1.equal(d2))

    def test_createPoints(self):
        projection = Projection.wgs84()
        vectorDataset = ESRIShapefileDriver().create(filename=join(outdir, 'VectorDataset.createPoints.shp'), 
        projection=projection)
        self.assertEqual(0, vectorDataset.featureCount())
        self.assertEqual(0, vectorDataset.fieldCount())

        vectorDataset.addField(name='myField', ogrType=ogr.OFTString)
        self.assertEqual(1, vectorDataset.fieldCount())

        for x, y, v in [(0, 0, 'first'), (1, 1, 'second')]:
            feature = Feature.fromVectorDataset(vectorDataset=vectorDataset)
            feature.setGeometry(geometry=Point(x=x, y=y, projection=projection))
            feature.setValue(attribute='myField', value=v)
            vectorDataset.addFeature(feature=feature)
        self.assertEqual(2, vectorDataset.featureCount())

        self.assertListEqual(['first', 'second'], vectorDataset.attributeTable()['myField'])

    def test_createCircles(self):
        projection = Projection.wgs84()
        vectorDataset = ESRIShapefileDriver().create(filename=join(outdir, 'VectorDataset.createCircles.shp'), 
        projection=projection)
        feature = Feature.fromVectorDataset(vectorDataset=vectorDataset)
        feature.setGeometry(geometry=Point(x=0, y=0, projection=projection).buffer(distance=1))
        vectorDataset.addFeature(feature=feature)


    def test_createRectangles(self):
        projection = Projection.wgs84()
        vectorDataset = ESRIShapefileDriver().create(filename=join(outdir, 'VectorDataset.createRectangles.shp'), 
        projection=projection)
        feature = Feature.fromVectorDataset(vectorDataset=vectorDataset)
        feature.setGeometry(geometry=Geometry.fromRectangle(xmin=0, xmax=3, ymin=0, ymax=3, projection=projection))
        vectorDataset.addFeature(feature=feature)
        print(vectorDataset.geometryTypeName())

    def test_fromFilename(self):
        for ext in ['shp', 'gpkg']:
            filename = join(outdir, 'file.' + ext)
            driver = VectorDriver.fromFilename(filename=filename)
            print(driver)
        print(VectorDriver.fromFilename(filename=''))

        try:
            VectorDriver.fromFilename(filename='file.xyz')
        except errors.InvalidOGRDriverError as error:
            print(str(error))

    def test_prepareCreation(self):
        filename = join(outdir, 'prepareCreation', 'vector.shp')
        print(filename)
        driver = ESRIShapefileDriver()
        points = [Point(x, y, Projection.wgs84()) for x, y in ((-1, -1), (1, 1))]
        ds = VectorDataset.fromPoints(points=points, filename=filename, driver=driver)
        ds.close()
        driver.prepareCreation(filename=filename)
        assert not exists(filename)


class TestRasterBand(TestCase):

    def test(self):
        band = raster.bandByIndex(0)
        band.gdalBand()
        try:
            raster.bandByIndex(-1)
        except errors.IndexError as error:
            print(error)

    def test_array(self):
        ds = openRasterDataset(LT51940232010189KIS01.cfmask)
        ds.bandByIndex(0).array()
        ds.bandByIndex(0).array(grid=Grid(extent=ds.extent().reproject(Projection.wgs84()), resolution=0.01))

    def test_readAsArray(self):
        ds = openRasterDataset(LT51940232010189KIS01.cfmask)
        band = ds.bandByIndex(0)
        self.assertIsInstance(obj=band, cls=RasterBandDataset)
        self.assertIsInstance(obj=band.readAsArray(), cls=numpy.ndarray)
        self.assertIsInstance(
            obj=band.readAsArray(grid=ds.grid().subset(offset=Pixel(x=0, y=0), size=RasterSize(x=10, y=10), trim=True)),
            cls=numpy.ndarray)
        with self.assertRaises(errors.AccessGridOutOfRangeError):
            band.readAsArray(grid=ds.grid().subset(offset=Pixel(x=-1, y=-1), size=RasterSize(x=10, y=10)))
        with self.assertRaises(errors.AccessGridOutOfRangeError):
            band.readAsArray(grid=ds.grid().subset(offset=Pixel(x=-10, y=-10), size=RasterSize(x=10, y=10)))
        a = band.readAsArray()
        b = band.readAsArray(grid=ds.grid().subset(offset=Pixel(x=0, y=0), size=ds.grid().size()))
        self.assertTrue(numpy.all(a == b))

    def test_writeArray(self):
        ds = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        array2d = numpy.full(shape=grid.shape(), fill_value=42)
        array3d = numpy.full(shape=grid.shape(), fill_value=42)
        band.writeArray(array=array2d)
        band.writeArray(array=array3d)
        band.writeArray(array=array3d, grid=grid)
        with self.assertRaises(errors.ArrayShapeMismatchError):
            band.writeArray(array=array2d[:10, :10])
        band.writeArray(array=array2d[:10, :10], grid=grid.subset(offset=Pixel(x=0, y=0), size=RasterSize(x=10, y=10)))
        band.writeArray(array=array2d[:10, :10], grid=grid.subset(offset=Pixel(x=-5, y=-5),
        size=RasterSize(x=10, y=10)))
        band.writeArray(array=array2d[:10, :10], grid=grid.subset(offset=Pixel(x=0, y=0), size=RasterSize(x=10, y=10)))
        band.writeArray(array=array2d[:10, :10][None], grid=grid.subset(offset=Pixel(x=0, y=0),
        size=RasterSize(x=10, y=10)))

        with self.assertRaises(errors.AccessGridOutOfRangeError):
            band.writeArray(array=array2d, grid=grid.subset(offset=Pixel(x=10, y=10), size=grid.size()))

    def test_setGetMetadataItem(self):
        ds = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        band.setMetadataItem(key='my key', value=42, domain='ENVI')
        self.assertEqual(band.metadataItem(key='my key', domain='ENVI', dtype=int), 42)
        band.metadataItem(key='my key')
        band.metadataItem(key='not a key')
        try:
            band.metadataItem(key='not a key', required=True)
        except errors.MissingMetadataItemError as error:
            print(error)
        band.setMetadataItem(key='my key', value=None)
        band.metadataDomain(domain='ENVI')
        band.metadataDict()

    def test_copyMetadata(self):
        ds = MemDriver().create(grid=grid)
        ds2 = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        band2 = ds2.bandByIndex(index=0)
        band.setMetadataItem(key='my key', value=42, domain='ENVI')
        band2.copyMetadata(other=band)
        self.assertEqual(band2.metadataItem(key='my key', domain='ENVI', dtype=int), 42)

    def test_setGetNoDataValue(self):
        ds = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        self.assertIsNone(band.noDataValue())
        try:
            band.noDataValue(required=True)
        except errors.MissingNoDataValueError as error:
            print(error)

        self.assertEqual(band.noDataValue(default=123), 123)
        band.setNoDataValue(value=42)
        self.assertEqual(band.noDataValue(default=123), 42)


    def test_setDescription(self):
        ds = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        band.setDescription(value='Hello')
        self.assertEqual(band.description(), 'Hello')

    def test_description(self):
        self.test_setDescription()

    def test_metadataDomainList(self):
        ds = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        band.setMetadataItem(key='my key', value=42, domain='ENVI')
        band.setMetadataItem(key='my key', value=42, domain='xyz')
        gold = {'ENVI', 'xyz'}
        lead = set(band.metadataDomainList())
        self.assertSetEqual(gold, lead)

    def test_fill(self):
        ds = MemDriver().create(grid=grid)
        band = ds.bandByIndex(index=0)
        band.fill(value=42)
        array = band.readAsArray()
        self.assertTrue(numpy.all(array == 42))


class TestVectorDataset(TestCase):
    def test(self):
        gridSameProjection = Grid(extent=vector.extent(), resolution=Resolution(x=1, y=1))
        vector.rasterize(grid=grid)
        vector.rasterize(grid=gridSameProjection, noDataValue=-9999)
        vector.featureCount()
        vector.fieldCount()
        vector.fieldNames()
        vector.fieldTypeNames()
        vector.filename()
        openVectorDataset(filename=BrandenburgDistricts.shp).close()
        try:
            openVectorDataset(filename=BrandenburgDistricts.shp, layerNameOrIndex=-1)
        except errors.InvalidOGRLayerError as error:
            print(error)
        try:
            ds = openVectorDataset(filename=BrandenburgDistricts.shp, layerNameOrIndex=dict())
        except errors.ObjectParserError as error:
            print(error)

    def test_rasterize(self):
        import enmapboxtestdata
        points = openVectorDataset(filename=enmapboxtestdata.landcover_points)
        grid = points.grid(resolution=30)
        raster = points.rasterize(grid=grid)
        print(raster)

    def test_rasterizeOnGridWithDifferentProjection(self):
        import enmapboxtestdata
        points = openVectorDataset(filename=enmapboxtestdata.landcover_points)
        grid = Grid(extent=points.extent().reproject(projection=Projection.wgs84WebMercator()),
                    resolution=30)
        raster = points.rasterize(grid=grid)
        print(raster)

    def test_createFidDataset(self):
        import enmapboxtestdata
        points = openVectorDataset(filename=enmapboxtestdata.landcover_points)
        fid = points.createFidDataset(filename=join(outdir, 'createFidDataset2.gpkg'))
        grid = openRasterDataset(enmapboxtestdata.enmap).grid()
        raster = fid.rasterize(grid=grid, filename=join(outdir, 'rastered2.bsq'), initValue=-1, 
        burnAttribute=fid.fieldNames()[0])
        self.assertListEqual(list(np.unique(raster.readAsArray())), [-1.,  0.,  1.,  2.,  3.,  4.])

    def test_extractPixel(self):
        import enmapboxtestdata
        points = openVectorDataset(filename=enmapboxtestdata.landcover_points)
        rasterValues, vectorValues = points.extractPixel(rasterDataset=openRasterDataset(enmapboxtestdata.enmap))
        print(rasterValues)
        print(vectorValues)

    def test_reproject(self):
        import enmapboxtestdata
        points = openVectorDataset(filename=enmapboxtestdata.landcover_points)
        points = openVectorDataset(filename='regions.gpkg')
        reprojected = points.reproject(projection=Projection.wgs84())
        print(reprojected)

    def test_attributeTable(self):
        print(vector.attributeTable())

class TestExtent(TestCase):
    def test(self):
        extent = grid.extent()
        Extent.fromGeometry(geometry=extent.geometry())
        extent.upperLeft()
        extent.upperRight()
        extent.lowerLeft()
        extent.lowerRight()
        extent.reproject(projection=Projection.wgs84())
        extent.intersects(other=extent)
        extent.intersection(other=extent)
        extent.union(other=extent)
        extent.centroid()


class TestGeometry(TestCase):
    def test(self):
        geometry = grid.extent().geometry()
        print(geometry.intersects(other=geometry))
        print(geometry.union(other=geometry))
        print(geometry.intersection(other=geometry))
        print(geometry.within(other=geometry))


class TestResolution(TestCase):
    def test(self):
        resolution = Resolution(x=30, y=30)
        print(Resolution.fromAny(resolution))
        print(Resolution.fromAny(30))
        print(Resolution.fromAny('30'))
        try:
            print(Resolution.fromAny(dict()))
        except errors.ObjectParserError as error:
            print(error)

        self.assertTrue(resolution.equal(other=Resolution(x=30, y=30)))
        self.assertFalse(resolution.equal(other=Resolution(x=10, y=10)))
        assert (resolution / 2).equal(other=Resolution(15, 15))
        print(resolution / (1, 1))
        print(resolution / '1')
        try:
            print(resolution / dict())
        except errors.TypeError as error:
            print(error)
        assert (resolution * 2).equal(other=Resolution(60, 60))
        print(resolution * (1, 1))
        print(resolution * '1')
        try:
            print(resolution * dict())
        except errors.TypeError as error:
            print(error)



class TestPixel(TestCase):
    def test(self):
        p = Pixel(x=0, y=1)
        Pixel.parse(p)
        Pixel.parse((0, 1))
        try:
            Pixel.parse(dict())
        except errors.ObjectParserError as error:
            print(error)


class TestPoint(TestCase):
    def test(self):

        p = Point(x=0, y=1, projection=Projection.wgs84())
        assert p.x() == 0
        assert p.y() == 1
        assert p.projection().equal(Projection.wgs84())

class TestRasterSize(TestCase):
    def test(self):

        s = RasterSize(x=1, y=1)
        print(s)
        self.assertIsInstance(RasterSize.parse(s), RasterSize)
        self.assertIsInstance(RasterSize.parse((1, 1)), RasterSize)
        try: RasterSize.parse(dict())
        except errors.ObjectParserError as error: print(error)
        try: RasterSize(0, 1)
        except errors.InvalidRasterSize as error: print(error)
        try: RasterSize(1, 0)
        except errors.InvalidRasterSize as error: print(error)


class TestENVI(TestCase):
    def test(self):
        print(Envi.gdalType(enviType=4))
        print(Envi.numpyType(enviType=4))
        print(Envi.typeSize(enviType=4))
        ds = RasterDataset.fromArray(array=np.zeros(shape=(3, 100, 100)), filename=join(outdir, 'raster.bsq'), 
        driver=EnviDriver())
        filenameHeader = Envi.findHeader(filenameBinary=ds.filename(), ext='.hdr')
        assert Envi.findHeader(filenameBinary='not a file') is None
        metadata = Envi.readHeader(filenameHeader=filenameHeader)
        Envi.writeHeader(filenameHeader=join(outdir, 'raster.txt'), metadata=metadata)

class TestProjection(TestCase):
    def test(self):
        print(Projection.fromEpsg(3035))
        print(Projection.wgs84().wkt())
        print(Projection.wgs84())
        print(Projection.utm(zone=33, north=True))
        print(Projection.utm(zone=33, north=False))
        print(Projection.wgs84WebMercator())
        print(Projection.wgs84().equal(other=Projection.wgs84WebMercator()))

class TestAuxClasses(TestCase):
    def test(self):
        print(Column(x=0, z=0))
        print(Row(y=0, z=0))
        print(Pixel(x=0, y=0))

'''
