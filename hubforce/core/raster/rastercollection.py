# from __future__ import annotations
from dataclasses import dataclass
from typing import Tuple, Callable, Sequence

from force4qgis.hubforce.core.metadata.date import DateRange
from force4qgis.hubforce.core.progress import printProgress, Progress
from force4qgis.hubforce.core.raster.grid import Grid, GridShape
from force4qgis.hubforce.core.raster.raster import Raster


@dataclass(frozen=True)
class RasterCollection(object):
    """Raster collection."""
    rasters: Tuple[Raster, ...]
    grids: Tuple[Grid, ...]
    tilenames: Tuple[str, ...]

    def __post_init__(self):
        assert isinstance(self.rasters, tuple)
        for raster in self.rasters:
            assert isinstance(raster, Raster)
        for grid in self.grids:
            assert isinstance(grid, Grid)

    def __len__(self):
        return len(self.rasters)

    def __iter__(self):
        return iter(self.rasters)

    def __getitem__(self, index) -> Raster:
        assert isinstance(index, int)
        return self.rasters[index]

    @property
    def empty(self) -> bool:
        return len(self) == 0

    @property
    def first(self) -> Raster:
        return self.rasters[0]

    def filterDate(self, dateRange: DateRange) -> 'RasterCollection':
        rasters = tuple(raster for raster in self.rasters if raster.date in dateRange)
        return RasterCollection(rasters=rasters, grids=self.grids, tilenames=self.tilenames)

    def select(
            self, selectors, newBandNames=None, newRasterName=None, callback: Callable=None, verbose=1
    ) -> 'RasterCollection':
        return self.map(
            function=Raster.select, selectors=selectors, newBandNames=newBandNames, newRasterName=newRasterName,
            callback=callback, verbose = verbose
        )

    def map(self, function: Callable, *args, callback: Callable = None, verbose=1, **kwargs) -> 'RasterCollection':
        if callback is None:
            callback = printProgress

        if verbose > 0:
            callback(f'RasterCollection.map({function}) 0%..', end='')

        rasters = list()
        for i, raster in enumerate(self.rasters, 1):
            if verbose > 0:
                callback(Progress(i, len(self.rasters)))

            rasters.append(function(raster, *args, **kwargs))

        if verbose > 0:
            callback(f'Done')

        return RasterCollection(rasters=tuple(rasters), grids=self.grids, tilenames=self.tilenames)

    def reduce(self, function: Callable, *args, callback: Callable = None, verbose=1, **kwargs) -> Raster:
        if callback is None:
            callback = printProgress

        if verbose > 0:
            callback(f'RasterCollection.reduce({function}) ..', end='')

        assert not self.empty, 'cannot reduce empty collection'
        raster = function(self, *args, **kwargs)
        assert isinstance(raster, Raster)

        if verbose > 0:
            callback(f'Done')

        return raster

    def toBands(self, name='bandStack') -> Raster:
        """Converts a collection to a single multi-band image containing all of the bands of every image in the
        collection. Output bands are named by prefixing the existing band names with the image name from which
        it came (e.g.: 'image1_band1')."""

        bands = list()
        for i, raster in enumerate(self.rasters):
            for band in raster.bands:
                bands.append(band.rename(name=f'{raster.name}_{band.name}'))
        raster = Raster(name=name, date=None, bands=tuple(bands), grids=self.grids, tilenames=self.tilenames)
        return raster

    def compute(
            self, dirname: str, blockShape: GridShape, extention: str = None, processes: int = None,
            noDataRaster: Raster = None, callback=None, verbose=1
    ):
        from force4qgis.hubforce.core.delayed.compute import computeRasterCollection
        if callback is None:
            callback = printProgress
        return computeRasterCollection(
            rasterCollection=self, dirname=dirname, blockShape=blockShape, extention=extention, processes=processes,
            noDataRaster=noDataRaster, callback=callback, verbose=verbose
        )
