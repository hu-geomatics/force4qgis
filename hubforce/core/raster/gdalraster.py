# from __future__ import annotations

from dataclasses import dataclass
from typing import Optional, List, Union, Any, Dict, Iterable
from os.path import exists

import numpy as np
from osgeo import gdal, gdal_array

from force4qgis.hubforce.core.const.gdaldatatype import GdalDataType
from force4qgis.hubforce.core.const.numpydatatype import NumpyDataType
from force4qgis.hubforce.core.const.rasteraccess import RasterAccess
from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.const.timeseries import (TIMESERIES_DATE_KEY, TIMESERIES_DOMAIN,
                                                       TIMESERIES_DATESTAMP_FORMAT)
from force4qgis.hubforce.core.metadata.date import Date
from force4qgis.hubforce.core.metadata.metadataformatter import MetadataFormatter
from force4qgis.hubforce.core.raster.gdalband import GdalBand
from force4qgis.hubforce.core.raster.geotransform import GeoTransform, GdalGeoTransform
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.shape import RasterShape, GridShape
from force4qgis.hubforce.core.spatial.projection import Projection


@dataclass(frozen=True)
class GdalRaster(object):
    """Raster dataset."""
    gdalDataset: gdal.Dataset

    def __post_init__(self):
        assert isinstance(self.gdalDataset, gdal.Dataset), repr(self.gdalDataset)

    def __getstate__(self):
        from force4qgis.hubforce.core.raster.rasterdriver import MEM_DRIVER
        self.flushCache()
        assert not self.filename.startswith('/vsimem/'), 'cannot pickle /vsimem/ raster'
        # assert not isMemRaster, 'cannot pickle in-memory raster'
        isMemRaster = self.driver == MEM_DRIVER
        if isMemRaster:
            array = self.readAsArray()
        else:
            array = None
        return isMemRaster, self.filename, array, self.grid

    def __setstate__(self, state):
        isMemRaster, filename, array, grid = state
        if isMemRaster:
            raster = GdalRaster.fromArray(array=array, grid=grid)
        else:
            raster = GdalRaster.open(filename=filename)
        self.__init__(gdalDataset=raster.gdalDataset)

    def __del__(self):
        self.flushCache()

    @property
    def geoTransform(self) -> GeoTransform:
        gdalGeoTransform = GdalGeoTransform(*self.gdalDataset.GetGeoTransform())
        return GeoTransform.fromGdalGeoTransform(gdalGeoTransform)

    @property
    def shape(self) -> RasterShape:
        return RasterShape(
            x=self.gdalDataset.RasterXSize, y=self.gdalDataset.RasterYSize, z=self.gdalDataset.RasterCount
        )

    @property
    def grid(self) -> Grid:
        shape = GridShape(x=self.gdalDataset.RasterXSize, y=self.gdalDataset.RasterYSize)
        projection = Projection(wkt=str(self.gdalDataset.GetProjection()))
        return Grid.fromGeoTransform(geoTransform=self.geoTransform, shape=shape, projection=projection)

    @property
    def driver(self) -> 'RasterDriver':
        from force4qgis.hubforce.core.raster.rasterdriver import RasterDriver
        gdalDriver: gdal.Driver = self.gdalDataset.GetDriver()
        return RasterDriver(name=gdalDriver.ShortName)

    @classmethod
    def fromFilename(cls, filename: str, access=RasterAccess.ReadOnly):

        assert isinstance(filename, str), type(filename)
        assert exists(filename) or filename.startswith('/vsimem/'), filename
        gdalDataset = gdal.Open(filename, access.value)
        assert gdalDataset is not None, filename
        assert gdalDataset.GetProjection() != ''
        return cls(gdalDataset=gdalDataset)

    open = fromFilename

    @classmethod
    def fromArray(
            cls, array: np.ndarray, grid: Optional[Grid] = None, filename: Optional[str] = None,
            driver: Optional['RasterDriver'] = None, options: Optional[List[str]] = None
    ) -> 'GdalRaster':
        from force4qgis.hubforce.core.raster.rasterdriver import RasterDriver

        if driver is None:
            driver = RasterDriver.fromFilename(filename=filename)
        return driver.createFromArray(array=array, grid=grid, filename=filename, options=options)

    @property
    def filenames(self) -> str:
        """Return filenames list."""
        filenames = self.gdalDataset.GetFileList()
        if filenames is None:
            filenames = []
        return filenames

    @property
    def filename(self) -> str:
        """Return filename."""
        return self.description

    def setGrid(self, grid: Grid):
        """Set grid."""
        assert isinstance(grid, Grid)
        self.gdalDataset.SetGeoTransform(grid.geoTransform)
        self.gdalDataset.SetProjection(grid.projection.wkt)

    def band(self, number) -> GdalBand:
        """Return the band dataset."""
        return GdalBand(gdalDataset=self.gdalDataset, gdalBand=self.gdalDataset.GetRasterBand(number), number=number)

    @property
    def bands(self) -> Iterable[GdalBand]:
        """Iterate over all bands."""
        for i in range(self.shape.z):
            yield self.band(number=i + 1)

    def readAsArray(
            self, grid: Optional[Grid] = None, resampling=Resampling.NearestNeighbour
    ) -> np.ndarray:
        """Read as 3d array."""

        if grid is None:
            array = self.gdalDataset.ReadAsArray()
        else:
            assert isinstance(grid, Grid)
            # assert grid.within(self.grid)
            assert grid.extent.within(self.grid.extent)
            resolution = self.grid.resolution
            extent = self.grid.extent
            xoff = int(round((grid.extent.xmin - extent.xmin) / resolution.x, 0))
            yoff = int(round((extent.ymax - grid.extent.ymax) / resolution.y, 0))
            xsize = int(round((grid.extent.xmax - grid.extent.xmin) / resolution.x, 0))
            ysize = int(round((grid.extent.ymax - grid.extent.ymin) / resolution.y, 0))
            buf_ysize, buf_xsize = grid.shape
            resampleAlg = resampling.gdalCode
            array = self.gdalDataset.ReadAsArray(
                xoff=xoff, yoff=yoff, xsize=xsize, ysize=ysize, buf_xsize=buf_xsize, buf_ysize=buf_ysize,
                resample_alg=resampleAlg)

        if array.ndim == 2:
            array = np.expand_dims(array, 0)
        assert array.ndim == 3
        return array

    # def readAsNanArray(
    #         self, grid: Optional[Grid] = None, resampling=Resampling.NearestNeighbour
    # ) -> np.ndarray:
    #     """Read as float32 3d array with no data values replaced by NaN values."""
    #     array = self.readAsArray(grid=grid, resampling=resampling).astype(np.float32)
    #     for a, band in zip(array, self.bands):
    #         if band.noDataValue() is not None:
    #             a[a == band.noDataValue()] = np.nan
    #     return array

    def writeArray(self, array: np.ndarray, grid: Optional[Grid] = None):
        """Write 3d array."""

        assert isinstance(array, np.ndarray)
        assert array.ndim == 3
        assert len(array) == self.shape.z
        for band, bandArray in zip(self.bands, array):
            band.writeArray(bandArray, grid=grid)

    def flushCache(self):
        """Flush the cache."""
        self.gdalDataset.FlushCache()

    def setNoDataValue(self, value: Optional[float]):
        """Set unique no data value to all bands."""
        for i, band in enumerate(self.bands):
            band.setNoDataValue(value)

    def noDataValue(self, default: Optional[float] = None, required: bool = False) -> float:
        """Return unique no data value (or default if missing)."""
        noDataValues = [band.noDataValue() for band in self.bands]
        assert len(set(noDataValues)) == 1, f'multiple no data values found: {noDataValues}'
        noDataValue = noDataValues[0]
        if noDataValue is None:
            noDataValue = default
        if required:
            assert noDataValue is not None
        return noDataValue

    def setDescription(self, value: str):
        """Set description."""
        self.gdalDataset.SetDescription(value)

    @property
    def description(self) -> str:
        """Return description, which is equal to the filename."""
        return self.gdalDataset.GetDescription()

    def metadataDomainList(self) -> List[str]:
        """Returns the list of metadata domain names."""
        domainList = self.gdalDataset.GetMetadataDomainList()
        if domainList is None:
            return list()
        return domainList

    def metadataItem(
            self, key: str, domain: str, dtype: Optional[Union[str, float, int]] = None, required=False, default=None):
        """Return (type-casted) metadata value.
        If metadata item is missing, but not required, return the default value."""

        if dtype is None:
            dtype = str
        key = key.replace(' ', '_')
        gdalString = self.gdalDataset.GetMetadataItem(key, domain)
        if gdalString is None:
            assert not required, f'missing metadata item: {key} in {domain}'
            return default
        return MetadataFormatter.stringToValue(gdalString, dtype=dtype)

    def metadataDomain(self, domain=''):
        """Returns the metadata dictionary for the given ``domain``."""
        values = dict()
        for key in self.gdalDataset.GetMetadata(domain):
            key = key.replace('_', ' ')
            values[key] = self.metadataItem(key=key, domain=domain)
        return values

    @property
    def metadataDict(self):
        """Returns the metadata dictionary for all domains."""
        values = dict()
        for domain in self.metadataDomainList():
            values[domain] = self.metadataDomain(domain=domain)
        return values

    def setMetadataItem(self, key: str, value: Union[Any, List[Any]], domain: str = ''):
        """Set metadata item."""
        if value is None:
            return
        key = key.replace(' ', '_').strip()
        gdalString = MetadataFormatter.valueToString(value)
        self.gdalDataset.SetMetadataItem(key, gdalString, domain)

    def setMetadataDomain(self, values: Dict[str, Union[Any, List[Any]]], domain: str):
        """Set the metadata domain."""
        assert isinstance(values, dict)
        for key, value in values.items():
            self.setMetadataItem(key=key, value=value, domain=domain)

    def setMetadataDict(self, values=Dict[str, Dict[str, Union[Any, List[Any]]]]):
        """Set the metadata."""
        assert isinstance(values, dict)
        for domain, metadataDomain in values.items():
            self.setMetadataDomain(values=metadataDomain, domain=domain)

    def date(self, required=False, default=None):
        """Return aquisition date."""
        date = self.metadataItem(
            key=TIMESERIES_DATE_KEY, domain=TIMESERIES_DOMAIN, required=required, default=default,
            dtype=Date.fromDatestamp
        )
        return date

    def setDate(self, date: Date):
        """Set aquisition date."""
        assert isinstance(date, Date)
        self.setMetadataItem(
            key=TIMESERIES_DATE_KEY, value=date.format(TIMESERIES_DATESTAMP_FORMAT), domain=TIMESERIES_DOMAIN
        )

    @property
    def dtype(self):
        return self.numpyDataType.value

    @property
    def numpyDataType(self) -> NumpyDataType:
        """Return raster data type."""
        gdalTypeCode = self.gdalDataType.value
        numpyTypeCode = gdal_array.GDALTypeCodeToNumericTypeCode(gdalTypeCode)
        return NumpyDataType(numpyTypeCode)

    @property
    def gdalDataType(self) -> GdalDataType:
        return GdalDataType(self.band(1).gdalBand.DataType)
