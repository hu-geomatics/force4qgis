# from __future__ import annotations

import numpy as np

from dataclasses import dataclass
from os.path import basename, splitext
from typing import Tuple, List, Union, Optional, Sequence

from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.metadata.date import Date
from force4qgis.hubforce.core.progress import printProgress
from force4qgis.hubforce.core.raster.band import Band
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.shape import GridShape
from force4qgis.hubforce.core.spatial.location import Location

DEBUG = False


@dataclass(frozen=True)
class Raster(object):
    """Raster."""
    name: str
    date: Optional[Date]
    bands: Tuple[Band, ...]
    grids: Tuple[Grid, ...]
    tilenames: Tuple[str, ...]

    def __post_init__(self):
        assert isinstance(self.name, str)
        assert isinstance(self.date, (Date, type(None)))
        assert isinstance(self.bands, tuple)
        assert isinstance(self.grids, tuple)
        assert isinstance(self.tilenames, tuple)
        for band in self.bands:
            assert isinstance(band, Band)
        for grid in self.grids:
            assert isinstance(grid, Grid)
            assert grid.resolution.equal(self.grids[0].resolution)
            assert grid.projection == self.grids[0].projection
        for tilename in self.tilenames:
            assert isinstance(tilename, str)
        assert len(self.grids) == len(self.tilenames)
        assert len(self.bandNames) == len(set(self.bandNames)), f'duplicated band names: {self.bandNames}'

    @staticmethod
    def open(
            filenames: Sequence[str], tilenames: Tuple[str], name: str = None, date: Date = None,
            bandNames: Sequence[str] = None
    ) -> 'Raster':
        assert isinstance(filenames, Sequence)
        assert len(filenames) > 0
        gdalRasters = [GdalRaster.open(filename=filename) for filename in filenames]

        for gdalRaster in gdalRasters:
            assert gdalRaster.shape.z == gdalRasters[0].shape.z, 'number of raster bands mismatch'
        grids = tuple(gdalRaster.grid for gdalRaster in gdalRasters)
        dtype = gdalRasters[0].dtype
        if bandNames is None:
            bandNames = list()
            for i, gdalBand in enumerate(gdalRasters[0].bands):
                bandName = gdalBand.description
                if bandName == '':
                    bandName = f'B{i + 1}'
                bandNames.append(bandName)
        assert len(bandNames) == gdalRasters[0].shape.z

        def bands():
            for number, name, in enumerate(bandNames, 1):
                noDataValue = gdalRasters[0].band(number).noDataValue()
                yield Band(
                    name=name, noDataValue=noDataValue,
                    filenames=tuple(filenames), numbers=(number,) * len(grids), grids=grids,
                    tilenames=tilenames, resampling=Resampling.NearestNeighbour, dtype=dtype, mask=None)

        if name is None:
            name = splitext(basename(gdalRasters[0].description))[0]
        return Raster(name=name, date=date, bands=tuple(bands()), grids=grids, tilenames=tilenames)

    def __len__(self):
        return len(self.bands)

    def __iter__(self):
        return iter(self.bands)

    @property
    def bandNames(self):
        return [band.name for band in self.bands]

    def band(self, number: int) -> Band:
        return self.bands[number - 1]

    def select(
            self, selectors: Sequence[Union[str, int]], newBandNames: Sequence[str] = None,
            newRasterName: str = None
    ) -> 'Raster':

        # derives band numbers and new names
        numbers = list()
        bandNames = self.bandNames
        assert isinstance(selectors, (list, tuple))
        for selector in selectors:
            if isinstance(selector, int):
                assert 1 <= selector <= len(self)
                number = selector
            elif isinstance(selector, str):
                number = bandNames.index(selector) + 1
            else:
                assert 0
            numbers.append(number)
        if newBandNames is None:
            newBandNames = (self.bands[number - 1].name for number in numbers)
        else:
            assert len(selectors) == len(newBandNames)
        if newRasterName is None:
            newRasterName = self.name

        # subset bands
        bands = tuple(self.bands[number - 1].rename(name) for number, name in zip(numbers, newBandNames))
        raster = Raster(name=newRasterName, date=self.date, bands=bands, grids=self.grids, tilenames=self.tilenames)
        return raster

    def rename(self, name: str = None, bandNames: Sequence[str] = None) -> 'Raster':
        """Rename raster and raster bands."""
        if name is None:
            name = self.name
        if bandNames is None:
            bandNames = self.bandNames
        assert len(bandNames) == len(self.bands)
        selectors = list(range(1, len(self.bands) + 1))
        raster = self.select(selectors=selectors, newBandNames=bandNames, newRasterName=name)
        return raster

    def addBands(self, raster: 'Raster', names: List[str] = None) -> 'Raster':
        """Returns an image containing all bands copied from the first input and selected bands from the second input.
        The new image has the metadata from the first input image."""

        if names is None:
            bands2 = raster.bands
        else:
            bands2 = raster.select(selectors=names)

        return Raster(
            name=self.name, date=self.date, bands=self.bands + bands2, grids=self.grids, tilenames=self.tilenames
        )

    def setMask(self, mask: 'Raster') -> 'Raster':
        if len(self.bands) == len(mask.bands):
            maskBands = mask.bands
        elif len(mask.bands) == 1:
            maskBands = mask.bands * len(self.bands)
        else:
            assert 0
        bands = tuple(band.setMask(mask=maskBand) for band, maskBand in zip(self.bands, maskBands))
        return Raster(name=self.name, date=self.date, bands=bands, grids=self.grids, tilenames=self.tilenames)

    def setDate(self, date: Date) -> 'Raster':
        return Raster(name=self.name, date=date, bands=self.bands)

    def setName(self, name: str) -> 'Raster':
        return Raster(name=name, date=self.date, bands=self.bands)

    def expression(self, expression, name='EXPRESSION', outNoDataValue=None, outType=None):
        from force4qgis.hubforce.algorithms.bandmath import bandmath
        return bandmath(expression=expression, raster=self, name=name, outNoDataValue=outNoDataValue, outType=outType)

    def sampleFeatures(self, filename, debug=DEBUG, allTouched=None):
        assert isinstance(filename, str)

        features = list()
        labels = list()

        for tilename in self.tilenames:

            ######################
            # using HUBDC API here
            from hubdc.core import openVectorDataset, openRasterDataset
            tileindex = self.band(0).tilenames.index(tilename)
            grid = openRasterDataset(self.band(0).filenames[tileindex]).grid()
            vector = openVectorDataset(filename=filename)
            vector.ogrLayer().SetSpatialFilter(grid.extent().geometry().ogrGeometry())

            fidVector = vector.createFidDataset(filename=f'/vsimem/data/fid{tileindex}.gpkg', fidName='_fid')

            if fidVector.featureCount() == 0:
                continue

            fidArray = fidVector.rasterize(initValue=-1, burnAttribute='_fid', grid=grid, allTouched=allTouched,
                filename=f'/vsimem/data/fid{tileindex}.tif').readAsArray()[0]

            valid = fidArray != -1
            xs = grid.xMapCoordinatesArray()[valid]
            ys = grid.yMapCoordinatesArray()[valid]
            ######################

            fidsUnsorted = fidArray[valid]

            xs, ys, fids = list(zip(*sorted(zip(xs, ys, fidsUnsorted), key=lambda t: t[2])))
            locations = [Location(x=x, y=y) for x, y in zip(xs, ys)]
            fs = np.array([band.sampleLocations(locations=locations, tilename=tilename, debug=debug)
                           for band in self.bands])
            ls = vector.attributeTable()
            ls = {key: list(np.array(ls[key])[np.array(fids, dtype=np.uint64)]) for key in ls}

            features.append(fs)
            labels.append(ls)

        if len(features) == 0 and len(labels) == 0:
            return None

        features = np.concatenate(features, axis=1)
        for ilabels in labels[1:]:
            for key in labels[0]:
                labels[0][key].extend(ilabels[key])
        labels = labels[0]
        for k, v in labels.items():
            labels[k] = np.array(v)

        return features, labels

    def predict(self, estimatorFilename: str, name=None, bandNames=None):
        from force4qgis.hubforce.algorithms.estimatorpredict import estimatorPredict
        return estimatorPredict(estimatorFilename=estimatorFilename, raster=self, name=name, bandNames=bandNames)

    def predictProbability(self, estimatorFilename: str, name=None, bandNames=None):
        from force4qgis.hubforce.algorithms.estimatorpredictprobability import estimatorPredictProbability
        return estimatorPredictProbability(estimatorFilename=estimatorFilename, raster=self, name=name,
            bandNames=bandNames)

    def compute(self, dirname: str, blockShape: GridShape, extention: str = None, processes: int = None, callback=None):
        from force4qgis.hubforce.core.delayed.compute import computeRaster
        if callback is None:
            callback = printProgress
        computeRaster(
            raster=self, dirname=dirname, blockShape=blockShape, extention=extention, processes=processes,
            callback=callback
        )
