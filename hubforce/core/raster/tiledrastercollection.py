from dataclasses import dataclass
from typing import Tuple

from force4qgis.hubforce.core import Grid
from force4qgis.hubforce.core import RasterCollection


@dataclass(frozen=True)
class TiledRasterCollection(object):
    """Raster collection."""
    rasterCollections: Tuple[RasterCollection, ...]

    def __post_init__(self):
        assert isinstance(self.rasterCollections, tuple)
        for rasterCollection in self.rasterCollections:
            assert isinstance(rasterCollection, RasterCollection)

    def __len__(self):
        return len(self.rasterCollections)

    def __iter__(self):
        return iter(self.rasterCollections)

    @property
    def empty(self) -> bool:
        return len(self) == 0

    @property
    def first(self) -> RasterCollection:
        assert not self.empty
        return self.rasterCollections[0]

    @property
    def grid(self) -> Grid:
        return self.first.grid