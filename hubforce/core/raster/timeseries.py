# from __future__ import annotations
from dataclasses import dataclass
from typing import List, Iterable

from force4qgis.hubforce.core import TimeseriesShape, DateRange
from force4qgis.hubforce.core import TIMESERIES_DOMAIN, TIMESERIES_DATESTAMP_FORMAT
from force4qgis.hubforce.core import Date
from force4qgis.hubforce.core import Timeband


@dataclass(frozen=True)
class Timeseries(GdalRaster):

    def __post_init__(self):
        GdalRaster.__post_init__(self)
        assert self.dates is not None
        assert self.names is not None
        assert len(self.dates) * len(self.names) == self.shape.z

    @classmethod
    def fromRaster(cls, raster) -> Timeseries:
        return cls(gdalDataset=raster.gdalDataset)

    @property
    def timebands(self) -> Iterable[Timeband]:
        for band in self.bands:
            yield Timeband(gdalDataset=band.gdalDataset, gdalBand=band.gdalBand, number=band.number)

    @property
    def dates(self) -> List[Date]:
        default = [None] * self.shape.z
        dates = list()
        for datestamp in self.metadataItem(key='dates', domain=TIMESERIES_DOMAIN, default=default):
            if datestamp is not None:
                datestamp = Date.fromDatestamp(datestamp=datestamp)
            dates.append(datestamp)
        return dates

    @property
    def names(self) -> List[str]:
        default = [None]
        return self.metadataItem(key='names', domain=TIMESERIES_DOMAIN, default=default)

    def setDates(self, dates: List[Date]):
        datestamps = [date.format(pattern=TIMESERIES_DATESTAMP_FORMAT) for date in dates]
        self.setMetadataItem(key='dates', value=datestamps, domain=TIMESERIES_DOMAIN)

    def setNames(self, names: List[str]):
        self.setMetadataItem(key='names', value=names, domain=TIMESERIES_DOMAIN)

    @property
    def shape4d(self) -> TimeseriesShape:
        z, y, x = self.shape
        return TimeseriesShape(c=len(self.names), t=len(self.dates), y=y, x=x)

    def filteredIndices(self, dateRange: DateRange = None, names: List[str] = None):
        if dateRange is not None:
            assert isinstance(dateRange, DateRange)
        if names is not None:
            for name in names:
                assert isinstance(name, str)
                assert name in self.names

        indices = list()
        index = 0
        for date in self.dates:
            for name in self.names:
                invalid = False
                if dateRange is not None:
                    if date not in dateRange:
                        invalid = True
                if names is not None:
                    if name not in names:
                        invalid = True
                if not invalid:
                    indices.append(index)
                index += 1
        return indices


@dataclass(frozen=True)
class SpectralTimeseries(Timeseries):

    def __post_init__(self):
        Timeseries.__post_init__(self)
        assert self.wavelength is not None
        assert len(self.wavelength) == len(self.names)

    @property
    def wavelength(self):
        return self.metadataItem(key='wavelength', domain=TIMESERIES_DOMAIN, dtype=float)
