# from __future__ import annotations
from dataclasses import dataclass
from typing import Optional, Tuple, Sequence, Union

import numpy as np

from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.spatial.location import Location


DEBUG = True


@dataclass(frozen=True)
class Band(object):
    """Tiled raster band."""
    name: str
    noDataValue: Optional[Union[float, int]]
    filenames: Tuple[str, ...]
    numbers: Tuple[int, ...]
    grids: Tuple[Grid, ...]
    tilenames: Tuple[str, ...]
    resampling: Resampling
    dtype: np.dtype
    mask: Optional['Band']

    def __post_init__(self):
        assert isinstance(self.name, str)
        assert isinstance(self.noDataValue, (float, int, type(None)))
        for filename in self.filenames:
            assert isinstance(filename, str)
        for number in self.numbers:
            assert isinstance(number, int) and number >= 1
        for tilename in self.tilenames:
            assert isinstance(tilename, str)
        for grid in self.grids:
            assert isinstance(grid, Grid)
            assert grid.resolution.equal(self.grids[0].resolution)
            assert grid.projection == self.grids[0].projection
        try:
            assert len(self.filenames) == len(self.numbers) == len(self.grids) == len(self.tilenames)
        except:
            assert 0
        assert isinstance(self.resampling, Resampling)
        assert isinstance(self.dtype, type)
        assert isinstance(self.mask, (Band, type(None)))

    @property
    def resolution(self):
        return self.grids[0].resolution

    @property
    def projection(self):
        return self.grids[0].projection

    def rename(self, name) -> 'Band':
        return Band(
            name=name, noDataValue=self.noDataValue,
            filenames=self.filenames, numbers=self.numbers, grids=self.grids, tilenames=self.tilenames,
            resampling=self.resampling, dtype=self.dtype, mask=self.mask
        )

    def setMask(self, mask: 'Band'):
        assert self.grids == mask.grids
        return Band(
            name=self.name, noDataValue=self.noDataValue,
            filenames=self.filenames, numbers=self.numbers, grids=self.grids, tilenames=self.tilenames,
            resampling=self.resampling,
            dtype=self.dtype, mask=mask
        )

    def sampleLocations(self, locations: Sequence[Location], tilename: str, debug=DEBUG):
        tileindex = self.tilenames.index(tilename)
        filename = self.filenames[tileindex]
        number = self.numbers[tileindex]
        if debug:
            print(f'sample from {filename} / {number}')

        gdalBand = GdalRaster.open(filename=filename).band(number=number)
        # grid = self.grids[tileindex]
        grid = gdalBand.grid

        values = list()

        for location in locations:
            pixel = grid.pixelLocation(location=location)
            value = gdalBand.gdalBand.ReadAsArray(int(pixel.x), int(pixel.y), 1, 1)
            assert value is not None
            values.append(value[0, 0])
        return values
