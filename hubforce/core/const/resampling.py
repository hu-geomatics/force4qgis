# from __future__ import annotations
from enum import Enum
from osgeo import gdal

GDAL_INT_TO_STR_MAPPING = dict([
    (gdal.GRA_NearestNeighbour, 'near'),
    (gdal.GRA_Bilinear, 'bilinear'),
    (gdal.GRA_Cubic, 'cubic'),
    (gdal.GRA_CubicSpline, 'cubicspline'),
    (gdal.GRA_Lanczos, 'lanczos'),
    (gdal.GRA_Average, 'average'),
    (gdal.GRA_Mode, 'mode'),
    (gdal.GRIORA_Gauss, 'gauss'),
    (gdal.GRA_Max, 'max'),
    (gdal.GRA_Min, 'min'),
    (gdal.GRA_Med, 'med'),
    (gdal.GRA_Q1, 'q1'),
    (gdal.GRA_Q3, 'q3')])


class Resampling(Enum):
    NearestNeighbour = 'near'
    Bilinear = 'bilinear'
    Cubic = 'cubic'
    CubicSpline = 'cubicspline'
    Lanczos = 'lanczos'
    Average = 'average'
    Mode = 'mode'
    Gauss = 'gauss'
    Max = 'max'
    Min = 'min'
    Med = 'med'
    Q1 = 'q1'
    Q3 = 'q3'

    @classmethod
    def fromName(cls, name: str) -> 'Resampling':
        """Translate clear names into corresponding option."""
        for option in Resampling:
            if option.name == name:
                return option
        raise ValueError(f'invalid name: {name}')

    @classmethod
    def fromGdalCode(cls, number: int) -> 'Resampling':
        option = cls.GDAL_INT_TO_STR_MAPPING.get(number, None)
        if option is None:
            raise ValueError(f'invalid number: {number}')
        return option

    @property
    def gdalCode(self):
        for code, name in GDAL_INT_TO_STR_MAPPING.items():
            if self.value == name:
                return code
        assert 0, 'unexpected error'

    @classmethod
    def isValidWarpOption(cls, option: 'Resampling') -> bool:
        return option not in [Resampling.Gauss]

    @classmethod
    def isValidTranslateOption(cls, option: 'Resampling') -> bool:
        return option not in [Resampling.Max, Resampling.Min, Resampling.Med,
                              Resampling.Q1, Resampling.Q3]
