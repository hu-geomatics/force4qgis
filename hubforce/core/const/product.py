from enum import Enum, auto


class Product(Enum):
    Min = auto()
    Mean = auto()
    Median = auto()
    Max = auto()
    Std = auto()
    Percentiles = auto()
    #First = auto()
    #Last = auto()
    #Central = auto()
