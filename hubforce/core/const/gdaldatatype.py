from enum import Enum

from osgeo import gdal


class GdalDataType(Enum):
    """
    >>> GdalDataType.Int32
    <GdalDataTypeOption.Int32: 5>
    >>> GdalDataType.Int32.value == gdal.GDT_Int32
    True
    >>> GdalDataType(gdal.GDT_Int32) == GdalDataType.Int32
    True
    """
    Unknown = 0
    Byte = 1
    UInt16 = 2
    Int16 = 3
    UInt32 = 4
    Int32 = 5
    Float32 = 6
    Float64 = 7
    CInt16 = 8
    CInt32 = 9
    CFloat32 = 10
    CFloat64 = 11
    TypeCount = 12
