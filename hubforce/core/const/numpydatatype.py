from enum import Enum

import numpy as np

class NumpyDataType(Enum):
    """
    >>> NumpyDataType.Int32
    <NumpyDataTypeOption.Int32: <class 'numpy.int32'>>
    >>> NumpyDataType.Int32.value == np.int32
    True
    >>> NumpyDataType(np.int32) == NumpyDataType.Int32
    True
    """
    Bool = np.bool
    Int8 = np.int8
    Int16 = np.int16
    Int32 = np.int32
    Int64 = np.int64
    UInt8 = np.uint8
    UInt16 = np.uint16
    UInt32 = np.uint32
    UInt64 = np.uint64
    Float32 = np.float32
    Float64 = np.float64
