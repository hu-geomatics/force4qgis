from enum import Enum


class RasterAccess(Enum):
    ReadOnly = 0
    Update = 1
