# from __future__ import annotations
from os import makedirs
from os.path import exists, dirname

from dataclasses import dataclass
from typing import Union, Any


@dataclass(frozen=False)
class Progress(object):
    i: int
    n: int

    @property
    def percentage(self) -> float:
        return self.i / self.n * 100


@dataclass
class DetailedProgress(Progress):
    i: int
    n: int
    message: str


def printProgress(progress: Union[Any, Progress], end=None, file=None, flush=True):
    if isinstance(progress, DetailedProgress):
        print(f'{progress.i + 1} / {progress.n} {progress.message}', file=file, flush=flush)
    elif isinstance(progress, Progress):
        p = round(progress.percentage)
        print(f'{p}%', end='..', file=file, flush=flush)
    else:
        print(str(progress), end=end, file=file, flush=flush)


def makeLogProgress(filename: str):
    try:
        makedirs(dirname(filename))
    except:
        pass

    def logProgress(progress: Union[Any, Progress], end=None, flush=True):
        printProgress(progress=progress, end=end, flush=flush)
        with open(filename, 'a') as file:
            printProgress(progress=progress, end=end, file=file, flush=flush)

    return logProgress