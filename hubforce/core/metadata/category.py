from dataclasses import dataclass, field

from force4qgis.hubforce.core.base import DataClassArray
from force4qgis.hubforce.core.metadata.color import Color


@dataclass(frozen=True)
class Category(DataClassArray):
    name: str
    color: Color = field(default_factory=Color)