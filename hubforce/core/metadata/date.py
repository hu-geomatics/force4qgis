# from __future__ import annotations
from dataclasses import dataclass
import datetime

from force4qgis.hubforce.core.base import DataClassIterable


@dataclass(frozen=True)
class Date(DataClassIterable):
    year: int
    month: int
    day: int

    def __post_init__(self):
        assert isinstance(self.year, int)
        assert isinstance(self.month, int)
        assert isinstance(self.day, int)
        assert isinstance(self.date, datetime.date)

    def __lt__(self, other):
        return self.date.__lt__(other.date)

    def __le__(self, other):
        return self.date.__le__(other.date)

    def __gt__(self, other):
        return self.date.__gt__(other.date)

    def __ge__(self, other):
        return self.date.__ge__(other.date)

    @staticmethod
    def daysInYear(year: int) -> int:
        days = int((Date(year=year + 1, month=1, day=1).date - Date(year=year, month=1, day=1).date).days)
        return days

    @staticmethod
    def isLeapYear(year: int):
        return Date.daysInYear(year=year) == 366

    @property
    def decimalYear(self) -> float:
        days = (self.date - Date(year=self.year, month=1, day=1).date).days
        daysInYear = Date.daysInYear(year=self.year)
        return float(self.year) + days / daysInYear

    @property
    def date(self) -> datetime.date:
        return datetime.date(*self)

    def addDays(self, days: int) -> 'Date':
        date = self.date + datetime.timedelta(days=days)
        return Date.fromDate(date)

    def addMonths(self, months: int) -> 'Date':
        month = self.month + months
        addYears, month = divmod(month, 12)
        if month == 0:
            addYears -= 1
            month = 12
        date = Date(year=self.year + addYears, month=month, day=self.day)
        return date

    @classmethod
    def fromDate(cls, date: datetime.date) -> 'Date':
        return Date(date.year, date.month, date.day)

    @classmethod
    def fromDatestamp(cls, datestamp: str, pattern='yyyy-mm-dd') -> 'Date':
        assert isinstance(datestamp, str)
        assert len(datestamp) == len(pattern)
        year = int(datestamp[pattern.find('y'):pattern.rfind('y') + 1])
        month = int(datestamp[pattern.find('m'):pattern.rfind('m') + 1])
        day = int(datestamp[pattern.find('d'):pattern.rfind('d') + 1])
        return Date(year, month, day)

    def format(self, pattern='yyyy-mm-dd') -> str:
        pattern = pattern.replace('yyyy', str(self.year).zfill(4))
        pattern = pattern.replace('mm', str(self.month).zfill(2))
        pattern = pattern.replace('dd', str(self.day).zfill(2))
        return pattern


@dataclass(frozen=True)
class DateRange(DataClassIterable):
    start: Date  # inclusive
    end: Date  # inclusive

    def __post_init__(self):
        assert isinstance(self.start, Date)
        assert isinstance(self.end, Date)
        assert (self.end.date - self.start.date).days >= 0, 'end date before start date'

    def __contains__(self, item: Date) -> bool:
        if item is None:
            return False
        assert isinstance(item, Date)
        return self.start.date <= item.date <= self.end.date

    @property
    def date(self) -> datetime.date:
        return datetime.date(*self)

    @property
    def center(self) -> Date:
        date = self.start.date + datetime.timedelta(days=int((self.end.date - self.start.date).days / 2))
        return Date.fromDate(date)

    @classmethod
    def Year(cls, year: int):
        return DateRange(start=Date(year=year, month=1, day=1), end=Date(year=year + 1, month=1, day=1).addDays(-1))

    @classmethod
    def Month(cls, year: int, month: int):
        start = Date(year, month, 1)
        if month < 12:
            end = Date(year, month + 1, 1)
        else:
            end = Date(year + 1, 1, 1)
        return DateRange(start=start, end=end.addDays(-1))

    @classmethod
    def Day(cls, year: int, month: int, day: int):
        day = Date(year, month, day)
        return DateRange(start=day, end=day)

    def format(self, pattern='yyyymmdd'):
        return f'{self.start.format(pattern=pattern)}-{self.end.format(pattern=pattern)}'


@dataclass(frozen=True)
class DayOfYear(DataClassIterable):
    month: int
    day: int

    def format(self, pattern='mm-dd') -> str:
        pattern = pattern.replace('mm', str(self.month).zfill(2))
        pattern = pattern.replace('dd', str(self.day).zfill(2))
        return pattern


@dataclass(frozen=True)
class DateSeason(DataClassIterable):
    start: DayOfYear
    end: DayOfYear

    def __contains__(self, item: Date) -> bool:
        if item is None:
            return False
        assert isinstance(item, Date)

        startDate = Date(year=item.year, month=self.start.month, day=self.start.day)
        endDate = Date(year=item.year, month=self.end.month, day=self.end.day)
        if startDate.date <= endDate.date:
            dateRange = DateRange(start=startDate, end=endDate)
            return item in dateRange
        else:
            startDate1 = Date(year=item.year - 1, month=self.start.month, day=self.start.day)
            startDate2 = startDate
            endDate1 = endDate
            endDate2 = Date(year=item.year + 1, month=self.end.month, day=self.end.day)
            dateRange1 = DateRange(start=startDate1, end=endDate1)
            dateRange2 = DateRange(start=startDate2, end=endDate2)
            return (item in dateRange1) or (item in dateRange2)

    def format(self, pattern='mmdd', separator='-') -> str:
        return f'{self.start.format(pattern=pattern)}{separator}{self.end.format(pattern=pattern)}'
