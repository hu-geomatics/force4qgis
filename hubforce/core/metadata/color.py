from dataclasses import dataclass, field
from itertools import product
from random import randint

from force4qgis.hubforce.core.base import DataClassArray


@dataclass(frozen=True)
class Color(DataClassArray):
    red: int = 0
    green: int = 0
    blue: int = 0
    alpha: int = 255

    @classmethod
    def fromRandom(cls):
        return cls(red=randint(0, 255), green=randint(0, 255), blue=randint(0, 255))
