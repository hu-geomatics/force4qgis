from unittest import TestCase

from force4qgis.hubforce.core import Geometry, PointGeometry, PolygonGeometry, Envelope
from force4qgis.hubforce.core import Location
from force4qgis.hubforce.core import Projection


class TestGeometry(TestCase):

    def test_withInvalidArguments(self):
        with self.assertRaises(ValueError):
            Geometry(wkt='e(0 0 0 0)')
        with self.assertRaises(ValueError):
            Geometry(wkt='point(a b)')
        with self.assertRaises(ValueError):
            Geometry(wkt='Point(0 1)')

    def test_fixWktFormat(self):
        wkt = 'Point(0 1)'
        niceWkt =  Geometry.formatWkt(wkt)
        self.assertIsInstance(Geometry(wkt=niceWkt), Geometry)

    def test_coordinatesNotSupportedType(self):
        with self.assertRaises(TypeError):
            Geometry(wkt='MULTIPOINT (0 0,1 1)').coordinates()

    def test_CreatePointAndCheckCoordinates(self):
        coordinate = Location(0, 1)
        point = Geometry.Create.Point(coordinate=coordinate)
        self.assertTrue(coordinate, point.coordinates()[0])

    def test_CreatePolygonAndGetCoordinates(self):
        coordinates = [Location(*xy) for xy in [(0, 0), (1, 0), (1, 1), (0, 1)]]
        polygon = Geometry.Create.Polygon(coordinates=coordinates)
        lead = polygon.coordinates()
        for a, b in zip(coordinates, lead):
            self.assertTrue(a == b)

    def test_CreateRectangleAndCheckExtent(self):
        coordinates = [Location(1, 10), Location(2, 20)]
        rectangle = Geometry.Create.Rectangle(coordinates)
        e = rectangle.envelope()
        gold = Location(1, 20), Location(2, 10)
        lead = e.ul(), e.lr()
        self.assertTrue(gold == lead)

    def test_intersectionOfTwoIntersectingRectangles(self):
        a = Geometry.Create.Rectangle([Location(0, 0), Location(10, 10)])
        b = Geometry.Create.Rectangle([Location(5, 5), Location(15, 15)])
        self.assertTrue(a.intersects(b))
        e = a.intersection(b).envelope()
        gold = Location(x=5, y=10), Location(x=10, y=5)
        lead = e.ul(), e.lr()
        self.assertTrue(gold == lead)

    def test_intersectionOfTwoNonIntersectingRectanglesIsEmpty(self):
        a = Geometry.Create.Rectangle([Location(0, 0), Location(10, 10)])
        b = Geometry.Create.Rectangle([Location(50, 50), Location(150, 150)])
        self.assertFalse(a.intersects(b))

    def test_union(self):
        a = Geometry.Create.Rectangle([Location(0, 0), Location(10, 10)])
        b = Geometry.Create.Rectangle([Location(5, 5), Location(15, 15)])
        e = a.union(b).envelope()
        gold = Location(x=0, y=15), Location(x=15, y=0)
        lead = e.ul(), e.lr()
        self.assertTrue(gold == lead)

    def test_within(self):
        r = Geometry.Create.Rectangle([Location(0, 0), Location(10, 10)])
        self.assertTrue(Geometry.Create.Point(Location(5, 5)).within(r))
        self.assertFalse(Geometry.Create.Point(Location(50, 50)).within(r))

    def test_reproject(self):
        p = Geometry.Create.Point(Location(0, 0), Projection.Create.Wgs84WebMercator())
        p2 = p.reproject(Projection.Create.Wgs84())
        self.assertTrue(p2.projection == Projection.Create.Wgs84())

    def test_buffer(self):
        point = Geometry.Create.Point(Location(0, 0))
        circle = point.buffer(10)
        gold = Envelope(-10, 10, -10, 10)
        lead = circle.envelope()
        self.assertTrue(gold, lead)

    def test_formatWktWithInvalidWkt(self):
        with self.assertRaises(ValueError):
            Geometry.formatWkt(wkt='invalid')


class TestPoint(TestCase):

    def test_Create(self):
        point = Geometry.Create.Point((Location(0, 1)))
        self.assertIsInstance(point, PointGeometry)

    def test_invalidGeometryType(self):
        with self.assertRaises(ValueError):
            PointGeometry(wkt=Geometry.Create.Polygon([Location(0, 0), Location(1, 1)]).wkt)

    def test_getXYAndCoordinate(self):
        gold = Location(0, 1)
        point = Geometry.Create.Point(gold)
        lead = point.coordinate()
        self.assertTrue(gold == lead)
        self.assertTrue(gold.x == point.x() == 0)
        self.assertTrue(gold.y == point.y())



class TestPolygon(TestCase):

    def test_Create(self):
        point = Geometry.Create.Polygon([Location(0, 0), Location(1, 1), Location(2, 0)])
        self.assertIsInstance(point, PolygonGeometry)

    def test_invalidGeometryType(self):
        with self.assertRaises(ValueError):
            PolygonGeometry(wkt=Geometry.Create.Point(Location(0, 0)).wkt)
