from unittest.case import TestCase

from force4qgis.hubforce.core import Location
from force4qgis.hubforce.core import Extent


class TestExtent(TestCase):

    def test_CreateAndCheckCorners(self):
        extent = Extent(0, 1, 10, 11)
        ul, ur, ll, lr = Location(0, 11), Location(1, 11), Location(0, 10), Location(1, 10)
        self.assertEqual(ul, extent.ul())
        self.assertEqual(ur, extent.ur())
        self.assertEqual(ll, extent.ll())
        self.assertEqual(lr, extent.lr())

    def test_extentWithNegativeSize(self):
        with self.assertRaises(ValueError):
            Extent(0, -1, 10, 11)

    def test_compareAlmostEqualExtents(self):
        a = Extent(0, 1, 0, 1)
        b = Extent(0, 1, 0, 1.0000001)
        self.assertFalse(a == b)
        self.assertTrue(a.almostEqual(b))

    #def test_almostInside(self):
    #    def check(a, b):
    #        self.assertFalse(a == b)
    #        self.assertTrue(a.almostEqual(b))

    #    for small in [Extent(0, 1, 0, 1 + 1e-6), Extent(0, 1, 0, 1 - 1e-6)]:
    #        big = Extent(0, 1, 0, 1)
