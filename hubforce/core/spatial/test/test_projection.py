from unittest import TestCase
from fnmatch import fnmatch

from osgeo import osr

from force4qgis.hubforce.core import Projection


class TestProjection(TestCase):

    def test_Create(self):
        self.assertTrue(fnmatch(Projection.Create.Epsg(4326).wkt,
                                'GEOGCS["WGS 84",*'))
        self.assertTrue(fnmatch(Projection.Create.Wgs84().wkt,
                                'GEOGCS["WGS 84",*'))
        self.assertTrue(fnmatch(Projection.Create.Wgs84WebMercator().wkt,
                                'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",*'))

    def test_invalidWkt(self):
        with self.assertRaises(ValueError):
            Projection(wkt='')
        with self.assertRaises(ValueError):
            Projection(wkt='invalid')

    def test_osrSpatialReference(self):
        self.assertIsInstance(Projection.Create.Wgs84().osrSpatialReference(), osr.SpatialReference)

    def test_equal(self):
        gold = Projection.Create.Wgs84()
        self.assertTrue(gold == Projection.Create.Wgs84())
        self.assertFalse(gold == Projection.Create.Wgs84WebMercator())
