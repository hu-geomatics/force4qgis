from typing import Union, Tuple, Sequence, Optional

import numpy as np

from force4qgis.hubforce.core.delayed.multibandraster import delayedMultiBandRaster
from force4qgis.hubforce.core.delayed.singlebandpixelfunction import PixelFunctionArguments
from force4qgis.hubforce.core.metadata.date import Date
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.raster import Raster


def noDataRaster(
        name: str,
        bandNames: Sequence[str],
        date: Optional[Date],
        noDataValue: Union[float, int],
        dtype: type,
        grids: Tuple[Grid, ...],
        tilenames: Tuple[str, ...]
) -> Raster:

    outSize = len(bandNames)
    sourceRasters = []
    raster = delayedMultiBandRaster(
        name=name, grids=grids, tilenames=tilenames, sourceRasters=sourceRasters,
        pixelFunction=noDataRasterPixelFunction, outSize=outSize,
        outNoDataValue=noDataValue, outType=dtype,
        date=date,
        bandNames=bandNames
    )
    return raster


def noDataRasterPixelFunction(args: PixelFunctionArguments):
    out_ar = np.full(shape=(args.out_size, args.ysize, args.xsize), fill_value=args.out_ndv, dtype=args.out_dtype)
    return out_ar


