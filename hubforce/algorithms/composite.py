import warnings
from typing import Callable, Sequence

import numpy as np

from force4qgis.hubforce.core.delayed.singlebandpixelfunction import PixelFunctionArguments
from force4qgis.hubforce.core.delayed.singlebandraster import delayedSingleBandRaster
from force4qgis.hubforce.core.delayed.utils import functionImportPath
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection


def composite(
        rasterCollection: RasterCollection, aggregationFunction: Callable, aggregationName: str = None, **kwargs
) -> Raster:
    assert isinstance(rasterCollection, RasterCollection)
    assert callable(aggregationFunction)
    if aggregationName is None:
        aggregationName = aggregationFunction.__name__
    z = len(rasterCollection.first)
    for raster in rasterCollection.rasters:
        assert len(raster) == z, 'number of raster bands not matching'
    bandNames = list()
    composites = list()
    for bandNumber in range(1, len(rasterCollection.first) + 1):
        name = f'{aggregationName.upper()}_{rasterCollection.first.bandNames[bandNumber-1]}'
        sourceRasters = [raster.select(selectors=[bandNumber]) for raster in rasterCollection.rasters]
        aggregationFunctionPath = functionImportPath(aggregationFunction)
        bandNames.append(name)
        kwargs['aggregationFunction'] = aggregationFunctionPath
        composite = delayedSingleBandRaster(
            name=name,
            grids=rasterCollection.grids,
            tilenames=rasterCollection.tilenames,
            sourceRasters=sourceRasters,
            pixelFunction=compositePixelFunction, outType=np.int16,
            outNoDataValue=-9999,
            kwds=kwargs
        )
        composites.append(composite)

    composite = RasterCollection(
        rasters=tuple(composites), grids=rasterCollection.grids, tilenames=rasterCollection.tilenames
    ).toBands().rename(name=aggregationName, bandNames=bandNames)
    return composite


def minComposite(rasterCollection: RasterCollection):
    return composite(rasterCollection=rasterCollection, aggregationFunction=np.nanmin)


def meanComposite(rasterCollection: RasterCollection):
    return composite(rasterCollection=rasterCollection, aggregationFunction=np.nanmean)


def maxComposite(rasterCollection: RasterCollection):
    return composite(rasterCollection=rasterCollection, aggregationFunction=np.nanmax)


def stdComposite(rasterCollection: RasterCollection):
    return composite(rasterCollection=rasterCollection, aggregationFunction=np.nanstd)


def medianComposite(rasterCollection: RasterCollection):
    return composite(rasterCollection=rasterCollection, aggregationFunction=np.nanmedian)

def compositePixelFunction(args: PixelFunctionArguments, aggregationFunction):
    # import aggragation function
    *path, funcname = aggregationFunction.decode().split('.')
    path = '.'.join(path)
    namespace = dict()
    exec(f'from {path} import {funcname}', namespace)
    aggregationFunction = namespace[funcname]

    # skip missing bands
    missings = [a is None for a in args.in_ar]
    in_ar = [a for a, missing in zip(args.in_ar, missings) if not missing]
    in_ma = [m for m, missing in zip(args.in_ma, missings) if not missing]
    in_ndv = [ndv for ndv, missing in zip(args.in_ndv, missings) if not missing]
    assert len(in_ar) == len(in_ma) == len(in_ndv)

    if len(in_ar) == 0:
        out_ar = np.full(shape=(args.ysize, args.xsize), fill_value=args.out_ndv, dtype=args.out_dtype)
    else:
        # convert inputs to float
        in_ar = [a.astype(np.float32) for a in in_ar]

        # replace noDataValues with nan
        for a, ndv in zip(in_ar, in_ndv):
            a[a == ndv] = np.nan

        for a, m in zip(in_ar, in_ma):
            if m is not None:
                a[np.logical_not(m)] = np.nan

        # aggregate
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')
            out_ar = aggregationFunction(in_ar, axis=0)

        # convert result
        out_ar[np.isnan(out_ar)] = args.out_ndv
        out_ar = out_ar.astype(dtype=args.out_dtype)

    return out_ar
