from typing import Optional, List

from osgeo import gdal

from force4qgis.hubforce.core import *


def reprojectRaster(
        raster: GdalRaster, grid: Optional[Grid] = None, filename: Optional[str] = None,
        driver: Optional[RasterDriver] = None, options: Optional[List[str]] = None,
        resampling=Resampling.NearestNeighbour, **kwargs
) -> GdalRaster:
    """Reproject raster into grid. All **kwargs are passed to gdal.WarpOptions."""

    assert isinstance(raster, GdalRaster)
    if grid is None:
        grid = raster.grid
    assert isinstance(grid, Grid)
    if driver is None:
        driver = RasterDriver.fromFilename(filename=filename)
    assert isinstance(driver, RasterDriver)
    filename = driver.prepareCreation(filename=filename)
    if options is None:
        options = driver.options
    assert isinstance(options, list)
    assert isinstance(resampling, Resampling)
    assert Resampling.isValidWarpOption(option=resampling)
    resampleAlg = resampling.value

    outputBounds = (grid.extent.xmin, grid.extent.ymin, grid.extent.xmax, grid.extent.ymax)
    warpOptions = gdal.WarpOptions(
        format=driver.name, outputBounds=outputBounds, xRes=grid.resolution.x, yRes=grid.resolution.y,
        dstSRS=grid.projection.wkt, creationOptions=options, resampleAlg=resampleAlg, **kwargs)
    gdalDataset = gdal.Warp(destNameOrDestDS=filename, srcDSOrSrcDSTab=raster.gdalDataset, options=warpOptions)

    return GdalRaster(gdalDataset=gdalDataset)
