import pickle

import numpy as np
from sklearn.base import is_classifier

from force4qgis.hubforce.core.delayed.singlebandpixelfunction import PixelFunctionArguments
from force4qgis.hubforce.core import Raster
from force4qgis.hubforce.core.delayed import delayedMultiBandRaster


def estimatorPredict(estimatorFilename: str, raster: Raster, name=None, bandNames=None) -> Raster:
    assert isinstance(raster, Raster)
    if name is None:
        name = 'prediction'
    with open(estimatorFilename, 'rb') as file:
        estimator = pickle.load(file)

    if is_classifier(estimator):
        outSize = 1
        outNoDataValue = 0
    else:
        assert 0, 'not yet implemented'

    if bandNames is not None:
        assert len(bandNames) == outSize

    grids = raster.grids
    tilenames = raster.tilenames
    sourceRasters = [raster]
    raster = delayedMultiBandRaster(
        name=name, grids=grids, tilenames=tilenames, sourceRasters=sourceRasters,
        pixelFunction=estimatorPredictPixelFunction, outSize=outSize,
        outNoDataValue=outNoDataValue,
        kwds=dict(estimatorFilename=estimatorFilename), date=raster.date,
        bandNames=bandNames
    )
    return raster


def estimatorPredictPixelFunction(args: PixelFunctionArguments, estimatorFilename):

    with open(estimatorFilename, 'rb') as file:
        estimator = pickle.load(file)

    ysize, xsize = args.in_ar[0].shape
    zsize = args.out_size

    # derive valid pixels
    valid = np.full_like(args.in_ar[0], fill_value=True, dtype=np.bool)
    for a, ndv in zip(args.in_ar, args.in_ndv):
        if ndv is None:
            pass
        elif np.isnan(ndv):
            valid[np.isnan(a)] = False
        else:
            valid[a == ndv] = False
    for a, m in zip(args.in_ar, args.in_ma):
        if m is None:
            pass
        else:
            valid[np.logical_not(m)] = False

    X = np.array([a[valid] for a in args.in_ar]).T
    y = estimator.predict(X=X)
    out_ar = np.full(shape=(zsize, ysize, xsize), fill_value=args.out_ndv, dtype=np.float32)

    assert y.ndim == 1, 'todo: handle multiband prediction'
    out_ar[0][valid] = y
    return out_ar
