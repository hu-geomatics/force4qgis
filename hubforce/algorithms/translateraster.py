from typing import List

from osgeo import gdal

from force4qgis.hubforce.core import Resampling, Grid, RasterDriver
from force4qgis.hubforce.core import GdalRaster


def translateRaster(
        raster: GdalRaster, filename='', grid: Grid = None, indices: List[int] = None, driver: RasterDriver = None,
        options: List[str] = None, resampling=Resampling.NearestNeighbour, **kwargs
) -> GdalRaster:
    if indices is None:
        bandList = None
    else:
        assert len(indices) > 0
        bandList = [i + 1 for i in indices]
    if driver is None:
        driver = RasterDriver.fromFilename(filename=filename)
    assert isinstance(driver, RasterDriver)
    filename = driver.prepareCreation(filename)
    if options is None:
        options = driver.options
    assert isinstance(options, list)
    assert isinstance(resampling, Resampling)
    if grid is None:
        translateOptions = gdal.TranslateOptions(
            format=driver.name, creationOptions=options, resampleAlg=resampling.value, bandList=bandList, **kwargs
        )
    else:
        xRes, yRes = grid.resolution
        ulx, uly = grid.extent.ul
        lrx, lry = grid.extent.lr
        projWin = [ulx, uly, lrx, lry]
        translateOptions = gdal.TranslateOptions(
            format=driver.name, creationOptions=options, xRes=xRes, yRes=yRes, projWin=projWin,
            resampleAlg=resampling.value, bandList=bandList,
            **kwargs
        )
    gdalDataset = gdal.Translate(destName=filename, srcDS=raster.gdalDataset, options=translateOptions)
    return GdalRaster(gdalDataset=gdalDataset)


def translateBand(
        band, filename='', grid: Grid = None, driver: RasterDriver = None,
        options: List[str] = None, resampling=Resampling.NearestNeighbour, **kwargs
) -> GdalRaster:
    raster = band.raster
    indices = [band.index]
    return translateRaster(
        raster=raster, filename=filename, grid=grid, indices=indices, driver=driver, options=options,
        resampling=resampling, **kwargs
    )
