import warnings
from typing import Dict

import numpy as np

from force4qgis.hubforce.core.delayed.singlebandpixelfunction import PixelFunctionArguments
from force4qgis.hubforce.core.delayed.singlebandraster import delayedSingleBandRaster
from force4qgis.hubforce.core.delayed.utils import encodeIterable, importFunction
from force4qgis.hubforce.core.raster.raster import Raster


def bandmath(expression: str, raster: Raster, name='bandmath', **kwargs: Dict) -> Raster:
    assert isinstance(expression, str)
    grids = raster.grids
    tilenames = raster.tilenames
    sourceRasters = [raster.select(selectors=[bandName]) for bandName in raster.bandNames]
    names = encodeIterable(raster.bandNames)
    raster = delayedSingleBandRaster(
        name=name, grids=grids, tilenames=tilenames, sourceRasters=sourceRasters, pixelFunction=bandmathPixelFunction,
        kwds=dict(names=names, expression=expression), date=raster.date, bandNames=[name],
        **kwargs
    )
    return raster


def bandmathPixelFunction(args: PixelFunctionArguments, names, expression):

    # assign arrays
    namespace = dict(in_ar=args.in_ar, np=np)
    exec(f'{names.decode()}, = in_ar', namespace)

    # evaluate expression
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore')
        try:
            out_ar = importFunction(expression)(*eval(names, namespace))
        except SyntaxError:
            out_ar = eval(expression, namespace)

    # apply mask
    out_ma = np.full(shape=(args.buf_ysize, args.buf_xsize), fill_value=np.True_, dtype=np.bool)
    for ma in args.in_ma:
        np.logical_and(out_ma, ma, out=out_ma)

    for ar, ndv in zip(args.in_ar, args.in_ndv):
        assert np.isfinite(ndv)
        ma = np.not_equal(ar, ndv)
        np.logical_and(out_ma, ma, out=out_ma)
    out_ar[np.logical_not(out_ma)] = args.out_ndv
    out_ar[np.logical_not(np.isfinite(out_ar))] = args.out_ndv

    return out_ar
