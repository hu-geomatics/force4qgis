import warnings
from typing import Callable, Sequence

import numpy as np

from force4qgis.hubforce.core.delayed.multibandraster import delayedMultiBandRaster
from force4qgis.hubforce.core.delayed.singlebandpixelfunction import PixelFunctionArguments
from force4qgis.hubforce.core.delayed.singlebandraster import delayedSingleBandRaster
from force4qgis.hubforce.core.delayed.utils import functionImportPath
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection


def percentiles(rasterCollection: RasterCollection, q: Sequence[int]) -> Raster:
    assert isinstance(rasterCollection, RasterCollection)
    z = len(rasterCollection.first)
    for raster in rasterCollection.rasters:
        assert len(raster) == z, 'number of raster bands not matching'
    bandNames = [f'P{v}' for v in q]
    tmpRasters = list()
    for bandNumber in range(1, len(rasterCollection.first) + 1):
        sourceRasters = [raster.select(selectors=[bandNumber]) for raster in rasterCollection.rasters]
        name = f'{rasterCollection.first.band(bandNumber).name}'
        kwds = dict(q=q)
        tmpRaster = delayedMultiBandRaster(
            name=name,
            grids=rasterCollection.grids,
            tilenames=rasterCollection.tilenames,
            sourceRasters=sourceRasters,
            pixelFunction=percentilePixelFunction,
            outSize=len(q),
            outType=np.int16,
            outNoDataValue=-9999,
            kwds=kwds,
            bandNames=bandNames
        )
        tmpRasters.append(tmpRaster)

    # sort bands by percentiles
    bands = list()
    for bandName in bandNames:
        bands.extend(raster.select(
            selectors=[bandName], newRasterName='tmp', newBandNames=[f'{bandName}_{raster.name}']
        ).band(1) for raster in tmpRasters)

    result = Raster(
        name='percentile', date=None, bands=tuple(bands), grids=rasterCollection.grids,
        tilenames=rasterCollection.tilenames
    )

    return result


def percentilePixelFunction(args: PixelFunctionArguments, q):
    q = eval(q)

    # skip missing bands
    missings = [a is None for a in args.in_ar]
    in_ar = [a for a, missing in zip(args.in_ar, missings) if not missing]
    in_ma = [m for m, missing in zip(args.in_ma, missings) if not missing]
    in_ndv = [ndv for ndv, missing in zip(args.in_ndv, missings) if not missing]
    assert len(in_ar) == len(in_ma) == len(in_ndv)

    if len(in_ar) == 0:
        out_ar = np.full(shape=(args.out_size, args.ysize, args.xsize), fill_value=args.out_ndv, dtype=args.out_dtype)
    else:
        # convert inputs to float
        in_ar = [a.astype(np.float32) for a in in_ar]

        # replace noDataValues with nan
        for a, ndv in zip(in_ar, in_ndv):
            a[a == ndv] = np.nan

        for a, m in zip(in_ar, in_ma):
            if m is not None:
                a[np.logical_not(m)] = np.nan

        # aggregate
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')
            # out_ar = np.nanpercentile(in_ar, q=q, axis=0)
            out_ar = _nanpercentile(in_ar, q=q)

        # convert result
        for ar in out_ar:
            ar[np.isinf(ar)] = args.out_ndv
        out_ar = np.array(out_ar, dtype=args.out_dtype)

    return out_ar


def _nanpercentile(arr, q):
    valid_obs = np.sum(np.isfinite(arr), axis=0)

    # sort - former NaNs will move to the end
    for a in arr:
        a[np.isnan(a)] = np.Inf
    sorted_arr = np.sort(arr, axis=0)

    results = []
    for percentile in q:
        assert 0 <= percentile <= 100
        k_arr = np.round((valid_obs - 1) * (percentile / 100.0)).astype(dtype=np.int)
        result = _zvalue_from_index(arr=sorted_arr, ind=k_arr)
        results.append(result)
    return results


def _zvalue_from_index(arr, ind):
    nB, nL, nS = arr.shape
    idx = nS * nL * ind + nS * np.arange(nL)[:, None] + np.arange(nS)[None, :]
    return np.take(arr, idx)
