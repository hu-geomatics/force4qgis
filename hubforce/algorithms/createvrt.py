from os import makedirs
from os.path import abspath, dirname, exists
from typing import List, Union, Iterable

from osgeo import gdal

from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.grid import Grid
from force4qgis.hubforce.core.raster.rasterdriver import VRT_DRIVER


def createVrt(rasters: List[GdalRaster], filename: str, separate: bool, grid: Grid = None, **kwargs) -> GdalRaster:
    assert isinstance(rasters, list)
    assert len(rasters) > 0
    for raster in rasters:
        assert isinstance(raster, GdalRaster)
    assert isinstance(filename, str)
    assert isinstance(separate, bool)
    if grid is not None:
        assert isinstance(grid, Grid)
        minX, minY = grid.extent.ll
        maxX, maxY = grid.extent.ur
        outputBounds = minX, minY, maxX, maxY
        xRes, yRes = grid.resolution
    else:
        outputBounds = xRes = yRes = None
    filenames = list()
    for raster in rasters:
        filenames.append(abspath(raster.filename))
    VRT_DRIVER.prepareCreation(filename=filename)
    options = gdal.BuildVRTOptions(separate=separate, outputBounds=outputBounds, xRes=xRes, yRes=yRes, **kwargs)
    gdalDataset: gdal.Dataset = gdal.BuildVRT(destName=filename, srcDSOrSrcDSTab=filenames, options=options)
    gdalDataset.FlushCache()
    return GdalRaster(gdalDataset=gdalDataset)

def createMosaic(rasters: List[GdalRaster], filename: str=None, grid: Grid = None, **kwargs) -> GdalRaster:
    return createVrt(rasters=rasters, filename=filename, separate=False, grid=grid, **kwargs)

def createStack(rasters: List[GdalRaster], filename: str=None, grid: Grid = None, **kwargs) -> GdalRaster:
    return createVrt(rasters=rasters, filename=filename, separate=True, grid=grid, **kwargs)

def createStackFromMultiBandRasters(
        filename: str, rasters: List[Union[GdalRaster, str]], numbers: List[List[int]] = None, grid: Grid = None
) -> GdalRaster:
    assert isinstance(rasters, list)
    assert len(rasters) > 0
    rasters = [GdalRaster.open(raster) if isinstance(raster, str) else raster for raster in rasters]
    for raster in rasters:
        assert isinstance(raster, (GdalRaster, str))
    if numbers is None:
        numbers = list()
        for raster in rasters:
            numbers.append([band.number for band in raster.bands])
    if grid is None:
        grid = rasters[0].grid
        for raster in rasters:
            grid = grid.union(raster.grid)

    # create VRT
    # - create mosaic from first band and grab SrcRect and DstRect for all rasters
    tmpfilename = '/vsimem/createMergedMosaic/tmp.vrt'
    createMosaic(rasters=rasters, filename=tmpfilename, bandList=[1])
    file = gdal.VSIFOpenL(tmpfilename, 'r')
    xml = [line + '\n' for line in gdal.VSIFReadL(1, 100000, file).decode().split('\n')]
    gdal.VSIFCloseL(file)
    gdal.Unlink(tmpfilename)
    xmlhead = xml[:3]
    xmltail = ['</VRTDataset>\n']
    xmlbandhead = xml[3]
    xmlbandtail = ['  </VRTRasterBand>\n']
    xmlRects = [line for line in xml if 'Rect xOff=' in line]
    xmlSrcRects = xmlRects[0::2]
    xmlDstRects = xmlRects[1::2]
    assert len(xmlSrcRects) == len(xmlDstRects) == len(rasters)

    # - write sources
    if not exists(dirname(filename)):
        makedirs(dirname(filename))
    with open(filename, 'w') as file:
        file.writelines(xmlhead)
        vrtBandIndex = 0
        for rasterIndex, (raster, bandNumbers) in enumerate(zip(rasters, numbers)):
            for bandNumber, band in zip(bandNumbers, raster.bands):
                xmlSrcRect = xmlSrcRects[rasterIndex]
                xmlDstRect = xmlDstRects[rasterIndex]
                file.writelines(xmlbandhead.replace('band="1"', f'band="{vrtBandIndex + 1}"', ))
                xmlsource = [
                    '    <SimpleSource>\n',
                    f'      <SourceFilename relativeToVRT="0">{abspath(band.raster.filename)}</SourceFilename>\n',
                    f'      <SourceBand>{bandNumber}</SourceBand>\n',
                    xmlSrcRect,
                    xmlDstRect,
                    '    </SimpleSource>\n']
                file.writelines(xmlsource)
                file.writelines(xmlbandtail)
                vrtBandIndex += 1
        file.writelines(xmltail)

    vrtRaster = GdalRaster.open(filename=filename)
    bands = [band for raster in rasters for band in raster.bands]
    for vrtBand, band in zip(vrtRaster.bands, bands):
        vrtBand.setDescription(band.description)
        vrtBand.setNoDataValue(band.noDataValue())
    vrtRaster.flushCache()
    return vrtRaster


def createMergedMosaic(rasters: List[Union[GdalRaster, str]], filename: str, grid: Grid = None) -> GdalRaster:
    #filename = autoFilename(filename, 'mosaic')
    assert 0
    assert isinstance(rasters, list)
    assert len(rasters) > 0
    rasters = [GdalRaster.open(raster) if isinstance(raster, str) else raster for raster in rasters]
    for raster in rasters:
        assert isinstance(raster, GdalRaster)
    if grid is None:
        grid = rasters[0].grid
        for raster in rasters:
            grid = grid.union(raster.grid)

    bandNames = list()
    for raster in rasters:
        for band in raster.bands:
            bandName = band.description
            assert bandName != '', 'empty band description string discovered, cannot merge raster bands'
            bandNames.append(bandName)
    bandNames = list(sorted(set(bandNames)))

    # create VRT
    # - create mosaic from first band and grab SrcRect and DstRect for all rasters
    tmpfilename = '/vsimem/createMergedMosaic/tmp.vrt'
    createMosaic(rasters=rasters, filename=tmpfilename, bandList=[1])
    file = gdal.VSIFOpenL(tmpfilename, 'r')
    xml = [line + '\n' for line in gdal.VSIFReadL(1, 100000, file).decode().split('\n')]
    gdal.VSIFCloseL(file)
    gdal.Unlink(tmpfilename)
    xmlhead = xml[:3]
    xmltail = ['</VRTDataset>\n']
    xmlbandhead = xml[3]
    xmlbandtail = ['  </VRTRasterBand>\n']
    xmlRects = [line for line in xml if 'Rect xOff=' in line]
    xmlSrcRects = xmlRects[0::2]
    xmlDstRects = xmlRects[1::2]
    assert len(xmlSrcRects) == len(xmlDstRects) == len(rasters)

    # - write sources
    with open(filename, 'w') as file:
        file.writelines(xmlhead)
        noDataValues = list()
        for vrtBandIndex, bandName in enumerate(bandNames):
            file.writelines(xmlbandhead.replace('band="1"', f'band="{vrtBandIndex + 1}"', ))
            for raster, xmlSrcRect, xmlDstRect in zip(rasters, xmlSrcRects, xmlDstRects):
                for rasterBandIndex, band in enumerate(raster.bands):
                    if band.description == bandName:
                        xmlsource = [
                            '    <SimpleSource>\n',
                            f'      <SourceFilename relativeToVRT="0">{abspath(raster.filename)}</SourceFilename>\n',
                            f'      <SourceBand>{rasterBandIndex + 1}</SourceBand>\n',
                            xmlSrcRect,
                            xmlDstRect,
                            '    </SimpleSource>\n']
                        file.writelines(xmlsource)
                        noDataValue = band.noDataValue()
                        break
            noDataValues.append(noDataValue)
            file.writelines(xmlbandtail)
        file.writelines(xmltail)

    vrtRaster = GdalRaster.open(filename=filename)
    for band, bandName, noDataValue in zip(vrtRaster.bands, bandNames, noDataValues):
        band.setDescription(bandName)
        band.setNoDataValue(noDataValue)
    vrtRaster.flushCache()
    return vrtRaster


# def createTimeseriesMosaic(timeseries: List[Timeseries], filename: str = None, grid: Grid = None) -> Timeseries:
#     filename = autoFilename(filename, 'timeseriesMosaic')
#     alltimebands = list()
#     timeseries = [Timeseries(gdalDataset=timeseries_.gdalDataset) for timeseries_ in timeseries]
#     for timeseries_ in timeseries:
#         assert isinstance(timeseries_, Timeseries)
#         assert timeseries_.names == timeseries[0].names
#         alltimebands.extend(timeseries_.timebands)
#     vrtRaster = createMergedMosaic(rasters=timeseries, filename=filename, grid=grid)
#
#     vrtNames = timeseries[0].names
#     vrtDates = list()
#     for vrtBand in vrtRaster.bands:
#         for i, timeband in enumerate(alltimebands):
#             if vrtBand.description == timeband.description:
#                 vrtDates.append(timeband.date)
#                 break
#     vrtDates = vrtDates[::len(vrtNames)]
#     vrtTimeseries = Timeseries(gdalDataset=vrtRaster.gdalDataset)
#     vrtTimeseries.setNames(vrtNames)
#     vrtTimeseries.setDates(vrtDates)
#     vrtTimeseries.flushCache()
#     return vrtTimeseries
#
#
# def createTimeseries(rasters: Iterable[GdalRaster], names: List[str] = None, dates: List[Date] = None, filename: str = None,
#         grid: Grid = None
# ) -> Timeseries:
#     filename = autoFilename(filename, 'timeseries')
#     rasters = list(rasters)
#     for raster in rasters:
#         assert isinstance(raster, GdalRaster)
#         assert raster.shape.z == rasters[0].shape.z, 'raster band counts not matching'
#     if names is None:
#         names = [f'Band {i + 1}' for i in range(rasters[0].shape.z)]
#
#     if dates is None:
#         dates = [raster.date(required=True) for raster in rasters]
#
#     stack = createStackFromMultiBandRasters(rasters=rasters, filename=filename, grid=grid)
#     timeseries = Timeseries(gdalDataset=stack.gdalDataset)
#     timeseries.setDates(dates=dates)
#     timeseries.setNames(names=names)
#     timeseries.flushCache()
#     return timeseries
