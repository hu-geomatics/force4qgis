from dataclasses import dataclass
from inspect import getsource
from typing import Callable, Union, List, Dict

import numpy as np

from hubforce.algorithms.bandmath import bandmath
from force4qgis.hubforce.core import Raster, Band
from force4qgis.hubforce.core.delayed.autofilename import autoFilename
from force4qgis.hubforce.core.delayed import functionImportPath


@dataclass(frozen=True)
class SpectralIndexDefinition(object):
    abbreviation: str = ''
    name: str = ''
    formular: Union[str, Callable] = ''
    description: str = ''
    url: str = ''

    @property
    def requiredInputs(self) -> List[str]:
        if isinstance(self.formular, str):
            code = self.formular
        elif callable(self.formular):
            code = getsource(self.formular)
        else:
            assert 0
        return [input for input in ('BLUE', 'GREEN', 'RED', 'RE1', 'RE2', 'RE3', 'BNIR', 'NIR', 'SWIR1', 'SWIR2')
                if input in code]


BLUE = SpectralIndexDefinition(abbreviation='BLUE', name='Blue Waveband', formular='BLUE')
GREEN = SpectralIndexDefinition(abbreviation='GREEN', name='Green Waveband', formular='GREEN')
RED = SpectralIndexDefinition(abbreviation='RED', name='Red Waveband', formular='RED')
RE1 = SpectralIndexDefinition(abbreviation='RE1', name='Red Egde Waveband', formular='RE1')
RE2 = SpectralIndexDefinition(abbreviation='RE2', name='Red Egde Waveband', formular='RE2')
RE3 = SpectralIndexDefinition(abbreviation='RE3', name='Red Egde Waveband', formular='RE3')
BNIR = SpectralIndexDefinition(abbreviation='BNIR', name='Near-Infrared Waveband', formular='BNIR')
NIR = SpectralIndexDefinition(abbreviation='NIR', name='Near-Infrared Waveband', formular='NIR')
SWIR1 = SpectralIndexDefinition(abbreviation='SWIR1', name='Short Wavelength Infrared Waveband 1', formular='SWIR1')
SWIR2 = SpectralIndexDefinition(abbreviation='SWIR2', name='Short Wavelength Infrared Waveband 2', formular='SWIR2')
NDVI = SpectralIndexDefinition(
    abbreviation='NDVI',
    name='Normalized Difference Vegetation Index',
    formular='(NIR - RED) / (NIR + RED)',
    description='',
    url='https://www.indexdatabase.de/db/i-single.php?id=58'
)
EVI = SpectralIndexDefinition(
    abbreviation='EVI',
    name='Enhanced Vegetation Index',
    formular='2.5 * ((NIR - RED) / (NIR + 6 * RED - 7.5 * BLUE + {scale}))',
    description='',
    url='https://www.indexdatabase.de/db/i-single.php?id=16'
)


def tcbFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [0.2043, 0.4158, 0.5524, 0.5741, 0.3124, 0.2303]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def tcgFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [-0.1603, -0.2819, -0.4934, 0.7940, 0.0002, 0.1446]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def tcwFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2):
    coeff = [0.0315, 0.2021, 0.3102, 0.1594, -0.6806, -0.6109]
    return _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff)


def _tcFormular(BLUE, GREEN, RED, NIR, SWIR1, SWIR2, coeff):
    TCB = np.multiply(BLUE, coeff[0], dtype=np.float32)
    for b, c in zip((GREEN, RED, NIR, SWIR1, SWIR2), coeff[1:]):
        TCB += np.multiply(b, c, dtype=np.float32)
    return TCB


TCB = SpectralIndexDefinition(
    abbreviation='TCB',
    name='Tasseled Cap Brightness',
    formular=tcbFormular,
    description='',
    url='https://www.indexdatabase.de/db/i-single.php?id=16'
)
TCG = SpectralIndexDefinition(
    abbreviation='TCG',
    name='Tasseled Cap Greenness',
    formular=tcgFormular,
    description='',
    url='https://www.indexdatabase.de/db/i-single.php?id=16'
)

TCW = SpectralIndexDefinition(
    abbreviation='TCW',
    name='Tasseled Cap Wetness',
    formular=tcwFormular,
    description='',
    url='https://www.indexdatabase.de/db/i-single.php?id=16'
)


# 'enhancedVegetationIndex2, EVI2 = 2.4*((NIR-Red)/(NIR+Red+scale)).
# 'Normalized Burn Ratio, NBR = (NIR - SWIR2) / (NIR + SWIR2)'
# 'Normalized Burn Ratio 2,  NBR2 = (SWIR1 - SWIR2) / (SWIR1 + SWIR2)'


def extractSpectralIndex(
        raster: Raster, spectralIndexDefinition: SpectralIndexDefinition, scale=None, filename: str = None, **kwargs
) -> Raster:
    expression = spectralIndexDefinition.formular
    if isinstance(expression, str):
        if '{scale}' in expression:
            assert scale is not None
            expression = expression.format(scale=scale)
        else:
            assert scale is None
        inputs = autoInputs(raster=raster, expression=expression)
    elif callable(expression):
        inputs = autoInputs(raster=raster, expression=getsource(expression))
        expression = functionImportPath(expression)
    else:
        assert 0
    for input in spectralIndexDefinition.requiredInputs:
        assert input in inputs, f'missing input: {input}'

    filename = autoFilename(filename, name=spectralIndexDefinition.abbreviation.lower())
    raster2 = bandmath(expression=expression, inputs=inputs, filename=filename, **kwargs)
    name = f'{spectralIndexDefinition.abbreviation} ({spectralIndexDefinition.name})'
    raster2 = raster2.rename(name, update=True)
    raster2 = raster2.setDate(date=raster.date)
    return raster2


def autoInputs(raster: Raster, expression: str) -> Dict[str, Band]:
    inputs = dict()
    for band in raster.bands:
        if band.name in expression:
            inputs[band.name] = band
    return inputs
