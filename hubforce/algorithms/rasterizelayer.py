from typing import Optional, List

import numpy as np
from osgeo import gdal

from force4qgis.hubforce.core import *


def rasterizeLayer(
        layer: Layer, grid: Grid, dtype=np.float32, initValue=0, burnValue=1,
        burnAttribute: Optional[str] = None, allTouched=False, filterSQL: Optional[str] = None,
        noDataValue: Optional[float] = None, filename='', driver=MEM_DRIVER, options: Optional[List[str]] = None
) -> GdalRaster:
    """Rasterized vector into grid."""
    assert isinstance(layer, Layer)
    assert isinstance(grid, Grid)
    assert layer.projection == grid.projection
    if driver is None:
        driver = RasterDriver.fromFilename(filename=filename)
    assert isinstance(driver, RasterDriver)
    if options is None:
        options = driver.options
    assert isinstance(options, list)

    layer.ogrLayer.SetAttributeFilter(filterSQL)
    layer.ogrLayer.SetSpatialFilter(grid.extent.geometry.ogrGeometry)

    raster = driver.createDataset(grid=grid, bands=1, dtype=dtype, filename=filename, options=options)
    if noDataValue is not None:
        raster.setNoDataValue(noDataValue)
    raster.bandByIndex(index=0).fill(value=initValue)
    rasterizeLayerOptions = list()
    if allTouched:
        rasterizeLayerOptions.append('ALL_TOUCHED=TRUE')
    if burnAttribute:
        rasterizeLayerOptions.append(f'ATTRIBUTE={burnAttribute}')
    gdal.RasterizeLayer(raster.gdalDataset, [1], layer.ogrLayer, burn_values=[burnValue], options=rasterizeLayerOptions)

    layer.ogrLayer.SetAttributeFilter(None)
    layer.ogrLayer.SetSpatialFilter(None)
    raster.flushCache()
    return raster
