from numpy.ma import abs

from force4qgis.hubforce.core import *


def reprojectGrid(grid: Grid, projection: Projection, resolution: Resolution) -> Grid:
    assert isinstance(grid, Grid)
    assert isinstance(projection, Projection)
    assert isinstance(resolution, Resolution)
    coordinateTransformation = CoordinateTransformation(source=grid.projection, target=projection)
    extent = grid.extent.reproject(coordinateTransformation=coordinateTransformation)
    ulPixelLocation = grid.pixelLocation(location=extent.ul)
    ulSnappedPixelLocation = ulPixelLocation.upperLeft
    ulLocation = grid.location(pixelLocation=ulSnappedPixelLocation)
    lrPixelLocation = grid.pixelLocation(location=extent.lr)
    lrSnappedPixelLocation = lrPixelLocation.lowerRight
    lrLocation = grid.location(pixelLocation=lrSnappedPixelLocation)
    size = Size.fromIterable(abs(lrLocation._ - ulLocation._))
    extent = Extent(ul=ulLocation, size=size)
    return Grid(extent=extent, resolution=resolution, projection=projection)
