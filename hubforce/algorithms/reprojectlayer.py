from osgeo import ogr, gdal

from force4qgis.hubforce.core import *


def reprojectLayer(layer: Layer, projection: Projection, filename='', driver=None, **kwargs) -> Layer:
    """"Reproject layer."""
    assert isinstance(layer, Layer)
    assert isinstance(projection, Projection)
    if driver is None:
        driver = memoryDriver
    assert isinstance(driver, VectorDriver)
    filename = driver.prepareCreation(filename)
    tmpfilename = '/vsimem/Layer.reproject/tmp.gpkg'

    gdalDataset = layer.gdalSource
    if isinstance(layer.gdalSource, ogr.DataSource):
        gdalDataset = _reprojectOgrDataSource(
            ogrDataSource=layer.gdalSource, tmpfilename=tmpfilename, layer=layer, projection=projection,
            filename=filename, driver=driver, **kwargs
        )
    outSource = _reprojectGdalDataset(
        gdalDataset=gdalDataset, layer=layer, projection=projection, filename=filename,
        driver=driver, **kwargs
    )
    outLayer = outSource.GetLayerByName(layer.name)
    return Layer(gdalSource=outSource, ogrLayer=outLayer)


def _reprojectOgrDataSource(ogrDataSource, tmpfilename, layer, projection, filename, driver, **kwargs) -> gdal.Dataset:
    assert isinstance(ogrDataSource, ogr.DataSource)
    ogrLayer = ogrDataSource.GetLayerByName(layer.name)
    outdriver = ogr.GetDriverByName('GPKG')
    mem: ogr.DataSource = outdriver.CreateDataSource(tmpfilename)
    mem.CopyLayer(ogrLayer, layer.name, ['OVERWRITE=YES'])
    mem.SyncToDisk()
    del mem
    gdalDataset = gdal.OpenEx(tmpfilename, gdal.OF_VECTOR, allowed_drivers=['GPKG'])
    return _reprojectGdalDataset(
        gdalDataset=gdalDataset, layer=layer, projection=projection, filename=filename,
        driver=driver, **kwargs
    )


def _reprojectGdalDataset(gdalDataset, layer, projection, filename, driver, **kwargs) -> gdal.Dataset:
    assert isinstance(gdalDataset, gdal.Dataset)
    options = gdal.VectorTranslateOptions(
        format=driver.name, dstSRS=projection.wkt, reproject=True, layers=[layer.name], layerName=layer.name, **kwargs
    )
    reprojected = gdal.VectorTranslate(filename, srcDS=gdalDataset, options=options)
    return reprojected
