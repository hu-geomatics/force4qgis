import pickle

import numpy as np

from force4qgis.hubforce.core import Raster
from force4qgis.hubforce.core.delayed import delayedMultiBandRaster


def estimatorPredictProbability(estimatorFilename: str, raster: Raster, name=None, bandNames=None) -> Raster:
    assert isinstance(raster, Raster)
    if name is None:
        name = 'probability'
    with open(estimatorFilename, 'rb') as file:
        estimator = pickle.load(file)
        outSize = max(estimator.classes_)
    if bandNames is not None:
        assert len(bandNames) >= outSize
        bandNames = bandNames[:outSize]

    grids = raster.grids
    tilenames = raster.tilenames
    sourceRasters = [raster]
    raster = delayedMultiBandRaster(
        name=name, grids=grids, tilenames=tilenames, sourceRasters=sourceRasters,
        pixelFunction=estimatorPredictProbabilityPixelFunction, outSize=outSize,
        outNoDataValue=np.finfo(np.float32).min,
        kwds=dict(estimatorFilename=estimatorFilename), date=raster.date,
        bandNames=bandNames
    )
    return raster


def estimatorPredictProbabilityPixelFunction(in_ar, in_ma, in_ndv, out_ndv, out_size, *args, estimatorFilename):

    with open(estimatorFilename, 'rb') as file:
        estimator = pickle.load(file)
        assert all(estimator.classes_ == sorted(estimator.classes_)) # not sure if classes are always sorted

    ysize, xsize = in_ar[0].shape
    zsize = out_size

    # derive valid pixels
    valid = np.full_like(in_ar[0], fill_value=True, dtype=np.bool)
    for a, ndv in zip(in_ar, in_ndv):
        if ndv is None:
            pass
        elif np.isnan(ndv):
            valid[np.isnan(a)] = False
        else:
            valid[a == ndv] = False
    for a, m in zip(in_ar, in_ma):
        if m is None:
            pass
        else:
            valid[np.logical_not(m)] = False

    X = np.array([a[valid] for a in in_ar]).T
    y = estimator.predict_proba(X=X)

    out_ar = np.full(shape=(zsize, ysize, xsize), fill_value=out_ndv, dtype=np.float32)
    for index, id in enumerate(estimator.classes_):
        out_ar[id-1][valid] = y.T[index]

    return out_ar
