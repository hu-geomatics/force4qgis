from typing import List

import numpy as np

from force4qgis.hubforce.core.const.qai import Qai
from force4qgis.hubforce.core.delayed.singlebandpixelfunction import PixelFunctionArguments
from force4qgis.hubforce.core.delayed.singlebandraster import delayedSingleBandRaster
from force4qgis.hubforce.core.raster.raster import Raster


def qaiMask(qai: Raster, flags: List[Qai], **kwargs) -> Raster:
    name = 'qaiMask'
    assert isinstance(qai, Raster)
    for flag in flags:
        assert isinstance(flag, Qai)
    sourceRasters = (qai,)
    grids = qai.grids
    tilenames = qai.tilenames
    flags = ', '.join([str(flag) for flag in flags])
    return delayedSingleBandRaster(
        name=name, grids=grids, tilenames=tilenames, sourceRasters=sourceRasters, pixelFunction=qaiMaskPixelFunction,
        outType=np.uint8, kwds=dict(flags=flags), date=qai.date, bandNames=[name], **kwargs
    )


def qaiMaskPixelFunction(args: PixelFunctionArguments, flags):
    qai, = args.in_ar
    out_ar = np.ones_like(qai, dtype=np.uint8)
    if flags == b'':
        flags = []
    else:
        flags = eval(flags)

    if isinstance(flags, Qai):
        flags = [flags]

    for flag in flags:
        if flag is Qai.ValidYes:
            bit, code, mask = 0, 0, 1
        elif flag is Qai.ValidNo:
            bit, code, mask = 0, 1, 1
        elif flag is Qai.CloudClear:
            bit, code, mask = 1, 0, 3
        elif flag is Qai.CloudLessConfident:
            bit, code, mask = 1, 1, 3
        elif flag is Qai.CloudConfident:
            bit, code, mask = 1, 2, 3
        elif flag is Qai.CloudCirrus:
            bit, code, mask = 1, 3, 3
        elif flag is Qai.CloudShadowNo:
            bit, code, mask = 3, 0, 1
        elif flag is Qai.CloudShadowYes:
            bit, code, mask = 3, 1, 1
        elif flag is Qai.SnowNo:
            bit, code, mask = 4, 0, 1
        elif flag is Qai.SnowYes:
            bit, code, mask = 4, 1, 1
        elif flag is Qai.WaterNo:
            bit, code, mask = 5, 0, 1
        elif flag is Qai.WaterYes:
            bit, code, mask = 5, 1, 1
        elif flag is Qai.AerosolEstimated:
            bit, code, mask = 6, 0, 3
        elif flag is Qai.AerosolInterpolated:
            bit, code, mask = 6, 1, 3
        elif flag is Qai.AerosolHigh:
            bit, code, mask = 6, 2, 3
        elif flag is Qai.AerosolFill:
            bit, code, mask = 6, 3, 3
        elif flag is Qai.SubzeroNo:
            bit, code, mask = 8, 0, 1
        elif flag is Qai.SubzeroYes:
            bit, code, mask = 8, 1, 1
        elif flag is Qai.SaturationNo:
            bit, code, mask = 9, 0, 1
        elif flag is Qai.SaturationYes:
            bit, code, mask = 9, 1, 1
        elif flag is Qai.HighSunZenithNo:
            bit, code, mask = 10, 0, 1
        elif flag is Qai.HighSunZenithYes:
            bit, code, mask = 10, 1, 1
        elif flag is Qai.IlluminationGood:
            bit, code, mask = 11, 0, 3
        elif flag is Qai.IlluminationLow:
            bit, code, mask = 11, 1, 3
        elif flag is Qai.IlluminationPoor:
            bit, code, mask = 11, 2, 3
        elif flag is Qai.IlluminationShadow:
            bit, code, mask = 11, 3, 3
        elif flag is Qai.SlopeNo:
            bit, code, mask = 13, 0, 1
        elif flag is Qai.SlopeYes:
            bit, code, mask = 13, 1, 1
        elif flag is Qai.WaterVaporMeasured:
            bit, code, mask = 14, 0, 1
        elif flag is Qai.WaterVaporFill:
            bit, code, mask = 14, 1, 1
        else:
            raise ValueError(f'not a valid QAI flag: {flag}')
        out_ar[np.right_shift(qai, bit) & mask == code] = 0

    return out_ar
