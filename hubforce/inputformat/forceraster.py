from os import DirEntry
from os.path import join, exists, splitext
from typing import Sequence, Callable

from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.raster.band import Band
from force4qgis.hubforce.core.raster.gdalband import GdalBand
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.resolution import Resolution

DEBUG = False


def forceRaster(
        forceRasterPath: str,
        basename: str,
        tilenames: Sequence[str], resolution: Resolution,
        resampling=Resampling.NearestNeighbour,
        bandList: Sequence[int] = None,
        bandNameFunc: Callable = None,
        debug=DEBUG
) -> Raster:
    tilenames = tuple(tilenames)
    filenames = list()
    grids = list()
    for tilename in tilenames:
        entry: DirEntry
        filename = join(forceRasterPath, tilename, basename)

        if debug:
            print(filename)

        assert exists(filename), filename

        gdalRaster = GdalRaster.open(filename=filename)
        grids.append(gdalRaster.grid.withResolution(resolution=resolution))
        filenames.append(filename)

    filenames = tuple(filenames)
    grids = tuple(grids)
    numbers = {band.number: (band.number,) * len(filenames) for band in gdalRaster.bands}

    bands = list()

    if bandList is None:
        bandList = range(1, gdalRaster.shape.z+1)

    for number in bandList:
        gdalBand = gdalRaster.band(number=number)

        if bandNameFunc is None:
            name = gdalBand.description
        else:
            name = bandNameFunc(gdalBand)

        band = Band(
            name=name, noDataValue=gdalBand.noDataValue(),
            filenames=tuple(filenames), numbers=numbers[number],
            grids=grids, tilenames=tilenames, resampling=resampling, dtype=gdalRaster.numpyDataType.value, mask=None
        )
        bands.append(band)

    name = splitext(basename)[0]
    date = None
    raster = Raster(name=name, date=date, bands=tuple(bands), grids=grids, tilenames=tilenames)

    return raster
