from collections import defaultdict, OrderedDict
from os import DirEntry, scandir
from os.path import join, basename, dirname, exists
from typing import Sequence, Callable

import numpy as np

from force4qgis.hubforce.core.const.resampling import Resampling
from force4qgis.hubforce.core.metadata.date import DateRange, DateSeason, Date
from force4qgis.hubforce.core.progress import printProgress
from force4qgis.hubforce.core.raster.band import Band
from force4qgis.hubforce.core.raster.gdalraster import GdalRaster
from force4qgis.hubforce.core.raster.raster import Raster
from force4qgis.hubforce.core.raster.rastercollection import RasterCollection
from force4qgis.hubforce.core.raster.resolution import Resolution
from force4qgis.hubforce.sensor import SENTINEL2_WAVEBANDS, LANDSAT_WAVEBANDS

DEBUG = True

def forceLevel2Collection(
        forceLevel2Path: str, tilenames: Sequence[str], sensors: Sequence[str], resolution: Resolution,
        extension='.tif', dateRange: DateRange = None, dateSeason: DateSeason = None,
        boaResampling=Resampling.Average, qaiResampling=Resampling.Mode, prefereLandsatImproved=False,
        callback: Callable=None, verbose=1,
) -> RasterCollection:
    if callback is None:
        callback = printProgress
    assert set(sensors).issubset({'SEN2A', 'SEN2B', 'LND04', 'LND05', 'LND07', 'LND08'})
    tilenames = tuple(tilenames)
    gridByTilename = OrderedDict()
    datasets = defaultdict(list)
    for tilename in tilenames:
        if verbose > 0:
            callback(f'Parse {tilename}')
        grid = None

        entry: DirEntry
        for entry in scandir(join(forceLevel2Path, tilename)):
            if not entry.name.endswith(f'BOA{extension}'):
                continue

            if grid is None:
                grid = GdalRaster.open(entry.path).grid.withResolution(resolution=resolution)

            datestamp, level, sensor, _ = entry.name.split('_')
            filename = entry.path
            if prefereLandsatImproved:
                if exists(filename.replace('BOA', 'IMP')):
                    filename = filename.replace('BOA', 'IMP')

            if dateRange is not None:
                date = Date.fromDatestamp(datestamp=datestamp, pattern='yyyymmdd')
                if not date in dateRange:
                    continue

            if dateSeason is not None:
                date = Date.fromDatestamp(datestamp=datestamp, pattern='yyyymmdd')
                if not date in dateSeason:
                    continue

            if not sensor in sensors:
                continue

            if verbose > 2:
                callback(f'Use {filename}')

            datasets[(datestamp, level, sensor)].append(filename)

        if grid is not None:
            gridByTilename[tilename] = grid

    sen2BandNames = [waveband.name for waveband in SENTINEL2_WAVEBANDS]
    lndBandNames = [waveband.name for waveband in LANDSAT_WAVEBANDS]

    tileCache = dict()
    def rasters():
        for (date, level, sensor), boaFilenames in datasets.items():
            if sensor in ('SEN2A', 'SEN2B'):
                bandNames = sen2BandNames
            elif sensor in ('LND04', 'LND05', 'LND07', 'LND08'):
                bandNames = lndBandNames
            else:
                assert 0, f'unknown sensor: {sensor}'

            tilenames_ = tuple(basename(dirname(f)) for f in boaFilenames)
            grids_ = tuple(gridByTilename[tilename] for tilename in tilenames_)
            if tilenames_ not in tileCache:
                tileCache[tilenames_] = (tilenames_, grids_)
            tilenames_, grids_ = tileCache[tilenames_]

            def bands():
                qaiFilenames = tuple(filename.replace('BOA', 'QAI').replace('IMP', 'QAI') for filename in boaFilenames)
                qaiBand = Band(
                    name='QAI', noDataValue=-9999,
                    filenames=qaiFilenames, numbers=(1,) * len(qaiFilenames), grids=grids_,
                    tilenames=tilenames_,
                    resampling=qaiResampling, dtype=np.int16, mask=None
                )

                for number, bandName in enumerate(bandNames, 1):
                    srBand = Band(
                        name=bandName, noDataValue=-9999,
                        filenames=tuple(boaFilenames), numbers=(number,) * len(boaFilenames),
                        grids=grids_, tilenames=tilenames_, resampling=boaResampling, dtype=np.int16, mask=None
                    )
                    srBand = srBand.setMask(srBand)
                    yield srBand
                yield qaiBand

            name = '_'.join((date, level, sensor))
            date = Date.fromDatestamp(datestamp=date, pattern='yyyymmdd')
            raster = Raster(name=name, date=date, bands=tuple(bands()), grids=grids_, tilenames=tilenames_)
            yield raster

    grids = tuple(gridByTilename.values())
    tilenames = tuple(gridByTilename.keys())
    return RasterCollection(rasters=tuple(rasters()), grids=grids, tilenames=tilenames)
