from osgeo import gdal
from distutils.version import LooseVersion

HUBDC_VERSION = '0.17.0'
HUBDC_VERSION_OBJ = LooseVersion(HUBDC_VERSION)
__version__ = HUBDC_VERSION

gdal.SetConfigOption('GDAL_VRT_ENABLE_PYTHON', 'YES')
