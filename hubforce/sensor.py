from dataclasses import dataclass
from enum import Enum
from typing import List


class Waveband(Enum):
    BLUE = 492
    GREEN = 560
    RED = 665
    RE1 = 704
    RE2 = 740
    RE3 = 783
    BNIR = 833
    NIR = 865
    SWIR1 = 1614
    SWIR2 = 2202


LANDSAT_WAVEBANDS = [Waveband.BLUE, Waveband.GREEN, Waveband.RED, Waveband.NIR, Waveband.SWIR1, Waveband.SWIR2]

SENTINEL2_WAVEBANDS = [
    Waveband.BLUE, Waveband.GREEN, Waveband.RED, Waveband.RE1, Waveband.RE2, Waveband.RE3, Waveband.BNIR, Waveband.NIR,
    Waveband.SWIR1, Waveband.SWIR2
]


@dataclass(frozen=True)
class Sensor(object):
    name: str
    shortName: str
    bands: List[Waveband]

    def index(self, band: Waveband) -> int:
        return self.bands.index(band)


SENTINEL2A_SENSOR = Sensor(name='Sentinel2A', shortName='SEN2A', bands=SENTINEL2_WAVEBANDS)
SENTINEL2B_SENSOR = Sensor(name='Sentinel2B', shortName='SEN2B', bands=SENTINEL2_WAVEBANDS)
LANDSAT4_SENSOR = Sensor(name='Landsat4', shortName='LND04', bands=LANDSAT_WAVEBANDS)
LANDSAT5_SENSOR = Sensor(name='Landsat5', shortName='LND05', bands=LANDSAT_WAVEBANDS)
LANDSAT7_SENSOR = Sensor(name='Landsat7', shortName='LND07', bands=LANDSAT_WAVEBANDS)
LANDSAT8_SENSOR = Sensor(name='Landsat8', shortName='LND08', bands=LANDSAT_WAVEBANDS)
